#!/bin/sh
define(`concat',`$1$2')dnl
define(`BUSNAME',ifdef(`CONFIG_PCB_SYSBUS_NAME',`CONFIG_PCB_SYSBUS_NAME',`sysbus'))dnl

. /usr/bin/pcb_common.sh

name="nemo-core"
pcb_app_options="-I /var/run/nemo -c /usr/lib/nemo/nemo.odl"
datamodel="NeMo"
trace_zones="nemo alias cgnat copy dhcp dhcp-api dhcpv6 dsl nat penable ppp sfp vlan wlan xtm"
component="sah_lib_nemo"
nemo_args=""

case $1 in
    boot|start)
        if [ "$RUNMODE" != "1" ] && [ -f "/usr/lib/nemo/runmodes/${RUNMODE}/nemo-defaults.odl" ] ; then
            nemo_args="--args --defaults /usr/lib/nemo/runmodes/${RUNMODE}/nemo-defaults.odl"
        fi
        pcb_start $name $pcb_app_options $nemo_args
        pcb_cli -q -w -1 'NeMo'
        mtk_load $name /usr/lib/nemo/modules/built-in.so
        ;;
    stop)
        mtk_unload $name built-in
        pcb_stop $name
        ;;
    debuginfo)
        pcb_debug_info $name $component $datamodel
        ;;
    log)
        action=$2
        if [ -n "$action" ]; then
            pcb_log $name $action $trace_zones
        else
            pcb_log $name enable $trace_zones
        fi
        ;;
    *)
        echo "Usage : $0 [boot|start|stop|debuginfo|log]"
        ;;
esac
