/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/common.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core.h"

typedef struct _mib_class {
	llist_iterator_t it;
	char *name;
	char *odlfile;
	flagexp_t *flagexp;
	object_t *object;
	object_t *datamodel;
} mib_class_t;

typedef struct _intf_blob {
	llist_t objects;
	llist_t parameters;
	llist_t functions;
	pcb_timer_t *timer;
	intf_t *intf;
	mib_class_t *class;
} mib_instance_t;

typedef struct _mib_item {
	llist_iterator_t it;
	union {
		object_t *object;
		parameter_t *parameter;
		function_t *function;
		void *raw;
	} u;
} mib_item_t;

static llist_t mib_classes = {NULL, NULL};

static mib_item_t *mib_item_create(llist_t *list, void *raw) {
	mib_item_t *item = calloc(1, sizeof(mib_item_t));
	if (!item) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	item->u.raw = raw;
	llist_append(list, &item->it);
	return item;
}

static void mib_item_destroy(mib_item_t *item) {
	llist_iterator_take(&item->it);
	free(item);
}

static void mib_instance_destroy(mib_instance_t *mib) {
	if (!mib)
		return;
	SAH_TRACEZ_INFO("nemo", "Destroying instance of mib %s on intf %s", mib->class->odlfile, intf_name(mib->intf));
	mib_item_t *item;
	while ((item = (mib_item_t *)llist_first(&mib->objects))) {
		object_delete(item->u.object);
		mib_item_destroy(item);
	}
	while ((item = (mib_item_t *)llist_first(&mib->parameters))) {
		parameter_delete(item->u.parameter);
		mib_item_destroy(item);
	}
	while ((item = (mib_item_t *)llist_first(&mib->functions))) {
		function_delete(item->u.function);
		mib_item_destroy(item);
	}
	llist_cleanup(&mib->objects);
	llist_cleanup(&mib->parameters);
	llist_cleanup(&mib->functions);
	intf_delBlob(mib->intf, mib->class);
	pcb_timer_destroy(mib->timer);
	object_commit(plugin_intf2object(mib->intf));
	free(mib);
}

static void mib_instance_expire(pcb_timer_t *timer, void *userdata) {
	(void)timer;
	mib_instance_t *mib = userdata;
	mib_instance_destroy(mib);
}

static void mib_instance_delay(mib_instance_t *mib) {
	pcb_timer_start(mib->timer, 1);
}

static void mib_object_copy(intf_t *intf, mib_instance_t *mib, object_t *dst, object_t *src) {
	object_t *srcobj, *dstobj;
	object_for_each_child(srcobj, src) {
		dstobj = object_create(dst, object_name(srcobj, path_attr_key_notation), object_attributes(srcobj));
		if (!dstobj) {
			SAH_TRACE_ERROR("nemo - object_create(parent=%s, object=%s, attr=0x%x) failed"
					, object_name(dst, path_attr_key_notation), object_name(srcobj, path_attr_key_notation), object_attributes(srcobj));
			continue;
		}
		if (object_attributes(srcobj) & object_attr_template) {
			object_setMaxInstances(dstobj, object_getMaxInstances(srcobj));
			if (intf) {
				object_setInstanceAddHandler(dstobj, plugin_objectInstanceAddHandler);
				object_setDeleteHandler(dstobj, plugin_objectDeleteHandler);
			}
		}
		object_setACL(dstobj, object_getACL(srcobj));
		mib_object_copy(intf, NULL, dstobj, srcobj);
		if (mib)
			mib_item_create(&mib->objects, dstobj);
	}
	parameter_t *srcpar, *dstpar;
	object_for_each_parameter(srcpar, src) {
		dstpar = parameter_copy(dst,srcpar);
		if (!dstpar) {
			SAH_TRACE_ERROR("nemo - parameter_create(parent=%s, parameter=%s, type=%d, attr=0x%x) failed"
					, object_name(dst, path_attr_key_notation), parameter_name(srcpar),  parameter_type(srcpar),  parameter_attributes(srcpar));
			continue;
		}
		if (intf)
			parameter_setWriteHandler(dstpar, plugin_parameterWriteHandler);
		if (mib)
			mib_item_create(&mib->parameters, dstpar);
	}
	function_t *srcfun, *dstfun;
	object_for_each_function(srcfun, src) {
		dstfun = function_copy(dst,srcfun);
		if (!dstfun) {
				SAH_TRACE_ERROR("nemo - function_copy (parent=%s, function=%s, type=%s, attr=0x%x) failed"
						, object_name(dst, path_attr_key_notation), function_name(srcfun),  function_typeName(srcfun),  function_attributes(srcfun));
			continue;
		}
		if (mib)
			mib_item_create(&mib->functions, dstfun);
	}
}

static mib_instance_t *mib_instance_create(intf_t *intf, mib_class_t *class) {
	bool ok = false;
	SAH_TRACEZ_INFO("nemo", "Instantiating mib %s on intf %s", class->odlfile, intf_name(intf));
	mib_instance_t *mib = calloc(1, sizeof(mib_instance_t));
	if (!mib) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		goto leave;
	}
	mib->intf = intf;
	mib->class = class;
	mib->timer = pcb_timer_create();
	pcb_timer_setUserData(mib->timer, mib);
	pcb_timer_setHandler(mib->timer, mib_instance_expire);
	mib_object_copy(intf, mib, plugin_intf2object(intf), class->datamodel);
	intf_addBlob(intf, class, mib);
	object_commit(plugin_intf2object(intf));
	ok = true;
leave:
	if (!ok) {
		mib_instance_destroy(mib);
		mib = NULL;
	}
	return mib;
}

static void mib_class_destroy(mib_class_t *class) {
	intf_t *intf;
	if (!class)
		return;
	SAH_TRACE_NOTICE("nemo - Destroying mib class %s", class->odlfile);
	for (intf = intf_first(); intf; intf = intf_next(intf))
		mib_instance_destroy(intf_getBlob(intf, class));
	llist_iterator_take(&class->it);
	flagexp_destroy(class->flagexp);
	if (class->object) {
		object_delete(class->object);
		object_commit(class->object);
	}
	free(class->name);
	free(class->odlfile);
	free(class);
}

static mib_class_t *mib_class_create(const char *name, const char *odlfile) {
	bool ok = false;
	char *flag = NULL;
	char *odlpath = NULL;
	SAH_TRACEZ_INFO("nemo", "Loading mib class from %s", odlfile);
	mib_class_t *class = calloc(1, sizeof(mib_class_t));
	if (!class) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		arg_setError(pcb_error_no_memory, "calloc failed", "");
		goto leave;
	}
	class->odlfile = strdup(odlfile);
	if (!class->odlfile) {
		SAH_TRACE_ERROR("nemo - strdup(\"%s\") failed", odlfile);
		arg_setError(pcb_error_no_memory, "strdup failed", odlfile);
		goto leave;
	}
	class->name = strdup(name);
	if (!class->name) {
		SAH_TRACE_ERROR("nemo - strdup(\"%s\") failed", name);
		arg_setError(pcb_error_no_memory, "strdup failed", name);
		goto leave;
	}
	if (asprintf(&odlpath, "/usr/lib/nemo/mibs/%s", odlfile) == -1) {
		SAH_TRACE_ERROR("nemo - asprintf(/usr/lib/nemo/mibs/%%s, %s) failed", odlfile);
		arg_setError(pcb_error_no_memory, "asprintf failed", "");
		goto leave;
	}
	if (!datamodel_loadDefinition(pcb_datamodel(plugin_getPcb()), odlpath)) {
		if (!datamodel_loadDefinition(pcb_datamodel(plugin_getPcb()), odlfile)) {
			SAH_TRACE_ERROR("nemo - datamodel_loadDefinition(%s) failed", odlfile);
			arg_setError(pcb_error_invalid_value, "Could not load MIB file", odlfile);
			goto leave;
		}
	}
	class->object = pcb_getObject(plugin_getPcb(), "NeMo.MIB", 0);
	class->object = object_getObject(class->object, name, path_attr_key_notation, NULL);
	if (!class->object) {
		SAH_TRACE_ERROR("nemo - object_createInstance(NeMo.MIB, %s) failed", name);
		arg_setError(pcb_error_no_memory, "object_createInstance failed", name);
		goto leave;
	}
	flag = object_parameterCharValue(class->object, "Flag");
	class->flagexp = flagexp_create();
	if (!class->flagexp)
		goto leave;
	if (!flagexp_parse(class->flagexp, flag))
		goto leave;
	object_parameterSetCharValue(class->object, "OdlFile", odlfile);
	class->datamodel = object_getObject(class->object, "DataModel", 0, NULL);
	if (!class->datamodel) {
		SAH_TRACE_ERROR("nemo - NeMo.MIB.%s.DataModel not found", name);
		arg_setError(pcb_error_not_found, "DataModel object of MIB not found", name);
		goto leave;
	}
	object_commit(class->object);

	llist_append(&mib_classes, &class->it);
	ok = true;
leave:
	if (odlpath)
		free(odlpath);
	if (flag)
		free(flag);
	if (!ok) {
		mib_class_destroy(class);
		class = NULL;
	}
	return class;
}

static mib_class_t *mib_class_findByOdlfile(const char *odlfile) {
	mib_class_t *class;
	for (class = (mib_class_t *)llist_first(&mib_classes); class; class = (mib_class_t *)llist_iterator_next(&class->it)) {
		if (!strcmp(class->odlfile, odlfile))
			return class;
	}
	return NULL;
}

static mib_class_t *mib_class_findByName(const char *name) {
	mib_class_t *class;
	for (class = (mib_class_t *)llist_first(&mib_classes); class; class = (mib_class_t *)llist_iterator_next(&class->it)) {
		if (!strcmp(class->name, name))
			return class;
	}
	return NULL;
}

static void mib_checkIntf(intf_t *intf) {
	mib_class_t *class;
	mib_instance_t *mib;
	bool eval;
	for (class = (mib_class_t *)llist_first(&mib_classes); class;
			class = (mib_class_t *)llist_iterator_next(&class->it)) {
		eval = flagexp_evaluate(class->flagexp, intf_flagset(intf));
		mib = intf_getBlob(intf, class);
		if (eval && !mib)
			mib_instance_create(intf, class);
		else if (!eval && mib)
			mib_instance_delay(mib); // give modules the chance to clean up their pointers before destroying the data model
		else if (eval && mib)
			pcb_timer_stop(mib->timer);
	}
}

static void mib_intfCreateHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	mib_checkIntf(intf);
}

static void mib_intfDestroyHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	mib_class_t *class;
	mib_instance_t *mib;
	for (class = (mib_class_t *)llist_first(&mib_classes); class;
			class = (mib_class_t *)llist_iterator_next(&class->it)) {
		mib = intf_getBlob(intf, class);
		if (mib)
			mib_instance_destroy(mib);
	}
}

static void mib_flagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)flag; (void)value; (void)userdata;
	mib_checkIntf(intf);
}

bool mib_load(const char *odlfile) {
	bool ok = false;
	if (!odlfile) {
		SAH_TRACE_ERROR("nemo - odlfile is NULL");
		return arg_returnError(pcb_error_invalid_value, "odlfile is NULL", odlfile);
	}
	char *name = strdup(odlfile);
	if (!name) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", odlfile);
		arg_setError(pcb_error_no_memory, "strdup failed", odlfile);
		goto leave;
	}
	char *nameptr = strchr(basename(name), '.');
	if (nameptr)
		*nameptr = '\0';
	nameptr = basename(name);
	if (mib_class_findByName(nameptr)) {
		SAH_TRACE_ERROR("nemo - mib %s(%s) already loaded", nameptr, odlfile);
		arg_setError(pcb_error_invalid_value, "MIB already loaded", nameptr);
		goto leave;
	}
	mib_class_t *class = NULL;
	if (!(class = mib_class_create(nameptr, odlfile)))
		goto leave;
	intf_t *intf;
	for (intf = intf_first(); intf; intf = intf_next(intf)) {
		if (flagexp_evaluate(class->flagexp, intf_flagset(intf)))
			mib_instance_create(intf, class);
	}
	ok = true;
	arg_setSuccess();
leave:
	free(name);
	return ok;
}

bool mib_unload(const char *odlfile) {
	mib_class_t *class;
	intf_t *intf;
	if (!odlfile) {
		SAH_TRACE_ERROR("nemo - odlfile is NULL");
		return arg_returnError(pcb_error_invalid_value, "odlfile is NULL", odlfile);
	}
	if (!(class = mib_class_findByOdlfile(odlfile))) {
		SAH_TRACE_ERROR("nemo - mib %s not found", odlfile);
		return arg_returnError(pcb_error_not_found, "MIB not found", odlfile);
	}
	for (intf = intf_first(); intf; intf = intf_next(intf))
		mib_instance_destroy(intf_getBlob(intf, class));
	mib_class_destroy(class);
	return arg_returnSuccess();
}

static void variant_map_addParameter(variant_map_t *map, uint32_t uid, const char *key, parameter_t *parameter) {
	if (!parameter_canReadValue(parameter, uid)) {
		string_t path;
		string_initialize(&path, 64);
		object_path(parameter_owner(parameter), &path, path_attr_key_notation);
		SAH_TRACE_NOTICE("nemo - Read access to %s.%s denied for user %u", string_buffer(&path)?:"(null)", parameter_name(parameter), uid);
		string_cleanup(&path);
		return;
	}
	parameter_update(parameter);
	variant_map_add(map, key, parameter_getValue(parameter));
}

static void variant_map_addObject(variant_map_t *map, uint32_t uid, const char *key, object_t *object) {
	variant_map_t m;
	variant_map_initialize(&m);
	parameter_t *par;
	object_t *obj;
	if (object_attributes(object) & object_attr_template) {
		object_for_each_instance(obj, object) {
			variant_map_addObject(&m, uid, object_name(obj, path_attr_key_notation), obj);
		}
	} else {
		object_for_each_parameter(par, object) {
			variant_map_addParameter(&m, uid, parameter_name(par), par);
		}
		object_for_each_child(obj, object) {
			variant_map_addObject(&m, uid, object_name(obj, path_attr_key_notation), obj);
		}
	}
	variant_map_addMap(map, key, &m);
}

typedef struct _getMibs_ctx {
	flagset_t *mibs;
	flagexp_t *flag;
	mib_class_t *class;
	variant_map_t map;
	uint32_t uid;
} getMibs_ctx_t;

static bool mib_getBaseMibStep(intf_t *intf, void *userdata) {
	getMibs_ctx_t *ctx = (getMibs_ctx_t *)userdata;
	variant_map_t map;
	object_t *object = plugin_intf2object(intf);
	if (flagexp_evaluate(ctx->flag, intf_flagset(intf))) {
		variant_map_initialize(&map);
		variant_map_addParameter(&map, ctx->uid, "Name", object_getParameter(object, "Name"));
		variant_map_addParameter(&map, ctx->uid, "Enable", object_getParameter(object, "Enable"));
		variant_map_addParameter(&map, ctx->uid, "Status", object_getParameter(object, "Status"));
		variant_map_addParameter(&map, ctx->uid, "Flags", object_getParameter(object, "Flags"));
		variant_map_addObject(&map, ctx->uid, "ULIntf", object_getObject(object, "ULIntf", 0, NULL));
		variant_map_addObject(&map, ctx->uid, "LLIntf", object_getObject(object, "LLIntf", 0, NULL));
		variant_map_addMap(&ctx->map, intf_name(intf), &map);
		variant_map_cleanup(&map);
	}
	return false;
}

static bool mib_getMibsStep(intf_t *intf, void *userdata) {
	getMibs_ctx_t *ctx = (getMibs_ctx_t *)userdata;
	variant_map_t map;
	mib_instance_t *mib;
	if (flagexp_evaluate(ctx->flag, intf_flagset(intf))) {
		mib = intf_getBlob(intf, ctx->class);
		if (!mib)
			return false;
		variant_map_initialize(&map);
		mib_item_t *item;
		for (item = (mib_item_t *)llist_first(&mib->parameters); item; item = (mib_item_t *)llist_iterator_next(&item->it)) {
			variant_map_addParameter(&map, ctx->uid, parameter_name(item->u.parameter), item->u.parameter);
		}
		for (item = (mib_item_t *)llist_first(&mib->objects); item; item = (mib_item_t *)llist_iterator_next(&item->it)) {
			variant_map_addObject(&map, ctx->uid, object_name(item->u.object, path_attr_key_notation), item->u.object);
		}
		variant_map_addMap(&ctx->map, intf_name(intf), &map);
		variant_map_cleanup(&map);
	}
	return false;
}

void mib_getMibs(variant_t *result, uint32_t uid, intf_t *intf, flagset_t *mibs, flagexp_t *flag, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	variant_initialize(result, variant_type_map);
	variant_map_t *resultmap = variant_da_map(result);
	getMibs_ctx_t ctx = {mibs, flag, NULL, {NULL, NULL}, uid};
	if (!mibs || !flagset_count(mibs, NULL) || flagset_check(mibs, "base")) {
		variant_map_initialize(&ctx.map);
		traverse_tree_walk(tree, mib_getBaseMibStep, &ctx);
		variant_map_addMap(resultmap, "base", &ctx.map);
		variant_map_cleanup(&ctx.map);
	}
	for (ctx.class = (mib_class_t *)llist_first(&mib_classes); ctx.class; ctx.class = (mib_class_t *)llist_iterator_next(&ctx.class->it)) {
		if (mibs && flagset_count(mibs, NULL) && !flagset_check(mibs, ctx.class->name))
			continue;
		variant_map_initialize(&ctx.map);
		traverse_tree_walk(tree, mib_getMibsStep, &ctx);
		variant_map_addMap(resultmap, ctx.class->name, &ctx.map);
		variant_map_cleanup(&ctx.map);
	}
	traverse_tree_destroy(tree);
}

static bool mib_objectCanWrite(object_t *object, uint32_t uid) {
	if (object_isReadOnly(object)) {
		string_t path;
		string_initialize(&path, 64);
		object_path(object, &path, path_attr_key_notation);
		SAH_TRACE_WARNING("nemo - %s is read-only.", string_buffer(&path)?:"(null)");
		string_cleanup(&path);
		return false;
	}
	if (!object_canWrite(object, uid)) {
		string_t path;
		string_initialize(&path, 64);
		object_path(object, &path, path_attr_key_notation);
		SAH_TRACE_WARNING("nemo - Write access to %s denied for user %u", string_buffer(&path)?:"(null)", uid);
		string_cleanup(&path);
		return false;
	}
	return true;
}

static void mib_objectSetMap(object_t *object, uint32_t uid, variant_map_t *map) {
	variant_map_iterator_t *it;
	object_t *obj;
	parameter_t *par;
	string_t path;
	string_initialize(&path, 64);
	object_path(object, &path, path_attr_key_notation);

	if (object_isTemplate(object)) {
		object_for_each_instance(obj, object) {
			if (variant_map_contains(map, object_name(obj, path_attr_key_notation)))
				continue;
			if (!mib_objectCanWrite(obj, uid))
				continue;
			SAH_TRACEZ_INFO("nemo", "object_delete(template=%s instance=%s)",
			                string_buffer(&path)?:"(null)", object_name(obj, path_attr_key_notation));
			object_delete(obj);
		}
	}
	variant_map_for_each(it, map) {
		if (variant_type(variant_map_iterator_data(it)) == variant_type_map) {
			obj = object_getObject(object, variant_map_iterator_key(it), path_attr_key_notation, NULL);
			if (!obj && object_isTemplate(object)) {
				if (!mib_objectCanWrite(object, uid))
					continue;
				SAH_TRACEZ_INFO("nemo", "object_createInstance(template=%s instance=%s)",
				               string_buffer(&path)?:"(null)", variant_map_iterator_key(it));
				obj = object_createInstance(object, 0, variant_map_iterator_key(it));
			}
			if (!obj) {
				SAH_TRACE_WARNING("nemo - Object %s not found and could not be created", variant_map_iterator_key(it));
				continue;
			}
			mib_objectSetMap(obj, uid, variant_da_map(variant_map_iterator_data(it)));
		} else {
			par = object_getParameter(object, variant_map_iterator_key(it));
			if (!par) {
				SAH_TRACE_WARNING("nemo - Parameter %s not found", variant_map_iterator_key(it));
				continue;
			}
			int diff;
			if (parameter_isReadOnly(par)) {
				SAH_TRACE_WARNING("nemo - %s.%s is read-only.", string_buffer(&path)?:"(null)", parameter_name(par));
				continue;
			}
			if (!parameter_canWrite(par, uid)) {
				SAH_TRACE_WARNING("nemo - Write access to %s.%s is denied for user %u.",
				                   string_buffer(&path)?:"(null)", parameter_name(par), uid);
				continue;
			}
			if (variant_compare(parameter_getValue(par), variant_map_iterator_data(it), &diff) && !diff)
				continue;
			char *charvalue = variant_char(variant_map_iterator_data(it));
			SAH_TRACEZ_INFO("nemo", "parameter_setValue(path=%s parameter=%s value=%s)",
				string_buffer(&path)?:"(null)", variant_map_iterator_key(it), charvalue?:"(null)");
			if (!parameter_setValue(par, variant_map_iterator_data(it))) {
				SAH_TRACE_WARNING("nemo - parameter_setValue(path=%s parameter=%s value=%s) failed: %x(%s)",
					string_buffer(&path)?:"(null)", variant_map_iterator_key(it), charvalue?:"(null)",
					pcb_error, error_string(pcb_error));
			}
			free(charvalue);
		}
	}
	string_cleanup(&path);
}

void mib_setMibs(uint32_t uid, variant_map_t *mibs) {
	variant_map_iterator_t *it, *jt;
	variant_map_t *imap, *jmap;
	intf_t *intf;
	object_t *object;
	variant_map_for_each(it, mibs) {
		imap = variant_da_map(variant_map_iterator_data(it));
		if (!imap) {
			SAH_TRACE_NOTICE("nemo - Argument value for MIB Class %s is not a map", variant_map_iterator_key(it));
			continue;
		}
		variant_map_for_each(jt, imap) {
			intf = intf_find(variant_map_iterator_key(jt));
			if (!intf) {
				SAH_TRACE_NOTICE("nemo - Intf %s not found", variant_map_iterator_key(jt));
				continue;
			}

			jmap = variant_da_map(variant_map_iterator_data(jt));
			if (!jmap) {
				SAH_TRACE_NOTICE("nemo - Argument value for MIB Class %s / Intf %s is not a map", variant_map_iterator_key(it), variant_map_iterator_key(jt));
				continue;
			}
			object = plugin_intf2object(intf);
			mib_objectSetMap(object, uid, jmap);
			object_commit(object);
		}
	}
}

void mib_init() {
	intf_addFlagListener(NULL, NULL, mib_flagHandler, NULL, false);
	intf_addMgmtListener(NULL, mib_intfCreateHandler, NULL, mib_intfDestroyHandler, NULL, true);
}

void mib_cleanup() {
	intf_delFlagListener(NULL, NULL, mib_flagHandler, NULL, false);
	intf_delMgmtListener(NULL, mib_intfCreateHandler, NULL, mib_intfDestroyHandler, NULL, false);
	while (!llist_isEmpty(&mib_classes))
		mib_class_destroy((mib_class_t *)llist_first(&mib_classes));
	llist_cleanup(&mib_classes);
}

