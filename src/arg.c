/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>

#include <debug/sahtrace.h>

#include <pcb/common.h>
#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core.h"

static unsigned long error = 0;
static const char *errorDescription = NULL;
static char errorInfo[256] = "";

static uint32_t guid;

static bool arg_parseCheck(void **target, const char *string, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	if (!string && (attributes & ARG_MANDATORY)) {
		SAH_TRACE_WARNING("nemo - Mandatory argument %s missing", name?name:"(null)");
		return arg_returnError(pcb_error_invalid_value, "Mandatory argument missing", name);
	}
	return arg_returnSuccess();
}

bool arg_parseIntf(intf_t **target, const char *string, uint32_t attributes, const char *name) {
	if (!arg_parseCheck((void **)target, string, attributes, name))
		return false;
	if (!string)
		return arg_returnSuccess();
	*target = intf_find(string);
	if (!*target) {
		SAH_TRACE_WARNING("nemo - Intf \"%s\" does not exist.", string);
		return arg_returnError(pcb_error_invalid_value, "Intf does not exist", string);
	}
	return arg_returnSuccess();
}

bool arg_parseFlagset(flagset_t **target, const char *string, uint32_t attributes, const char *name) {
	if (!arg_parseCheck((void **)target, string, attributes, name))
		return false;
	if (!string)
		return arg_returnSuccess();
	*target = flagset_create(false);
	if (!*target)
		return arg_returnError(pcb_error_no_memory, "Out of memory", "");
	if (!flagset_parse(*target, string)) {
		SAH_TRACE_WARNING("nemo - \"%s\" is not a valid flagset", string);
		return arg_returnError(pcb_error_invalid_value, "Not a valid flagset", string);
	}
	return arg_returnSuccess();
}

bool arg_parseFlagexp(flagexp_t **target, const char *string, uint32_t attributes, const char *name) {
	if (!arg_parseCheck((void **)target, string, attributes, name))
		return false;
	if (!string)
		return arg_returnSuccess();
	*target = flagexp_create();
	if (!*target)
		return arg_returnError(pcb_error_no_memory, "Out of memory", "");
	if (!flagexp_parse(*target, string)) {
		SAH_TRACE_WARNING("nemo - \"%s\" is not a valid flagexp", string);
		return arg_returnError(pcb_error_invalid_value, "Not a valid flagexp", string);
	}
	return arg_returnSuccess();
}

bool arg_parseTraverse(traverse_mode_t *target, const char *string, uint32_t attributes, const char *name) {
	if (!arg_parseCheck((void **)target, string, attributes, name))
		return false;
	if (!string)
		return arg_returnSuccess();
	if (traverse_mode_parse(target, string))
		return arg_returnSuccess();
	else
		return arg_returnError(pcb_error_invalid_value, "Not a valid traverse mode", string);
}

bool arg_parseQueryClass(query_class_t **target, const char *string, uint32_t attributes, const char *name) {
	if (!arg_parseCheck((void **)target, string, attributes, name))
		return false;
	if (!string)
		return arg_returnSuccess();
	*target = query_class_find(string);
	if (!*target) {
		SAH_TRACE_WARNING("nemo - \"%s\" is not a valid query class", string);
		return arg_returnError(pcb_error_invalid_value, "Not a valid query class", string);
	}
	return arg_returnSuccess();
}

bool arg_take(argument_value_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	if (!name) {
		SAH_TRACE_ERROR("nemo - name is NULL!");
		return arg_returnError(pcb_error_invalid_value, "name is NULL", "");
	}
	if (!args)
		*target = NULL;
	else if (attributes & ARG_BYNAME)
		*target = argument_valueByName(args, name);
	else
		*target = argument_valueFirstArgument(args);
	if (*target) {
		argument_valueTakeArgument(*target);
	} else if (attributes & ARG_MANDATORY) {
		SAH_TRACE_WARNING("nemo - Mandatory argument %s missing", name);
		return arg_returnError(pcb_error_invalid_value, "Mandatory argument missing", name);
	}
	return arg_returnSuccess();
}

bool arg_takeChar(char **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	argument_value_t *arg = NULL;
	if (!arg_take(&arg, args, attributes, name))
		return false;
	if (arg)
		*target = variant_char(argument_value(arg));
	if (arg) argument_valueDestroy(arg);
	return arg_returnSuccess();
}

bool arg_takeMap(variant_map_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	argument_value_t *arg = NULL;
	if (!arg_take(&arg, args, attributes, name))
		return false;
	if (arg)
		*target = variant_map(argument_value(arg));
	if (arg) argument_valueDestroy(arg);
	return arg_returnSuccess();
}

bool arg_takeIntf(intf_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	char *arg = NULL;
	if (!arg_takeChar(&arg, args, attributes, name))
		return false;
	bool ret = arg_parseIntf(target, arg, attributes, name);
	free(arg);
	return ret;
}

bool arg_takeFlagset(flagset_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	char *arg = NULL;
	if (!arg_takeChar(&arg, args, attributes, name))
		return false;
	bool ret = arg_parseFlagset(target, arg, attributes, name);
	free(arg);
	return ret;
}

bool arg_takeFlagexp(flagexp_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	char *arg = NULL;
	if (!arg_takeChar(&arg, args, attributes, name))
		return false;
	bool ret = arg_parseFlagexp(target, arg, attributes, name);
	free(arg);
	return ret;
}

bool arg_takeTraverse(traverse_mode_t *target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	char *arg = NULL;
	if (!arg_takeChar(&arg, args, attributes, name))
		return false;
	bool ret = arg_parseTraverse(target, arg, attributes, name);
	free(arg);
	return ret;
}

bool arg_takeQueryClass(query_class_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	char *arg = NULL;
	if (!arg_takeChar(&arg, args, attributes, name))
		return false;
	bool ret = arg_parseQueryClass(target, arg, attributes, name);
	free(arg);
	return ret;
}

bool arg_takeUInt32(uint32_t *target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	argument_value_t *arg = NULL;
	if (!arg_take(&arg, args, attributes, name))
		return false;
	if (arg)
		*target = variant_uint32(argument_value(arg));
	if (arg) argument_valueDestroy(arg);
	return arg_returnSuccess();
}

bool arg_takeUInt16(uint16_t *target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	argument_value_t *arg = NULL;
	if (!arg_take(&arg, args, attributes, name))
		return false;
	if (arg)
		*target = variant_uint16(argument_value(arg));
	if (arg) argument_valueDestroy(arg);
	return arg_returnSuccess();
}

bool arg_takeUInt8(uint8_t *target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	argument_value_t *arg = NULL;
	if (!arg_take(&arg, args, attributes, name))
		return false;
	if (arg)
		*target = variant_uint8(argument_value(arg));
	if (arg) argument_valueDestroy(arg);
	return arg_returnSuccess();
}

bool arg_takeList(variant_map_t **target, argument_value_list_t *args, uint32_t attributes, const char *name) {
	if (!target) {
		SAH_TRACE_ERROR("nemo - target is NULL!");
		return arg_returnError(pcb_error_invalid_value, "target is NULL", "");
	}
	argument_value_t *arg = NULL;
	if (!arg_take(&arg, args, attributes, name))
		return false;
	if (arg)
		*target = variant_array(argument_value(arg));
	if (arg) argument_valueDestroy(arg);
	return arg_returnSuccess();
}

unsigned long arg_error() {
	return error;
}

const char *arg_errorDescription() {
	return errorDescription;
}

const char *arg_errorInfo() {
	return errorInfo;
}

void arg_setError(unsigned long e, const char *eDescription, const char *eInfo) {
	error = e;
	errorDescription = eDescription;
	snprintf(errorInfo, sizeof(errorInfo), "%s", eInfo?eInfo:"");
}

void arg_setSuccess() {
	error = 0;
	errorDescription = "";
	*errorInfo = '\0';
}

bool arg_returnError(unsigned long e, const char *eDescription, const char *eInfo) {
	error = e;
	errorDescription = eDescription;
	snprintf(errorInfo, sizeof(errorInfo), "%s", eInfo?eInfo:"");
	return false;
}

bool arg_returnSuccess() {
	error = 0;
	errorDescription = "";
	*errorInfo = '\0';
	return true;
}

uint32_t arg_getUserID() {
	return guid;
}

void arg_setUserID(uint32_t uid) {
	guid = uid;
}

void arg_clearUserID() {
	guid = 0;
}


