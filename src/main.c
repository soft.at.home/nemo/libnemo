/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>
#include <components.h>

#include "nemo/core/plugin.h"
#include "core/fun.h"
#include "core/mib.h"
#include "core/csi.h"

bool plugin_init(pcb_t *pcb);
void plugin_cleanup();
void plugin_load(const char *configfile);
void plugin_setAutoSave(bool enable);

static void pcb_plugin_load_deprecated_defaults() {
	const char *path = "/etc/nemo-defaults.odl";
	struct stat st_buf;

	if (lstat(path, &st_buf))
		return;
	if ((st_buf.st_mode&S_IFMT) != S_IFREG)
		return;
	plugin_load(path);
}

bool pcb_plugin_initialize(pcb_t *pcb, int argc, char *argv[]) {
	char *defaults = CONFIG_SAH_LIB_NEMO_DEFAULTS_PATH;

	SAH_TRACE_NOTICE("nemo - Initializing nemo plugin");

	if (argc == 2) {
		if (strcmp(argv[0],"--defaults") != 0) {
			SAH_TRACEZ_ERROR("nemo", "Argument %s not supported", argv[0]);
			return false;
		} 

		defaults = argv[1];

	} else if (argc != 0) {
		SAH_TRACEZ_ERROR("nemo", "Unexpected number of arguments");
		return false;
	}
	
	// initialize nemo core components
	if (!plugin_init(pcb))
		return false;
	mib_init();
	fun_init();
	csi_init();
	
	// load mibs at startup
	struct dirent *dirent = NULL;
	datamodel_t *datamodel = NULL;
	char *nemo_mib_path = NULL;
	DIR *nemo_mib_dir = NULL;
	const char *prefixpath = getenv("PCB_PREFIX_PATH");
	if (asprintf(&nemo_mib_path, "%s/usr/lib/nemo/mibs", prefixpath?prefixpath:"") == -1) {
		SAH_TRACE_ERROR("nemo - asprintf(%%s/usr/lib/nemo/mib, %s) failed", prefixpath?prefixpath:"");
		goto skipmibs;		 
	}
	nemo_mib_dir = opendir(nemo_mib_path);
	if (!nemo_mib_dir) {
		SAH_TRACE_ERROR("nemo - Could not open NeMo MIB directory %s", nemo_mib_path);
		goto skipmibs;
	}
	while ((dirent = readdir(nemo_mib_dir))) {
		if (!strstr(dirent->d_name, ".odl")) // only try odl files.
			continue;
		mib_load(dirent->d_name);
	}
skipmibs:
	if (datamodel)
		datamodel_destroy(datamodel);
	if (nemo_mib_dir)
		closedir(nemo_mib_dir);
	free(nemo_mib_path);

	// load defaults and persistent config
	intf_create("lo"); // mandatory intf
	SAH_TRACEZ_NOTICE("nemo", "Loading defaults from: %s", defaults);

	pcb_plugin_load_deprecated_defaults();
	plugin_load(defaults);
	plugin_load(CONFIG_RWDATAPATH"/nemo-config.odl");
	plugin_setAutoSave(true);

	return true;
}

void pcb_plugin_cleanup() {
	SAH_TRACE_NOTICE("nemo - Cleaning up nemo plugin");
	
	plugin_setAutoSave(false);
	csi_cleanup();
	fun_cleanup();
	mib_cleanup();
	plugin_cleanup();
	while (intf_first())
		intf_destroy(intf_first());
	
	// Flush write buffers. Rationale: allow modules to clean up their out-of-process state.
	connection_list_t conns = {NULL, NULL};
	connection_info_t *conninfo = pcb_connection(plugin_getPcb());
	connection_t *conn;
	bool needWrite;
	int ret;
	if (!conninfo) {
		SAH_TRACE_ERROR("nemo - pcb_connection(pcb) returned NULL");
		return;
	}
	llist_append(&conns, &conninfo->it);
	do {
		needWrite = false;
		for (conn = (connection_t *)llist_first(&conninfo->connections); conn; conn = (connection_t *)llist_iterator_next(&conn->it)) {
			if (peer_needWrite(&conn->info)) {
				needWrite = true;
				break;
			}
		}
		if (needWrite) {
			ret = connection_waitForEvents(&conns, NULL);
			if (ret > 0)
				ret = connection_handleEvents(&conns) ? 1 : 0;
		}
	} while (needWrite && ret > 0);
	llist_iterator_take(&conninfo->it);
}


