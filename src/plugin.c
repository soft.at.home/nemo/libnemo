/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <libgen.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>
#include <components.h>

#include "nemo/core.h"

#define NOTIFY_NEMO_QUERY (notify_custom + 1)
#define ME "nemo"

static int dummy;
static void *blobkey = &dummy;

static pcb_t *pcb = NULL;
static peer_info_t *peer = NULL;
static pcb_timer_t *savetimer = NULL;
static bool autosave = false;
static object_t *intfobject = NULL;

typedef struct _plugin_parameter_handlers {
	parameter_write_handler_t write;
	parameter_read_handler_t read;
} plugin_parameter_handlers_t;

struct _intf_blob {
	intf_t *intf;
	object_t *object;
	bool updatingFlags;
	bool updatingEnable;
	bool updatingStatus;
	bool deleting;
};

pcb_t *plugin_getPcb() {
	return pcb;
}

peer_info_t *plugin_getPeer() {
	return peer;
}

bool plugin_load(const char *configfile) {
	if (datamodel_load(pcb_datamodel(plugin_getPcb()), configfile, SERIALIZE_FORMAT(serialize_format_odl,0,0))) {
		SAH_TRACEZ_INFO("nemo", "Loaded config from %s", configfile);
		return true;
	} else {
		SAH_TRACE_WARNING("nemo - Config %s could not be loaded", configfile);
		return false;
	}
}

void plugin_save(const char *configfile) {
	if (datamodel_save(datamodel_root(pcb_datamodel(plugin_getPcb())), (uint32_t)-1, configfile,
	                   SERIALIZE_FORMAT(serialize_format_odl,0,0))) {
		SAH_TRACEZ_INFO("nemo", "Saved config to %s", configfile);
	} else {
		SAH_TRACE_ERROR("nemo - Config %s could not be saved", configfile);
	}
}

static void plugin_savehandler(pcb_timer_t *timer, void *userdata) {
	(void)timer; (void)userdata;
	static int counter = 0;
	counter++;
	SAH_TRACEZ_INFO("nemo", "AutoSave triggered for the %d%s time.", counter,
	                counter==1 ? "st" : counter==2 ? "nd": counter==3 ? "rd" : "th" );
	plugin_save(CONFIG_RWDATAPATH"/nemo-config.odl");
}

static void plugin_savetrigger(object_t *object, parameter_t *parameter) {
	if (!autosave)
		return;
	if (parameter && !(parameter_attributes(parameter) & parameter_attr_persistent))
		return;
	//object_t *objorig = object;
	while (object) {
		if (!(object_attributes(object) & object_attr_persistent))
			return;
		object = object_parent(object);
	}
	/*
	SAH_TRACEZ_INFO("nemo", "AutoSave scheduled because of %s.%s.",
	                  object_name(objorig, path_attr_key_notation), parameter_name(parameter)?:"");
	*/
	pcb_timer_start(savetimer, 100);
}

void plugin_setAutoSave(bool enable) {
	autosave = enable;
}

static inline intf_blob_t *plugin_getBlobFromObject(object_t *object) {
	return (intf_blob_t *)object_getUserData(object);
}

intf_t *plugin_object2intf(object_t *object) {
	intf_blob_t *blob = plugin_getBlobFromObject(object);
	return blob?blob->intf:NULL;
}

static inline intf_blob_t *plugin_getBlobFromIntf(intf_t *intf) {
	return (intf_blob_t *)intf_getBlob(intf, blobkey);
}

object_t *plugin_intf2object(intf_t *intf) {
	intf_blob_t *blob = plugin_getBlobFromIntf(intf);
	return blob?blob->object:NULL;
}

static bool plugin_object2parameterspec(intf_t **intf, char **spec, object_t *object, parameter_t *parameter) {
	char path[256];
	char *pathptr = &path[255];
	int n;
	*pathptr = '\0';
	if (parameter) {
		n = strlen(parameter_name(parameter));
		pathptr -= n;
		if (pathptr <= path)
			return false;
		memcpy(pathptr, parameter_name(parameter), n);
		*--pathptr = '.';
	}
	while (object_parent(object) != intfobject) {
		const char *objname = object_name(object, path_attr_key_notation);
		if (!objname)
			return false;
		n = strlen(objname);
		pathptr -= n;
		if (pathptr <= path)
			return false;
		memcpy(pathptr, objname, n);
		*--pathptr = '.';
		object = object_parent(object);
	}
	if (*pathptr)
		pathptr++;
	*spec = strdup(pathptr);
	if (!*spec) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", pathptr);
		return false;
	}
	*intf = plugin_object2intf(object);
	return true;
}

bool plugin_objectInstanceAddHandler(object_t *template, object_t *instance) {
	(void)template;
	if (!instance) {
		SAH_TRACE_ERROR("nemo - instance is NULL");
		return false;
	}
	intf_t *intf = NULL;
	char *spec = NULL;
	if (!plugin_object2parameterspec(&intf, &spec, instance, NULL))
		goto leave;
	intf_mibnotify(intf, intf_mibevent_added, spec);
	// plugin_savetrigger(instance, NULL); XXX: We don't know yet if this is a non-persistent instance!
leave:
	free(spec);
	return true;
}

bool plugin_objectDeleteHandler(object_t *object) {
	if (!object) {
		SAH_TRACE_ERROR("nemo - object is NULL");
		return false;
	}
	intf_t *intf = NULL;
	char *spec = NULL;
	if (!plugin_object2parameterspec(&intf, &spec, object, NULL))
		goto leave;
	intf_mibnotify(intf, intf_mibevent_deleted, spec);
	plugin_savetrigger(object, NULL);
leave:
	free(spec);
	return true;
}

static void plugin_intfCreateHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	SAH_TRACEZ_INFO("nemo", "plugin_intfCreateHandler: %s", intf_name(intf));
	intf_blob_t *blob = calloc(1, sizeof(intf_blob_t));
	if (!blob) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	blob->intf = intf;
	intf_addBlob(intf, blobkey, blob);
	blob->object = object_getObject(intfobject, intf_name(intf), path_attr_key_notation, NULL);
	if (!blob->object)
		blob->object = object_createInstance(intfobject, 0, intf_name(intf));
	if (!blob->object) {
		SAH_TRACE_ERROR("nemo - Could not create NeMo.Intf.%s", intf_name(intf));
		return;
	}
	object_setPersistent(blob->object, false);
	object_setUserData(blob->object, blob);
	object_parameterSetCharValue(blob->object, "Name", intf_name(intf));
	object_commit(blob->object);
}

void plugin_parameterWriteHandler(parameter_t *parameter, const variant_t *oldvalue) {
	plugin_parameter_handlers_t *handlers = parameter_getUserData(parameter);
	if (handlers && handlers->write)
		handlers->write(parameter, oldvalue);

	intf_t *intf = NULL;
	char *spec = NULL;
	if (!plugin_object2parameterspec(&intf, &spec, parameter_owner(parameter), parameter))
		goto leave;
	intf_mibnotify(intf, intf_mibevent_updated, spec);
	plugin_savetrigger(parameter_owner(parameter), parameter);
leave:
	free(spec);
}

bool plugin_parameterReadHandler(parameter_t *parameter, variant_t *value) {
	plugin_parameter_handlers_t *handlers = parameter_getUserData(parameter);
	if (handlers && handlers->read)
		return handlers->read(parameter, value);
	return true;
}

static void plugin_parameterDestroyHandler(parameter_t *parameter) {
	free(parameter_getUserData(parameter));
}

static plugin_parameter_handlers_t *plugin_claimParameterHandlers(parameter_t *parameter) {
	plugin_parameter_handlers_t *handlers = parameter_getUserData(parameter);
	if (!handlers) {
		handlers = (plugin_parameter_handlers_t *)calloc(1, sizeof(plugin_parameter_handlers_t));
		if (!handlers) {
			SAH_TRACE_ERROR("nemo - calloc failed");
			return NULL;
		}
		parameter_setUserData(parameter, handlers);
		parameter_setDestroyHandler(parameter, plugin_parameterDestroyHandler);
	}
	return handlers;
}

void plugin_setParameterWriteHandler(parameter_t *parameter, parameter_write_handler_t handler) {
	plugin_parameter_handlers_t *handlers = plugin_claimParameterHandlers(parameter);
	if (handlers)
		handlers->write = handler;
}

void plugin_setParameterReadHandler(parameter_t *parameter, parameter_read_handler_t handler) {
	plugin_parameter_handlers_t *handlers = plugin_claimParameterHandlers(parameter);
	if (handlers)
		handlers->read = handler;
	parameter_setReadHandler(parameter, plugin_parameterReadHandler);
}

static void plugin_intfDestroyHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	SAH_TRACEZ_INFO("nemo", "plugin_intfDestroyHandler: %s", intf_name(intf));
	intf_blob_t *blob = plugin_getBlobFromIntf(intf);
	if (!blob)
		return;
	if (!blob->deleting) {
		blob->deleting = true;
		object_delete(blob->object);
		object_commit(blob->object);
	}
	intf_delBlob(intf, blobkey);
	free(blob);
}

static void plugin_intfFlagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)userdata;
	SAH_TRACEZ_INFO("nemo", "plugin_intfFlagHandler: %s %s on %s", value?"set":"cleared", flag, intf_name(intf));
	intf_blob_t *blob = plugin_getBlobFromIntf(intf);
	if (!blob)
		return;
	
	if (!blob->updatingFlags) {
		blob->updatingFlags = true;
		char * flagstring = flagset_print(intf_flagset(intf));
		object_parameterSetCharValue(blob->object, "Flags", flagstring);
		free(flagstring);
		blob->updatingFlags = false;
	}
	
	if (!strcmp(flag, "enabled")) {
		if (!blob->updatingEnable) {
			blob->updatingEnable = true;
			object_parameterSetBoolValue(blob->object, "Enable", value);
		}
	} else if (!strcmp(flag, "up")) {
		if (!blob->updatingStatus) {
			blob->updatingStatus = true;
			object_parameterSetBoolValue(blob->object, "Status", value);
		}
	}
	object_commit(blob->object);
	blob->updatingEnable = false;
	blob->updatingStatus = false;
}

static void plugin_intfLinkHandler(intf_t *ulintf, intf_t *llintf, bool value, void *userdata) {
	object_t *object, *instance;
	(void)userdata;
	SAH_TRACEZ_INFO("nemo", "plugin_intfLinkHandler: %s dependency %s -> %s", value?"added":"removed", intf_name(ulintf), intf_name(llintf));
	
	object = object_getObject(plugin_intf2object(ulintf), "LLIntf", path_attr_key_notation, NULL);
	instance = object_getObject(object, intf_name(llintf), path_attr_key_notation, NULL);
	if (value) {
		if (!instance) {
			instance = object_createInstance(object, 0, intf_name(llintf));
			if (!instance) {
				SAH_TRACE_ERROR("nemo - object_createInstance(NeMo.Intf.%s.LLIntf, %s) failed", 
						intf_name(ulintf), intf_name(llintf));
				goto leaveDown;
			}
			object_setUserData(instance, (void *)true);
			object_commit(instance);
			object_setUserData(instance, (void *)false);
		}
	} else {
		if (instance && !(bool)object_getUserData(instance)) {
			object_setUserData(instance, (void *)true);
			object_delete(instance);
			object_commit(instance);
		}
	}
leaveDown:

	object = object_getObject(plugin_intf2object(llintf), "ULIntf", path_attr_key_notation, NULL);
	instance = object_getObject(object, intf_name(ulintf), path_attr_key_notation, NULL);
	if (value) {
		if (!instance) {
			instance = object_createInstance(object, 0, intf_name(ulintf));
			if (!instance) {
				SAH_TRACE_ERROR("nemo - object_createInstance(NeMo.Intf.%s.ULIntf, %s) failed", 
						intf_name(llintf), intf_name(ulintf));
				goto leaveUp;
			}
			object_setUserData(instance, (void *)true);
			object_commit(instance);
			object_setUserData(instance, (void *)false);
		}
	} else {
		if (instance && !(bool)object_getUserData(instance)) {
			object_setUserData(instance, (void *)true);
			object_delete(instance);
			object_commit(instance);
		}
	}
leaveUp:
	return;
}

static object_t *plugin_query2object(query_t *q) {
	static char key[20];
	object_t *object = plugin_intf2object(query_intf(q));
	object = object_getObject(object, "Query", path_attr_key_notation, NULL);
	sprintf(key, "%u", query_id(q));
	object = object_getObject(object, key, path_attr_key_notation, NULL);
	return object;
}

static void plugin_queryNotify(query_t *q) {
	object_t *object = plugin_query2object(q);
	notification_t *notification = notification_create(NOTIFY_NEMO_QUERY);
	notification_setName(notification, "query");
	notification_addParameter(notification, notification_parameter_createVariant("Result", query_result(q)));
	string_t s;
	string_initialize(&s, 256);
	object_path(object, &s, path_attr_key_notation);
	notification_setObjectPath(notification, string_buffer(&s));
	string_cleanup(&s);
	if (!pcb_sendNotification(pcb, NULL, NULL, notification)) {
		SAH_TRACE_ERROR("nemo - Failed to send notification: %d (%s)", pcb_error, error_string(pcb_error));
	}
	notification_destroy(notification);
}

static void plugin_queryOpenHandler(query_t *q, void *userdata) {
	(void)userdata;
	object_t *object = plugin_intf2object(query_intf(q));
	object = object_getObject(object, "Query", path_attr_key_notation, NULL);
	object = object_createInstance(object, query_id(q), NULL);
	object_setUserData(object, q);
	char *description = query_describe(q);
	object_parameterSetCharValue(object, "Description", description);
	free(description);
	string_t s;
	string_initialize(&s, 256);
	variant_toString(&s, query_result(q));
	object_parameterSetStringValue(object, "ResultString", &s);
	string_cleanup(&s);
	object_commit(object);
}

static void plugin_queryCloseHandler(query_t *q, void *userdata) {
	(void)userdata;
	object_t *object = plugin_query2object(q);
	object_delete(object);
	object_commit(object);
}

static void plugin_queryAlterHandler(query_t *q, void *userdata) {
	(void)userdata;
	object_t *object = plugin_query2object(q);
	char *subscribers = flagset_print(query_subscribers(q));
	object_parameterSetCharValue(object, "Subscribers", subscribers);
	free(subscribers);
	object_commit(object);
}

static void plugin_queryTriggerHandler(query_t *q, void *userdata) {
	(void)userdata;
	object_t *object = plugin_query2object(q);
	string_t s;
	string_initialize(&s, 256);
	variant_toString(&s, query_result(q));
	object_parameterSetStringValue(object, "ResultString", &s);
	string_cleanup(&s);
	object_commit(object);
	plugin_queryNotify(q);
}

static query_listener_t plugin_queryListener = {
	plugin_queryOpenHandler,
	plugin_queryCloseHandler,
	plugin_queryAlterHandler,
	plugin_queryTriggerHandler
};

static bool plugin_addIntf(object_t *object, object_t *instance) {
	(void)object;
	if (!intf_find(object_name(instance, path_attr_key_notation)))
		intf_create(object_name(instance, path_attr_key_notation)); // mgmt handler will do the rest
	return true;
}

static bool plugin_delIntf(object_t *object, object_t *instance) {
	(void)object;
	intf_blob_t *blob = plugin_getBlobFromObject(instance);
	if (blob && !blob->deleting) {
		blob->deleting = true;
		intf_destroy(blob->intf); // mgmt handler will do the rest
	}
	return true;
}

static void plugin_writeEnable(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_blob_t *blob = plugin_getBlobFromObject(parameter_owner(parameter));
	if (!blob)
		return;
	
	if (!blob->updatingEnable) {
		blob->updatingEnable = true;
	 	if (variant_bool(parameter_getValue(parameter)))
	 		flagset_set(intf_flagset(blob->intf), "enabled");
	 	else
	 		flagset_clear(intf_flagset(blob->intf), "enabled");
		blob->updatingEnable = false;
	}
	plugin_parameterWriteHandler(parameter, oldvalue);
}

static void plugin_writeIntf(object_t *object) {
	intf_blob_t *blob = plugin_getBlobFromObject(object);
	if (!blob)
		return;
	intf_modify(plugin_object2intf(object));
}

static void plugin_writeStatus(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_blob_t *blob = plugin_getBlobFromObject(parameter_owner(parameter));
	if (!blob)
		return;
	
	if (!blob->updatingStatus) {
		blob->updatingStatus = true;
	 	if (variant_bool(parameter_getValue(parameter)))
	 		flagset_set(intf_flagset(blob->intf), "up");
	 	else
	 		flagset_clear(intf_flagset(blob->intf), "up");
	 	blob->updatingStatus = false;
	}
	plugin_parameterWriteHandler(parameter, oldvalue);
}

static void plugin_writeFlags(parameter_t *parameter, const variant_t *oldvalue) {
	(void)oldvalue;
	intf_blob_t *blob = plugin_getBlobFromObject(parameter_owner(parameter));
	if (!blob)
		return;
	if (!blob->updatingFlags) {
		blob->updatingFlags = true;
		flagset_parse(intf_flagset(blob->intf), 
			string_buffer(variant_da_string(parameter_getValue(parameter))));
		blob->updatingFlags = false;
	}
	plugin_parameterWriteHandler(parameter, oldvalue);
}

static bool plugin_addULIntf(object_t *object, object_t *instance) {
	parameter_setFromChar(object_getParameter(instance, "Name"), object_name(instance, path_attr_key_notation));
	
	if (!(bool)object_getUserData(instance)) {
		intf_t *llintf = plugin_object2intf(object_parent(object));
		intf_t *ulintf = intf_find(object_name(instance, path_attr_key_notation));
		if (!ulintf) {
			SAH_TRACE_ERROR("nemo - NeMo.Intf.%s not found!", object_name(instance, path_attr_key_notation));
			return false;
		}
		intf_link(ulintf, llintf);
	}
	return true;
}

static bool plugin_delULIntf(object_t *object, object_t *instance) {
	if (!(bool)object_getUserData(instance)) {
		intf_t *llintf = plugin_object2intf(object_parent(object));
		intf_t *ulintf = intf_find(object_name(instance, path_attr_key_notation));
		if (!ulintf) {
			SAH_TRACE_ERROR("nemo - NeMo.Intf.%s not found!", object_name(instance, path_attr_key_notation));
			goto leave;
		}
		object_setUserData(instance, (void *)true);
		intf_unlink(ulintf, llintf);
	}
leave:
	return true;
}

static bool plugin_addLLIntf(object_t *object, object_t *instance) {
	parameter_setFromChar(object_getParameter(instance, "Name"), object_name(instance, path_attr_key_notation));
	
	if (!(bool)object_getUserData(instance)) {
		intf_t *ulintf = plugin_object2intf(object_parent(object));
		intf_t *llintf = intf_find(object_name(instance, path_attr_key_notation));
		if (!llintf) {
			SAH_TRACE_ERROR("nemo - NeMo.Intf.%s not found!", object_name(instance, path_attr_key_notation));
			return false;
		}
		intf_link(ulintf, llintf);
	}
	return true;
}

static bool plugin_delLLIntf(object_t *object, object_t *instance) {
	if (!(bool)object_getUserData(instance)) {
		intf_t *ulintf = plugin_object2intf(object_parent(object));
		intf_t *llintf = intf_find(object_name(instance, path_attr_key_notation));
		if (!llintf) {
			SAH_TRACE_ERROR("nemo - NeMo.Intf.%s not found!", object_name(instance, path_attr_key_notation));
			goto leave;
		}
		object_setUserData(instance, (void *)true);
		intf_unlink(ulintf, llintf);
	}
leave:
	return true;
}

bool plugin_init(pcb_t *pcbref) {
	pcb = pcbref;
	plugin_config_t *pluginconfig = pcb_pluginConfig(pcbref);
	if (!pluginconfig || !pluginconfig->configfile) {
		SAH_TRACE_ERROR("nemo - Fatal error: pcb has no valid plugin config");
		return false;
	}
	peer = pluginconfig->system_bus;
	
	intfobject = pcb_getObject(pcb, "NeMo.Intf", 0);
	object_setInstanceAddHandler(intfobject, plugin_addIntf);
	object_setInstanceDelHandler(intfobject, plugin_delIntf);
	object_setWriteHandler(intfobject, plugin_writeIntf);
	parameter_setWriteHandler(object_getParameter(intfobject, "Enable"), plugin_writeEnable);
	parameter_setWriteHandler(object_getParameter(intfobject, "Flags"), plugin_writeFlags);
	parameter_setWriteHandler(object_getParameter(intfobject, "Status"), plugin_writeStatus);
	object_setInstanceAddHandler(object_getObject(intfobject, "LLIntf", 0, NULL), plugin_addLLIntf);
	object_setInstanceDelHandler(object_getObject(intfobject, "LLIntf", 0, NULL), plugin_delLLIntf);
	object_setInstanceAddHandler(object_getObject(intfobject, "ULIntf", 0, NULL), plugin_addULIntf);
	object_setInstanceDelHandler(object_getObject(intfobject, "ULIntf", 0, NULL), plugin_delULIntf);
	
	intf_addMgmtListener(NULL, plugin_intfCreateHandler, NULL, plugin_intfDestroyHandler, NULL, true);
	intf_addFlagListener(NULL, NULL, plugin_intfFlagHandler, NULL, true);
	intf_addLinkListener(NULL, NULL, plugin_intfLinkHandler, NULL, true);
	
	query_addListener(NULL, &plugin_queryListener, NULL);

	savetimer = pcb_timer_create();
	pcb_timer_setHandler(savetimer, plugin_savehandler);
	
	return true;
}

void plugin_cleanup() {
	SAH_TRACE_NOTICE("nemo - Cleaning up nemo plugin");
	pcb_timer_destroy(savetimer);
	autosave = false;
	query_delListener(NULL, &plugin_queryListener, NULL);
	intf_delFlagListener(NULL, NULL, plugin_intfFlagHandler, NULL, true);
	intf_delLinkListener(NULL, NULL, plugin_intfLinkHandler, NULL, true);
	intf_delMgmtListener(NULL, plugin_intfCreateHandler, NULL, plugin_intfDestroyHandler, NULL, true);
}


