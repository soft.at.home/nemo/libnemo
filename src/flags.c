/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include "nemo/core/flags.h"

typedef struct _flag flag_t;

struct _flag {
	llist_iterator_t it;
	char *flag;
	int count;
};

struct _flagset {
	llist_t list;
	flagset_alert_handler alert_handler;
	void *alert_userdata;
	bool recursive;
	int count;
};

typedef enum _flag_operator {
	flag_operator_atom,
	flag_operator_and,
	flag_operator_or,
	flag_operator_not,
	flag_operator_group,
} flag_operator_t;

struct _flagexp {
	flagexp_t *parent;
	flag_operator_t operator;
	flagexp_t *left;
	flagexp_t *right;
	char * flag;
};

static void flagset_add(flagset_t *flagset, const char *flag, int flaglen, int count) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	if (!flag) {
		SAH_TRACE_ERROR("nemo - flag is NULL");
		return;
	}
	flag_t *f = (flag_t *)calloc(1, sizeof(flag_t));
	if (!f) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	if (flaglen) {
		f->flag = calloc(flaglen+1, 1);
		if (!f->flag) {
			SAH_TRACE_ERROR("nemo - calloc(%d, 1) failed", flaglen);
			free(f);
			return;
		}
		strncpy(f->flag, flag, flaglen);
	} else {
		f->flag = strdup(flag);
		if (!f->flag) {
			SAH_TRACE_ERROR("nemo - strdup(\"%s\") failed", flag);
			free(f);
			return;
		}
	}
	llist_append(&flagset->list, &f->it);
	f->count = count;
	flagset->count += count;
	if (flagset->alert_handler)
		flagset->alert_handler(flagset, f->flag, true, flagset->alert_userdata);
}

static void flagset_del(flagset_t *flagset, flag_t *f) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	if (!f) {
		SAH_TRACE_ERROR("nemo - f is NULL");
		return;
	}
	llist_iterator_take(&f->it);
	flagset->count -= f->count;
	if (flagset->alert_handler)
		flagset->alert_handler(flagset, f->flag, false, flagset->alert_userdata);
	free(f->flag);
	free(f);
}

flagset_t *flagset_create(bool recursive) {
	flagset_t *flagset = (flagset_t *)calloc(1, sizeof(flagset_t));
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	flagset->recursive = recursive;
	flagset->count = 0;
	return flagset;
}

void flagset_reset(flagset_t *flagset) {
	if (!flagset)
		return;
	while (!llist_isEmpty(&flagset->list))
		flagset_del(flagset, (flag_t *)llist_first(&flagset->list));
}

void flagset_destroy(flagset_t *flagset) {
	flagset_reset(flagset);
	free(flagset);
}

static flag_t *flagset_find(flagset_t *flagset, const char *flag) {
	if (!flagset)
		return NULL;
	if (!flag) {
		SAH_TRACE_ERROR("nemo - flag is NULL");
		return NULL;
	}
	flag_t *f;
	for (f = (flag_t *)llist_first(&flagset->list); f; f = (flag_t *)llist_iterator_next(&f->it)) {
		if (!strcmp(f->flag, flag))
			return f;
	}
	return NULL;
}

bool flagset_parse(flagset_t *flagset, const char *str) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return false;
	}
	flag_t *f;
	flagset_t *newflags = flagset_create(flagset->recursive);
	if (str && newflags) {
		const char *strptr = str;
		char *newstr;
		int count;
		do {
			if (!(*str == '\0' || *str == ' ' || *str == '\t' || (flagset->recursive && *str == ':')))
				continue;
			if (str > strptr) {
				if (*str == ':') {
					count = strtol(str + 1, &newstr, 0);
					flagset_add(newflags, strptr, str - strptr, count);
					str = newstr;
				} else {
					flagset_add(newflags, strptr, str - strptr, 1);
				}
			}
			strptr = str + 1;
		} while (*str++);
	}
	// intersect + union instead of simply overwrite to avoid that unchanged flags are cleared and set again
	flagset_intersect(flagset, newflags);
	flagset_union(flagset, newflags);
	if (flagset->recursive) { // overwrite all counters
		flagset->count = 0;
		for (f = (flag_t *)llist_first(&flagset->list); f; f = (flag_t *)llist_iterator_next(&f->it)) {
			f->count = flagset_count(newflags, f->flag);
			flagset->count += f->count;
		}
	}
	flagset_destroy(newflags);
	return true;
}

char *flagset_print(flagset_t *flagset) {
	if (!flagset)
		return NULL;
	int n=0;
	char *buf, *bufptr;
	char countbuf[16];
	int started = 0;
	flag_t *f;
	for (f = (flag_t *)llist_first(&flagset->list); f; f = (flag_t *)llist_iterator_next(&f->it)) {
		n += strlen(f->flag) + started;
		if (f->count != 1 && flagset->recursive)
			n += sprintf(countbuf, ":%d", f->count);
		started = 1;
	}
	bufptr = buf = calloc(n + 1, 1);
	if (!buf) {
		SAH_TRACE_ERROR("nemo - calloc(%d, 1) failed", n + 1);
		return NULL;
	}
	started = 0;
	for (f = (flag_t *)llist_first(&flagset->list); f; f = (flag_t *)llist_iterator_next(&f->it)) {
		if (f->count != 1 && flagset->recursive)
			bufptr += sprintf(bufptr, "%s%s:%d", started?" ":"", f->flag, f->count);
		else
			bufptr += sprintf(bufptr, "%s%s", started?" ":"", f->flag);		
		started = 1;
	}
	return buf;
}

flagset_iterator_t *flagset_iterator(flagset_t *flagset) {
	if (!flagset)
		return NULL;
	return (flagset_iterator_t *)llist_first(&flagset->list);
}

flagset_iterator_t *flagset_iterator_next(flagset_iterator_t *it) {
	if (!it) {
		SAH_TRACE_ERROR("nemo - it is NULL");
		return NULL;
	}
	return (flagset_iterator_t *)llist_iterator_next(&((flag_t *)it)->it);
}

const char *flagset_iterator_current(flagset_iterator_t *it) {
	if (!it) {
		SAH_TRACE_ERROR("nemo - it is NULL");
		return NULL;
	}
	return ((flag_t *)it)->flag;
}

void flagset_set(flagset_t *flagset, const char *flag) {
	flag_t *f;
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	if (!(f = flagset_find(flagset, flag))) {
		flagset_add(flagset, flag, 0, 1);
	} else if (flagset->recursive) {
		f->count++;
		flagset->count++;
	}
}

void flagset_clear(flagset_t *flagset, const char *flag) {
	flag_t *f = flagset_find(flagset, flag);
	if (!f)
		return;
	if (flagset->recursive && f->count > 1) {
		f->count--;
		flagset->count--;
	} else {		
		flagset_del(flagset, f);
	}
}

bool flagset_check(flagset_t *flagset, const char *flag) {
	return flagset_find(flagset, flag) ? true : false;
}

int flagset_count(flagset_t *flagset, const char *flag) {
	if (!flagset)
		return 0;
	if (flag) {
		flag_t *f = flagset_find(flagset, flag);
		return f ? f->count : 0;
	} else {
		return flagset->count;
	}
}

void flagset_union(flagset_t *flagset, flagset_t *operand) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	if (!operand)
		return;
	flag_t *f, *fo;
	for (fo = (flag_t *)llist_first(&operand->list); fo; fo = (flag_t *)llist_iterator_next(&fo->it)) {
		if (!(f = flagset_find(flagset, fo->flag))) {
			flagset_add(flagset, fo->flag, 0, flagset->recursive ? fo->count : 1);
		} else if (flagset->recursive) {
			flagset->count += fo->count;
			f->count += fo->count;
		}
	}
}

void flagset_intersect(flagset_t *flagset, flagset_t *operand) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	if (!operand) {
		flagset_reset(flagset);
		return;
	}
	flag_t *f, *fo;
	flag_t *fnext;
	for(f = (flag_t *)llist_first(&flagset->list); f; f = fnext) {
		fnext = (flag_t *)llist_iterator_next(&f->it);
		if (!(fo = flagset_find(operand, f->flag))) {
			flagset_del(flagset, f);
		} else if (flagset->recursive && fo->count < f->count) {
			flagset->count += fo->count - f->count;
			f->count = fo->count;
		}
	}
}

void flagset_subtract(flagset_t *flagset, flagset_t *operand) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	if (!operand)
		return;
	flag_t *f, *fo;
	for (fo = (flag_t *)llist_first(&operand->list); fo; fo = (flag_t *)llist_iterator_next(&fo->it)) {
		if (!(f = flagset_find(flagset, fo->flag)))
			continue;
		if (flagset->recursive && f->count > fo->count) {
			flagset->count -= fo->count;
			f->count -= fo->count;
		} else {
			flagset_del(flagset, f);
		}
	}
}

bool flagset_equals(flagset_t *flagset1, flagset_t *flagset2) {
	flag_t *f;
	if (flagset1) for (f = (flag_t *)llist_first(&flagset1->list); f; f = (flag_t *)llist_iterator_next(&f->it))
		if (flagset_count(flagset2, f->flag) != f->count)
			return false;
	if (flagset2) for (f = (flag_t *)llist_first(&flagset2->list); f; f = (flag_t *)llist_iterator_next(&f->it))
		if (flagset_count(flagset1, f->flag) != f->count)
			return false;
	return true;
}

void flagset_alert(flagset_t *flagset, flagset_alert_handler handler, void *userdata) {
	if (!flagset) {
		SAH_TRACE_ERROR("nemo - flagset is NULL");
		return;
	}
	flagset->alert_handler = handler;
	flagset->alert_userdata = userdata;
}



flagexp_t *flagexp_create() {
	flagexp_t *flagexp = (flagexp_t *)calloc(1, sizeof(flagexp_t));
	if (!flagexp) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	flagexp->operator = flag_operator_group;
	return flagexp;
}

void flagexp_reset(flagexp_t *flagexp) {
	if (!flagexp)
		return;
	free(flagexp->flag);
	flagexp->flag = NULL;
	flagexp_destroy(flagexp->left);
	flagexp->left = NULL;
	flagexp_destroy(flagexp->right);
	flagexp->right = NULL;
	flagexp->operator = flag_operator_group;
}

void flagexp_destroy(flagexp_t *flagexp) {
	flagexp_reset(flagexp);
	free(flagexp);
}

#define FLAG_TOKEN_LEFTOP  1
#define FLAG_TOKEN_RIGHTOP 2

typedef enum _flag_token {
	flag_token_and,
	flag_token_or,
	flag_token_not,
	flag_token_openbracket,
	flag_token_closebracket,
	flag_token_whitespace,
	flag_token_default,
	flag_token_finish,
} flag_token_t;

typedef struct _flag_token_desc {
	flag_token_t token;
	const char * str;
	int len;
	int flags;
	flag_operator_t operator;
} flag_token_desc_t;

static flag_token_desc_t flag_token_desc_table[] = {
	{flag_token_and,          "&&", 2, FLAG_TOKEN_LEFTOP | FLAG_TOKEN_RIGHTOP , flag_operator_and   },
	{flag_token_or,           "||", 2, FLAG_TOKEN_LEFTOP | FLAG_TOKEN_RIGHTOP , flag_operator_or    },
	{flag_token_not,          "!",  1, FLAG_TOKEN_RIGHTOP                     , flag_operator_not   },
	{flag_token_openbracket,  "(",  1, FLAG_TOKEN_RIGHTOP                     , flag_operator_group },
	{flag_token_closebracket, ")",  1, FLAG_TOKEN_LEFTOP                      , flag_operator_group },
	{flag_token_whitespace,   " ",  1, 0                                      , flag_operator_atom  },
	{flag_token_whitespace,   "\t", 1, 0                                      , flag_operator_atom  },
};
static const int flag_token_desc_table_size = 7;
static flag_token_desc_t flag_token_desc_default = 
	{flag_token_default,      "",   1, 0                                      , flag_operator_atom  };
static flag_token_desc_t flag_token_desc_finish = 
	{flag_token_finish,       "",   0, FLAG_TOKEN_LEFTOP                      , flag_operator_group };
static flag_token_desc_t flag_token_desc_default_binary = 
	{flag_token_and,          "&&", 2, FLAG_TOKEN_LEFTOP | FLAG_TOKEN_RIGHTOP , flag_operator_and   };


bool flagexp_parse(flagexp_t *flagexp, const char *str) {
	if (!flagexp) {
		SAH_TRACE_ERROR("nemo - flagexp is NULL");
		return false;
	}
	bool ret = false;
	const char * strptr = str;
	flagexp_t *cur = flagexp;
	flagexp_t *f = NULL;
	flag_token_desc_t *t = NULL;
	int i;
	
	flagexp_reset(flagexp);
	if (!str)
		return true;
	do {
		if (!*str) {
			t = &flag_token_desc_finish;
		} else {
			for (i=0; i<flag_token_desc_table_size; i++) {
				if (!strncmp(flag_token_desc_table[i].str, str, flag_token_desc_table[i].len)) {
					t = &flag_token_desc_table[i];
					break;
				}
			}
			if (i == flag_token_desc_table_size)
				t = &flag_token_desc_default;
		}
		
		if (!(t->flags & FLAG_TOKEN_LEFTOP) && t->token != flag_token_whitespace && cur->right) {
			t = &flag_token_desc_default_binary;
			str -= t->len;
		}
		
		if (((t->flags & FLAG_TOKEN_LEFTOP) || (t->token == flag_token_whitespace && strptr && strptr!=str)) && !cur->right) {
			if (strptr && strptr != str) {
				f = calloc(1, sizeof(flagexp_t));
				if (!f) {
					SAH_TRACE_ERROR("nemo - calloc failed");
					goto leave;
				}
				f->parent = cur;
				f->flag = calloc(str - strptr + 1, 1);
				if (!f->flag) {
					SAH_TRACE_ERROR("nemo - calloc failed");
					goto leave;
				}
				strncpy(f->flag, strptr, str - strptr);
				f->operator = flag_operator_atom;
				cur->right = f;
				f = NULL;
			}
			strptr = NULL;
			while (cur->operator != flag_operator_group) {
				if (!cur->parent) {
					SAH_TRACE_ERROR("nemo - Syntax error: Check brackets!");
					goto leave;
				}
				cur = cur->parent;
			}
		}
		if (t->token == flag_token_whitespace) {
			if (strptr==str)
				strptr += t->len;
		}
			

		if (t->flags & FLAG_TOKEN_RIGHTOP) {
			f = calloc(1, sizeof(flagexp_t));
			if (!f) {
				SAH_TRACE_ERROR("nemo - calloc failed");
				goto leave;
			}
			f->parent = cur;
			f->operator = t->operator;
			if (t->flags & FLAG_TOKEN_LEFTOP)
				f->left = cur->right;
			cur->right = f;
			cur = f;
			f = NULL;
			strptr = str + t->len;
		} else if (t->token == flag_token_closebracket) {
			if (!cur->parent) {
				SAH_TRACE_ERROR("nemo - Syntax error: Check brackets!");
				goto leave;
			}
			cur = cur->parent;
			while (cur->operator != flag_operator_group) {
				if (!cur->parent) {
					SAH_TRACE_ERROR("nemo - Syntax error: Check brackets!");
					goto leave;
				}
				cur = cur->parent;
			}
		} else if (t->token == flag_token_finish) {
			if (cur != flagexp) {
				SAH_TRACE_ERROR("nemo - Syntax error: Check brackets!");
				goto leave;
			}
		}
		
		str += t->len;
	} while (t->token != flag_token_finish);
	ret = true;
leave:
	if (!ret) {
		flagexp_destroy(f);
		flagexp_reset(flagexp);
	}
	return ret;
}

static int flagexp__print(flagexp_t *flagexp, char *buf) {
	int n=0, m;
	if (!flagexp)
		return n;
	switch (flagexp->operator) {
		case flag_operator_atom:
			if (buf)
				buf += sprintf(buf, "%s", flagexp->flag);
			n += strlen(flagexp->flag);
			break;
		case flag_operator_and:
			n += m = flagexp__print(flagexp->left, buf);
			if (buf)
				buf += m;
			n += 4;
			if (buf)
				buf += sprintf(buf, " && ");
			n += m = flagexp__print(flagexp->right, buf);
			if (buf)
				buf += m;
			break;
		case flag_operator_or:
			n += m = flagexp__print(flagexp->left, buf);
			if (buf)
				buf += m;
			n += 4;
			if (buf)
				buf += sprintf(buf, " || ");
			n += m = flagexp__print(flagexp->right, buf);
			if (buf)
				buf += m;
			break;
		case flag_operator_not:
			n += 1;
			if (buf)
				buf += sprintf(buf, "!");
			n += m = flagexp__print(flagexp->right, buf);
			if (buf)
				buf += m;
			break;
		case flag_operator_group:
			n += 1;
			if (buf)
				buf += sprintf(buf, "(");
			n += m = flagexp__print(flagexp->right, buf);
			if (buf)
				buf += m;
			n += 1;
			if (buf)
				buf += sprintf(buf, ")");
			break;
	}
	(void)buf;
	return n;
}

char *flagexp_print(flagexp_t *flagexp) {
	if (!flagexp)
		return NULL;
	if (flagexp->operator == flag_operator_group && flagexp->right) // skip outer brackets
		return flagexp_print(flagexp->right);
	int n = flagexp__print(flagexp, NULL);
	char *buf = calloc(n + 1, 1);
	if (!buf) {
		SAH_TRACE_ERROR("nemo - calloc(%d, 1) failed", n + 1);
		return NULL;
	}
	flagexp__print(flagexp, buf);
	return buf;
}

bool flagexp_evaluate(flagexp_t *flagexp, flagset_t *flagset) {
	if (!flagexp)
		return true;
	switch (flagexp->operator) {
		case flag_operator_atom:
			return flagset_check(flagset, flagexp->flag);
		case flag_operator_and:
			return flagexp_evaluate(flagexp->left, flagset) && flagexp_evaluate(flagexp->right, flagset);
		case flag_operator_or:
			return flagexp_evaluate(flagexp->left, flagset) || flagexp_evaluate(flagexp->right, flagset);
		case flag_operator_not:
			return !flagexp_evaluate(flagexp->right, flagset);
		case flag_operator_group:
			return flagexp_evaluate(flagexp->right, flagset);
		default:
			SAH_TRACE_ERROR("nemo - Illegal flagexp operator: %d", flagexp->operator);
			return false;
	}
}

bool flagexp_contains(flagexp_t *flagexp, const char *flag) {
	if (!flagexp)
		return false;
	switch (flagexp->operator) {
		case flag_operator_atom:
			return !strcmp(flagexp->flag, flag);
		case flag_operator_and:
		case flag_operator_or:
			return flagexp_contains(flagexp->left, flag) || flagexp_contains(flagexp->right, flag);
		case flag_operator_not:
		case flag_operator_group:
			return flagexp_contains(flagexp->right, flag);
		default:
			SAH_TRACE_ERROR("nemo - Illegal flagexp operator: %d", flagexp->operator);
			return 0;
	}
	
}

bool flagexp_equals(flagexp_t *flagexp1, flagexp_t *flagexp2) {
	if ((flagexp1?1:0) != (flagexp2?1:0))
		return false;
	if (!flagexp1 && !flagexp2)
		return true;
	if (flagexp1->operator != flagexp2->operator)	
		return false;
	switch (flagexp1->operator) {
		case flag_operator_atom:
			return !strcmp(flagexp1->flag, flagexp2->flag);
		case flag_operator_and:
		case flag_operator_or:
			return (flagexp_equals(flagexp1->left, flagexp2->left) && flagexp_equals(flagexp1->right, flagexp2->right))
				|| (flagexp_equals(flagexp1->left, flagexp2->right) && flagexp_equals(flagexp1->right, flagexp2->left));
		case flag_operator_not:
		case flag_operator_group:
			return flagexp_equals(flagexp1->right, flagexp2->right);
		default:
			SAH_TRACE_ERROR("nemo - Illegal flagexp operator: %d", flagexp1->operator);
			return 0;
	}
}


