/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include "nemo/core/flags.h"
#include "nemo/core/intf.h"

struct _intf {
	llist_iterator_t it;
	char *name;
	flagset_t *flagset;
	llist_t llintfs;
	llist_t ulintfs;
	llist_t blobs;
};

struct _intf_link {
	llist_iterator_t it;
	intf_t *intf;
};

typedef struct _blobentry {
	llist_iterator_t it;
	void *key;
	intf_blob_t *blob;
} blobentry_t;

typedef struct _flaglistener {
	llist_iterator_t it;
	intf_t *intf;
	char *flag;
	intf_flaglistener function;
	void *userdata;
} flaglistener_t;

typedef struct _linklistener {
	llist_iterator_t it;
	intf_t *ulintf;
	intf_t *llintf;
	intf_linklistener function;
	void *userdata;
} linklistener_t;

typedef struct _mgmtlistener {
	llist_iterator_t it;
	intf_t *intf;
	intf_mgmtlistener create;
	intf_mgmtlistener modify;
	intf_mgmtlistener destroy;
	void *userdata;
} mgmtlistener_t;

typedef struct _miblistener {
	llist_iterator_t it;
	intf_t *intf;
	intf_miblistener function;
	void *userdata;
} miblistener_t;

typedef enum _mgmtevent {
	mgmtevent_create,
	mgmtevent_modify,
	mgmtevent_destroy,
} mgmtevent_t;

static llist_t intfs = {NULL, NULL};

static llist_t flaglisteners = {NULL, NULL};
static llist_t linklisteners = {NULL, NULL};
static llist_t mgmtlisteners = {NULL, NULL};
static llist_t miblisteners = {NULL, NULL};

static void intf_flagset_alert(flagset_t *flagset, const char *flag, bool value, void *userdata) {
	(void)flagset;
	intf_t *intf = (intf_t *)userdata;
	llist_iterator_t *(*start)(const llist_t *list) = value ? llist_first : llist_last;
	llist_iterator_t *(*step)(const llist_iterator_t *it) = value ? llist_iterator_next : llist_iterator_prev;
	flaglistener_t *l, *lnext;
	for (l = (flaglistener_t *)start(&flaglisteners); l; l = lnext) {
		lnext = (flaglistener_t *)step(&l->it);
		if (l->intf && l->intf != intf)
			continue;
		if (l->flag && strcmp(l->flag, flag))
			continue;
		l->function(intf, flag, value, l->userdata);
	}
}

static void intf_mgmt_notify(intf_t *intf, mgmtevent_t event) {
	llist_iterator_t *(*start)(const llist_t *list) = event!=mgmtevent_destroy ? llist_first : llist_last;
	llist_iterator_t *(*step)(const llist_iterator_t *it) = event!=mgmtevent_destroy ? llist_iterator_next : llist_iterator_prev;
	mgmtlistener_t *l, *lnext;
	for (l = (mgmtlistener_t *)start(&mgmtlisteners); l; l = lnext) {
		lnext = (mgmtlistener_t *)step(&l->it);
		if (l->intf && l->intf != intf)
			continue;
		switch (event) {
			case mgmtevent_create:  if (l->create)  l->create(intf,  l->userdata); break;
			case mgmtevent_modify:  if (l->modify)  l->modify(intf,  l->userdata); break;
			case mgmtevent_destroy: if (l->destroy) l->destroy(intf, l->userdata); break;
		}
	}
}

intf_t* intf_create(const char *name) {
	if (!name) {
		SAH_TRACE_ERROR("nemo - name is NULL");
		return NULL;
	}
	if (intf_find(name)) {
		SAH_TRACE_ERROR("nemo - intf %s already exists", name);
		return NULL;
	}
	intf_t *intf = calloc(1, sizeof(intf_t));
	if (!intf) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		goto error;
	}
	intf->name = strdup(name);
	if (!intf->name) {
		SAH_TRACE_ERROR("nemo - strdup(\"%s\") failed", name);
		goto error;
	}
	intf->flagset = flagset_create(false);
	if (!intf->flagset)
		goto error;
	flagset_alert(intf->flagset, intf_flagset_alert, intf);
	llist_append(&intfs, &intf->it);
	intf_mgmt_notify(intf, mgmtevent_create);
	return intf;
error:
	if (intf) {
		flagset_destroy(intf->flagset);
		free(intf->name);
	}
	free(intf);
	return NULL;
}

void intf_destroy(intf_t *intf) {
	if (!intf)
		return;
	while (!llist_isEmpty(&intf->llintfs))
		intf_unlink(intf, ((intf_link_t *)llist_first(&intf->llintfs))->intf);
	while (!llist_isEmpty(&intf->ulintfs))
		intf_unlink(((intf_link_t *)llist_first(&intf->ulintfs))->intf, intf);
	flagset_reset(intf->flagset);
	intf_mgmt_notify(intf, mgmtevent_destroy);
	flagset_destroy(intf->flagset);

	/* We clean up the blobs, but in fact the list of blobs should be empty: whoever attached any blob, it should be detached
	 * again by now. By doing the cleanup here, not knowing what the blobs actually are, we are probably leaking memory.
	 * Blobs don't have overridable destructors, but it's also not needed. Modules have had chances enough to clean up their mess
	 * by now. In fact we should log a warning for each blob that needs to cleaned up here, and let libmafia resolve the key.
	 * If the key is a static member of a module, we might get an ID of the polluter.
	 * (NOTE: libmafia is no longer preloaded)
	 */
	while (!llist_isEmpty(&intf->blobs))
		free(llist_iterator_take(llist_first(&intf->blobs)));

	flaglistener_t *fl, *flnext;
	for (fl = (flaglistener_t *)llist_first(&flaglisteners); fl; fl = flnext) {
		flnext = (flaglistener_t *)llist_iterator_next(&fl->it);
		if (fl->intf == intf)
			free(llist_iterator_take(&fl->it));
	}
	linklistener_t *ll, *llnext;
	for (ll = (linklistener_t *)llist_first(&linklisteners); ll; ll = llnext) {
		llnext = (linklistener_t *)llist_iterator_next(&ll->it);
		if (ll->llintf == intf || ll->ulintf == intf)
			free(llist_iterator_take(&ll->it));
	}
	mgmtlistener_t *ml, *mlnext;
	for (ml = (mgmtlistener_t *)llist_first(&mgmtlisteners); ml; ml = mlnext) {
		mlnext = (mgmtlistener_t *)llist_iterator_next(&ml->it);
		if (ml->intf == intf)
			free(llist_iterator_take(&ml->it));
	}
	miblistener_t *mibl, *miblnext;
	for (mibl = (miblistener_t *)llist_first(&miblisteners); mibl; mibl = miblnext) {
		miblnext = (miblistener_t *)llist_iterator_next(&mibl->it);
		if (mibl->intf == intf)
			free(llist_iterator_take(&mibl->it));
	}
	llist_iterator_take(&intf->it);
	free(intf->name);
	free(intf);
}

void intf_modify(intf_t *intf) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return;
	}
	intf_mgmt_notify(intf, mgmtevent_modify);
}

void intf_mibnotify(intf_t *intf, intf_mibevent_t event, const char *path) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return;
	}
	if (!path) {
		SAH_TRACE_ERROR("nemo - path is NULL");
		return;
	}
	llist_iterator_t *(*start)(const llist_t *list) = event!=intf_mibevent_deleted ? llist_first : llist_last;
	llist_iterator_t *(*step)(const llist_iterator_t *it) = event!=intf_mibevent_deleted ? llist_iterator_next : llist_iterator_prev;
	miblistener_t *l, *lnext;
	for (l = (miblistener_t *)start(&miblisteners); l; l = lnext) {
		lnext = (miblistener_t *)step(&l->it);
		if (l->intf && l->intf != intf)
			continue;
		l->function(intf, event, path, l->userdata);
	}
}

intf_t* intf_find(const char *name) {
	if (!name) {
		SAH_TRACE_ERROR("nemo - name is NULL");
		return NULL;
	}
	intf_t *intf;
	for (intf = (intf_t *)llist_first(&intfs); intf; intf = (intf_t *)llist_iterator_next(&intf->it)) {
		if (!strcmp(intf->name, name))
			return intf;
	}
	return NULL;
}

intf_t *intf_first() {
	return (intf_t *)llist_first(&intfs);
}

intf_t *intf_next(intf_t *intf) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	return (intf_t *)llist_iterator_next(&intf->it);
}

flagset_t *intf_flagset(intf_t *intf) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	return intf->flagset;
}

const char *intf_name(intf_t *intf) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	return intf->name;
}

intf_link_t *intf_llintfs(intf_t *intf) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	return (intf_link_t *)llist_first(&intf->llintfs);
}

intf_link_t *intf_ulintfs(intf_t *intf) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	return (intf_link_t *)llist_first(&intf->ulintfs);
}

intf_t *intf_link_current(intf_link_t *link) {
	if (!link) {
		SAH_TRACE_ERROR("nemo - link is NULL");
		return NULL;
	}
	return link->intf;
}

intf_link_t *intf_link_next(intf_link_t *link) {
	if (!link) {
		SAH_TRACE_ERROR("nemo - link is NULL");
		return NULL;
	}
	return (intf_link_t *)llist_iterator_next(&link->it);
}

static intf_link_t *intf_link_add(llist_t *list, intf_t *intf) {
	intf_link_t *link = calloc(1, sizeof(intf_link_t));
	if (!link) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	link->intf = intf;
	llist_append(list, &link->it);
	return link;
}

static void intf_link_del(intf_link_t *link) {
	llist_iterator_take(&link->it);
	free(link);
}

static void intf_link_notify(intf_t *ulintf, intf_t *llintf, bool value) {
	llist_iterator_t *(*start)(const llist_t *list) = value ? llist_first : llist_last;
	llist_iterator_t *(*step)(const llist_iterator_t *it) = value ? llist_iterator_next : llist_iterator_prev;
	linklistener_t *l, *lnext;
	for (l = (linklistener_t *)start(&linklisteners); l; l = lnext) {
		lnext = (linklistener_t *)step(&l->it);
		if (l->ulintf && l->ulintf != ulintf)
			continue;
		if (l->llintf && l->llintf != llintf)
			continue;
		l->function(ulintf, llintf, value, l->userdata);
	}
}

void intf_link(intf_t *ulintf, intf_t *llintf) {
	if (!ulintf) {
		SAH_TRACE_ERROR("nemo - ulintf is NULL");
		return;
	}
	if (!llintf) {
		SAH_TRACE_ERROR("nemo - llintf is NULL");
		return;
	}
	intf_link_t *link;
	for (link = (intf_link_t *)llist_first(&ulintf->llintfs); link; link = (intf_link_t *)llist_iterator_next(&link->it))
		if (link->intf == llintf)
			break;
	if (!link) {
		link = intf_link_add(&ulintf->llintfs, llintf);
		if (!link)
			return;
	}
	for (link = (intf_link_t *)llist_first(&llintf->ulintfs); link; link = (intf_link_t *)llist_iterator_next(&link->it))
		if (link->intf == ulintf)
			break;
	if (!link) {
		if (!intf_link_add(&llintf->ulintfs, ulintf)) {
			intf_link_del(link); // so consistency is guaranteed, even on out of memory exceptions
			return;
		}
	}
	if (!link) // llintf->ulintf was not found -> notify 
		intf_link_notify(ulintf, llintf, true);
}

void intf_unlink(intf_t *ulintf, intf_t *llintf) {
	if (!ulintf) {
		SAH_TRACE_ERROR("nemo - ulintf is NULL");
		return;
	}
	if (!llintf) {
		SAH_TRACE_ERROR("nemo - llintf is NULL");
		return;
	}
	intf_link_t *link;
	for (link = (intf_link_t *)llist_first(&ulintf->llintfs); link; link = (intf_link_t *)llist_iterator_next(&link->it))
		if (link->intf == llintf)
			break;
	if (link)
		intf_link_del(link);
	for (link = (intf_link_t *)llist_first(&llintf->ulintfs); link; link = (intf_link_t *)llist_iterator_next(&link->it))
		if (link->intf == ulintf)
			break;
	if (link)
		intf_link_del(link);
	if (link) // llintf->ulintf was found -> notify
		intf_link_notify(ulintf, llintf, false);
}

static void intf_floodFlagListener(intf_t *intf, const char *flag, intf_flaglistener l, void *userdata, bool value) {	
	intf_t *in;
	flagset_iterator_t *it;
	for (in = (intf_t *)llist_first(&intfs); in; in = (intf_t *)llist_iterator_next(&in->it)) {
		if (intf && intf != in)
			continue;
		for (it = flagset_iterator(intf_flagset(in)); it; it = flagset_iterator_next(it)) {
			if (flag && strcmp(flagset_iterator_current(it), flag))
				continue;
			l(in, flagset_iterator_current(it), value, userdata);
		}
	}
}

static void intf_floodLinkListener(intf_t *ulintf, intf_t *llintf, intf_linklistener l, void *userdata, bool value) {
	intf_t *in;
	intf_link_t *link;
	for (in = (intf_t *)llist_first(&intfs); in; in = (intf_t *)llist_iterator_next(&in->it)) {
		if (ulintf && in != ulintf)
			continue;
		for (link = (intf_link_t *)llist_first(&in->llintfs); link; link = (intf_link_t *)llist_iterator_next(&link->it)) {
			if (llintf && link->intf != llintf)
				continue;
			l(in, link->intf, value, userdata);
		}
	}
}

static void intf_floodMgmtListener(intf_t *intf, intf_mgmtlistener l, void *userdata) {
	intf_t *in;
	for (in = (intf_t *)llist_first(&intfs); in; in = (intf_t *)llist_iterator_next(&in->it)) {
		if (intf && intf != in)
			continue;
		l(in, userdata);
	}
}

static bool intf_equalsFlagListener(flaglistener_t *l, intf_t *intf, const char *flag, intf_flaglistener function, void *userdata) {
	if (l->intf != intf)
		return false;
	if (l->function != function)
		return false;
	if (l->userdata != userdata)
		return false;
	if (!l->flag && !flag)
		return true;
	if (!l->flag || !flag)
		return false;
	return !strcmp(l->flag, flag);
}

static bool intf_equalsLinkListener(linklistener_t *l, intf_t *ulintf, intf_t *llintf, intf_linklistener function, void *userdata) {
	if (l->ulintf != ulintf)
		return false;
	if (l->llintf != llintf)
		return false;
	if (l->function != function)
		return false;
	if (l->userdata != userdata)
		return false;
	return true;
}

static bool intf_equalsMgmtListener(mgmtlistener_t *l, intf_t *intf, intf_mgmtlistener create, intf_mgmtlistener modify, intf_mgmtlistener destroy, void *userdata) {
	if (l->intf != intf)
		return false;
	if (l->create != create)
		return false;
	if (l->modify != modify)
		return false;
	if (l->destroy != destroy)
		return false;
	if (l->userdata != userdata)
		return false;
	return true;
}

static bool intf_equalsMibListener(miblistener_t *l, intf_t *intf, intf_miblistener function, void *userdata) {
	if (l->intf != intf)
		return false;
	if (l->function != function)
		return false;
	if (l->userdata != userdata)
		return false;
	return true;
}

void intf_addFlagListener(intf_t *intf, const char *flag, intf_flaglistener l, void *userdata, bool boost) {
	if (!l) {
		SAH_TRACE_ERROR("nemo - l is NULL");
		return;
	}
	flaglistener_t *flaglistener = calloc(1, sizeof(flaglistener_t));
	if (!flaglistener) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	flaglistener->intf = intf;
	if (flag) {
		flaglistener->flag = strdup(flag);
		if (!flaglistener->flag) {
			SAH_TRACE_ERROR("nemo - strdup(\"%s\") failed", flag);
			free(flaglistener);
			return;		
		}
	}
	flaglistener->function = l;
	flaglistener->userdata = userdata;
	llist_append(&flaglisteners, &flaglistener->it);
	if (boost)
		intf_floodFlagListener(intf, flag, l, userdata, true);
}

void intf_delFlagListener(intf_t *intf, const char *flag, intf_flaglistener l, void *userdata, bool teardown) {
	if (!l) {
		SAH_TRACE_ERROR("nemo - l is NULL");
		return;
	}
	flaglistener_t *fl;
	for (fl = (flaglistener_t *)llist_first(&flaglisteners); fl; fl = (flaglistener_t *)llist_iterator_next(&fl->it))
		if (intf_equalsFlagListener(fl, intf, flag, l, userdata))
			break;
	if (fl) {
		llist_iterator_take(&fl->it);
		free(fl->flag);
		free(fl);
	}
	if (teardown)
		intf_floodFlagListener(intf, flag, l, userdata, false);
}

void intf_addLinkListener(intf_t *ulintf, intf_t *llintf, intf_linklistener l, void *userdata, bool boost) {
	if (!l) {
		SAH_TRACE_ERROR("nemo - l is NULL");
		return;
	}
	linklistener_t *linklistener = calloc(1, sizeof(linklistener_t));
	if (!linklistener) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	linklistener->ulintf = ulintf;
	linklistener->llintf = llintf;
	linklistener->function = l;
	linklistener->userdata = userdata;
	llist_append(&linklisteners, &linklistener->it);
	if (boost)
		intf_floodLinkListener(ulintf, llintf, l, userdata, true);
}

void intf_delLinkListener(intf_t *ulintf, intf_t *llintf, intf_linklistener l, void *userdata, bool teardown) {
	if (!l) {
		SAH_TRACE_ERROR("nemo - l is NULL");
		return;
	}
	linklistener_t *ll;
	for (ll = (linklistener_t *)llist_first(&linklisteners); ll; ll = (linklistener_t *)llist_iterator_next(&ll->it))
		if (intf_equalsLinkListener(ll, ulintf, llintf, l, userdata))
			break;
	if (ll) {
		llist_iterator_take(&ll->it);
		free(ll);
	}
	if (teardown)
		intf_floodLinkListener(ulintf, llintf, l, userdata, false);
}

void intf_addMgmtListener(intf_t *intf, intf_mgmtlistener create, intf_mgmtlistener modify, intf_mgmtlistener destroy, void *userdata, bool boost) {
	mgmtlistener_t *mgmtlistener = calloc(1, sizeof(mgmtlistener_t));
	if (!mgmtlistener) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	mgmtlistener->intf = intf;
	mgmtlistener->create = create;
	mgmtlistener->modify = modify;
	mgmtlistener->destroy = destroy;
	mgmtlistener->userdata = userdata;
	llist_append(&mgmtlisteners, &mgmtlistener->it);
	if (boost)
		intf_floodMgmtListener(intf, create, userdata);
}

void intf_delMgmtListener(intf_t *intf, intf_mgmtlistener create, intf_mgmtlistener modify, intf_mgmtlistener destroy, void *userdata, bool teardown) {
	mgmtlistener_t *ml;
	for (ml = (mgmtlistener_t *)llist_first(&mgmtlisteners); ml; ml = (mgmtlistener_t *)llist_iterator_next(&ml->it))
		if (intf_equalsMgmtListener(ml, intf, create, modify, destroy, userdata))
			break;
	if (ml) {
		llist_iterator_take(&ml->it);
		free(ml);
	}
	if (teardown)
		intf_floodMgmtListener(intf, destroy, userdata);
}

void intf_addMibListener(intf_t *intf, intf_miblistener l, void *userdata) {
	miblistener_t *miblistener = calloc(1, sizeof(miblistener_t));
	if (!miblistener) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	miblistener->intf = intf;
	miblistener->function = l;
	miblistener->userdata = userdata;
	llist_append(&miblisteners, &miblistener->it);
}

void intf_delMibListener(intf_t *intf, intf_miblistener l, void *userdata) {
	miblistener_t *ml;
	for (ml = (miblistener_t *)llist_first(&miblisteners); ml; ml = (miblistener_t *)llist_iterator_next(&ml->it))
		if (intf_equalsMibListener(ml, intf, l, userdata))
			break;
	if (ml) {
		llist_iterator_take(&ml->it);
		free(ml);
	}
}

void intf_addBlob(intf_t *intf, void *key, intf_blob_t *blob) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return;
	}
	if (!key) {
		SAH_TRACE_ERROR("nemo - blobkey is NULL");
		return;
	}
	blobentry_t *entry = calloc(1, sizeof(blobentry_t));
	if (!entry) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	entry->key = key;
	entry->blob = blob;
	llist_append(&intf->blobs, &entry->it);
}

void intf_delBlob(intf_t *intf, void *key) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return;
	}
	if (!key) {
		SAH_TRACE_ERROR("nemo - blobkey is NULL");
		return;
	}
	blobentry_t *entry;
	for (entry = (blobentry_t *)llist_first(&intf->blobs); entry; entry = (blobentry_t *)llist_iterator_next(&entry->it))
		if (entry->key == key)
			break;
	if (entry) {
		llist_iterator_take(&entry->it);
		free(entry);
	}
}

intf_blob_t *intf_getBlob(intf_t *intf, void *key) {
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	if (!key) {
		SAH_TRACE_ERROR("nemo - key is NULL");
		return NULL;
	}
	blobentry_t *entry;
	for (entry = (blobentry_t *)llist_first(&intf->blobs); entry; entry = (blobentry_t *)llist_iterator_next(&entry->it))
		if (entry->key == key)
			return entry->blob;
	return NULL;
}

