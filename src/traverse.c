/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>

#include "nemo/core/intf.h"
#include "nemo/core/traverse.h"

struct _traverse_node {
	llist_t edges;
	intf_t *intf;
	unsigned long marker;
	int refcount;
};

struct _traverse_edge {
	llist_iterator_t it;
	traverse_node_t *node;
};

static const char *traverse_mode_strs[] = {
	"this",
	"down",
	"up",
	"down exclusive",
	"up exclusive",
	"one level down",
	"one level up",
	"all",
};

bool traverse_mode_parse(traverse_mode_t *mode, const char *str) {
	traverse_mode_t m;
	if (!str)
		str = "(null)";
	for (m=traverse_mode_this; m<=traverse_mode_all; m++) {
		if (!strcmp(str, traverse_mode_strs[m])) {
			if (mode)
				*mode = m;
			return true;
		}
	}
	SAH_TRACE_ERROR("nemo - Illegal traverse mode: \"%s\"", str);
	return false;
}

const char *traverse_mode_string(traverse_mode_t mode) {
	if (mode <= traverse_mode_all)
		return traverse_mode_strs[mode];
	SAH_TRACE_ERROR("nemo - Illegal traverse mode: %d", mode);
	return "(unknown)";
}

traverse_node_t *traverse_node_find(traverse_node_t *node, intf_t *intf) {
	if (!node) {
		SAH_TRACEZ_ERROR("nemo", "node is NULL");
		return NULL;
	}
	if (node->intf == intf)
		return node;
	traverse_edge_t *edge;
	for (edge = (traverse_edge_t *)llist_first(&node->edges); edge; edge = (traverse_edge_t *)llist_iterator_next(&edge->it)) {
		node = traverse_node_find(edge->node, intf);
		if (node)
			return node;
	}
	return NULL;
}

static traverse_node_t *traverse_node_acquire(traverse_tree_t *tree, traverse_node_t *parent, intf_t *intf, traverse_mode_t mode) {
	traverse_edge_t *edge = (traverse_edge_t *)calloc(1, sizeof(traverse_edge_t));
	if (!edge) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	traverse_node_t *node = NULL;
	bool new = false;
	if (mode == traverse_mode_down || mode == traverse_mode_up ||
			mode == traverse_mode_down_exclusive || mode == traverse_mode_up_exclusive)
		node = traverse_node_find(tree, intf);
	if (!node) {
		node = (traverse_node_t *)calloc(1, sizeof(traverse_node_t));
		if (!node) {
			SAH_TRACE_ERROR("nemo - calloc failed");
			free(edge);
			return NULL;
		}
		node->intf = intf;
		new = true;
	}
	node->refcount++;
	edge->node = node;
	llist_append(&parent->edges, &edge->it);
	return new ? node : NULL;
}

static void traverse_node_release(traverse_node_t *node) {
	if (!node)
		return;
	node->refcount--;
	if (node->refcount)
		return;
	while (!llist_isEmpty(&node->edges)) {
		traverse_edge_t *edge = (traverse_edge_t *)llist_first(&node->edges);
		traverse_node_release(edge->node);
		llist_iterator_take(&edge->it);
		free(edge);
	}
	free(node);
}

intf_t *traverse_node_intf(traverse_node_t *node) {
	if (!node) {
		SAH_TRACE_ERROR("nemo - node is NULL");
		return NULL;
	}
	return node->intf;
}

traverse_edge_t *traverse_node_firstEdge(traverse_node_t *node) {
	if (!node) {
		SAH_TRACE_ERROR("nemo - node is NULL");
		return NULL;
	}
	return (traverse_edge_t *)llist_first(&node->edges);
}

void traverse_node_mark(traverse_node_t *node, unsigned long marker) {
	if (!node) {
		SAH_TRACE_ERROR("nemo - node is NULL");
		return;
	}
	node->marker = marker;
}

unsigned long traverse_node_marker(traverse_node_t *node) {
	if (!node) {
		SAH_TRACE_ERROR("nemo - node is NULL");
		return 0;
	}
	return node->marker;
}

traverse_edge_t *traverse_edge_next(traverse_edge_t *edge) {
	if (!edge) {
		SAH_TRACE_ERROR("nemo - edge is NULL");
		return NULL;
	}
	return (traverse_edge_t *)llist_iterator_next(&edge->it);
}

traverse_node_t *traverse_edge_node(traverse_edge_t *edge) {
	if (!edge) {
		SAH_TRACE_ERROR("nemo - edge is NULL");
		return NULL;
	}
	return edge->node;
}

static void traverse_node_build(traverse_tree_t *tree, traverse_node_t *node, traverse_mode_t mode, intf_t *intf) {
	intf_link_t *link = NULL;
	switch (mode) {
		case traverse_mode_all:
			for (intf = intf_first(); intf; intf = intf_next(intf))
				traverse_node_acquire(tree, node, intf, mode);
			return;
		case traverse_mode_this:
			node->intf = intf;
			return;
		case traverse_mode_down:
			node->intf = intf;
			link = intf_llintfs(intf);
			break;
		case traverse_mode_down_exclusive:
			mode = traverse_mode_down;
			link = intf_llintfs(intf);
			break;
		case traverse_mode_one_level_down:
			mode = traverse_mode_this;
			link = intf_llintfs(intf);
			break;
		case traverse_mode_up:
			node->intf = intf;
			link = intf_ulintfs(intf);
			break;
		case traverse_mode_up_exclusive:
			mode = traverse_mode_up;
			link = intf_ulintfs(intf);
			break;
		case traverse_mode_one_level_up:
			mode = traverse_mode_this;
			link = intf_ulintfs(intf);
			break;
	}
	for (; link; link = intf_link_next(link)) {
		intf = intf_link_current(link);
		traverse_node_t *n = traverse_node_acquire(tree, node, intf, mode);
		if (!n)
			continue;
		traverse_node_build(tree, n, mode, intf);
	}
}

traverse_tree_t *traverse_tree_create(traverse_mode_t mode, intf_t *intf) {
	traverse_tree_t *tree = (traverse_tree_t *)calloc(1, sizeof(traverse_tree_t));
	if (!tree) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	tree->refcount = 1;
	traverse_node_build(tree, tree, mode, intf);
	return tree;
}

void traverse_tree_destroy(traverse_tree_t *tree) {
	traverse_node_release(tree);
}

traverse_node_t *traverse_tree_root(traverse_tree_t *tree) {
	return tree;
}

static bool traverse_node_walk(traverse_node_t *node, bool (*step)(intf_t *intf, void *userdata), void *userdata) {
	if (node->marker)
		return false;
	node->marker = 1;
	if (node->intf && step && step(node->intf, userdata))
		return true;
	traverse_edge_t *edge;
	for (edge = (traverse_edge_t *)llist_first(&node->edges); edge; edge = (traverse_edge_t *)llist_iterator_next(&edge->it)) {
		if (traverse_node_walk(edge->node, step, userdata))
			return true;
	}
	return false;
}

void traverse_tree_clearMarks(traverse_tree_t *tree) {
	if (!tree) {
		SAH_TRACE_ERROR("nemo - tree is NULL");
		return;
	}
	traverse_edge_t *edge;
	tree->marker = 0;
	for (edge = (traverse_edge_t *)llist_first(&tree->edges); edge; edge = (traverse_edge_t *)llist_iterator_next(&edge->it))
		traverse_tree_clearMarks(edge->node);
}

bool traverse_tree_walk(traverse_tree_t *tree, bool (*step)(intf_t *intf, void *userdata), void *userdata) {
	if (!tree) {
		SAH_TRACE_ERROR("nemo - tree is NULL");
		return false;
	}
	traverse_tree_clearMarks(tree);
	return traverse_node_walk(tree, step, userdata);
}

static int traverse_node_print(traverse_node_t *node, char *buf) {
	if (node->marker) {
		if (buf)
			buf += sprintf(buf, "%s", intf_name(node->intf));
		return strlen(intf_name(node->intf));
	}
	int n = 0, m = 0, d;
	node->marker = 1;
	if (buf)
		buf += sprintf(buf, "%s:{", intf_name(node->intf));
	n += 2 + strlen(intf_name(node->intf));
	traverse_edge_t *edge;
	for (edge = (traverse_edge_t *)llist_first(&node->edges); edge; edge = (traverse_edge_t *)llist_iterator_next(&edge->it)) {
		if (m++) {
			if (buf)
				buf += sprintf(buf, ", ");
			n += 2;
		}
		d = traverse_node_print(edge->node, buf);
		n += d;
		if (buf)
			buf += d;
	}
	if (buf)
		buf += sprintf(buf, "}");
	n += 1;
	return n;
}

char *traverse_tree_print(traverse_tree_t *tree) {
	if (!tree) {
		SAH_TRACE_ERROR("nemo - tree is NULL");
		return NULL;
	}
	traverse_tree_clearMarks(tree);
	int n = traverse_node_print(tree, NULL);
	traverse_tree_clearMarks(tree);
	char *buf = malloc(n + 1);
	traverse_node_print(tree, buf);
	traverse_tree_clearMarks(tree);
	return buf;
}

