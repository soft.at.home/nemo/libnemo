/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core.h"
#include "core/fun.h"

struct _query_args {
	traverse_mode_t traverse;
	flagexp_t *flag;
	flagexp_t *condition;
	char *target;
	char *name;
};

typedef enum _fun_query_class_index {
	fun_query_class_isUp = 0,
	fun_query_class_hasFlag,
	fun_query_class_isLinkedTo,
	fun_query_class_getIntfs,
	fun_query_class_luckyIntf,
	fun_query_class_getFirstParameter,
	fun_query_class_getParameters,
	fun_query_class_count,
} fun_query_class_index_t;

static void fun_query_args_destroy(query_class_t *cl, query_args_t *args);
static bool fun_query_args_convert(query_class_t *cl, query_args_t **args, argument_value_list_t *arglist, uint32_t attributes);
static bool fun_query_args_compare(query_class_t *cl, query_args_t *args1, query_args_t *args2);
static char *fun_query_args_describe(query_class_t *cl, query_args_t *args);
static void fun_query_run(query_t *q, variant_t *result);

static query_class_info_t fun_query_classes_info[fun_query_class_count] = {
	{
		/* name */          "isUp",
		/* type */          variant_type_bool,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_isUp,
	}, {
		/* name */          "hasFlag",
		/* type */          variant_type_bool,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_hasFlag,
	}, {
		/* name */          "isLinkedTo",
		/* type */          variant_type_bool,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_isLinkedTo,
	}, {
		/* name */          "getIntfs",
		/* type */          variant_type_array,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_getIntfs,
	}, {
		/* name */          "luckyIntf",
		/* type */          variant_type_string,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_luckyIntf,
	}, {
		/* name */          "getFirstParameter",
		/* type */          variant_type_string,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_getFirstParameter,
	}, {
		/* name */          "getParameters",
		/* type */          variant_type_map,
		/* args_convert */  fun_query_args_convert,
		/* args_destroy */  fun_query_args_destroy,
		/* args_compare */  fun_query_args_compare,
		/* args_describe */ fun_query_args_describe,
		/* open */          NULL,
		/* close */         NULL,
		/* run */           fun_query_run,
		/* userdata */      fun_getParameters,
	}
};
static query_class_t *fun_query_classes[fun_query_class_count] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL};

static void fun_query_args_destroy(query_class_t *cl, query_args_t *args) {
	(void)cl;
	if (!args)
		return;
	flagexp_destroy(args->flag);
	flagexp_destroy(args->condition);
	free(args->target);
	free(args->name);
	free(args);
}

static bool fun_query_args_convert(query_class_t *cl, query_args_t **args, argument_value_list_t *arglist, uint32_t attributes) {
	*args = (query_args_t *)calloc(1, sizeof(query_args_t));
	if (!*args) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return false;
	}
	(*args)->traverse = traverse_mode_down;
	void *fun = query_class_info(cl)->userdata;
	if (fun == fun_getFirstParameter || fun == fun_getParameters) {
		if (!arg_takeChar(&(*args)->name, arglist, attributes | ARG_MANDATORY, "name"))
			goto error;
	}
	if (fun == fun_isLinkedTo) {
		if (!arg_takeChar(&(*args)->target, arglist, attributes | ARG_MANDATORY, "target"))
			goto error;
	}
	if (fun == fun_isUp || fun == fun_hasFlag || fun == fun_getIntfs || fun == fun_luckyIntf
			|| fun == fun_getFirstParameter || fun == fun_getParameters) {
		if (!arg_takeFlagexp(&(*args)->flag, arglist, attributes, "flag"))
			goto error;
	}
	if (fun == fun_hasFlag) {
		if (!arg_takeFlagexp(&(*args)->condition, arglist, attributes, "condition"))
			goto error;
	}
	if (!arg_takeTraverse(&(*args)->traverse, arglist, attributes, "traverse"))
		goto error;
	return true;
error:
	fun_query_args_destroy(cl, *args);
	*args = NULL;
	return false;
}

static bool fun_query_args_strcmp(const char *str1, const char *str2) {
	if (str1 && str2)
		return strcmp(str1, str2) ? false : true;
	else
		return str1 == str2;
}

static bool fun_query_args_compare(query_class_t *cl, query_args_t *args1, query_args_t *args2) {
	(void)cl;
	return true
		&& (args1->traverse == args2->traverse)
		&& flagexp_equals(args1->flag, args2->flag)
		&& flagexp_equals(args1->condition, args2->condition)
		&& fun_query_args_strcmp(args1->target, args2->target)
		&& fun_query_args_strcmp(args1->name, args2->name)
		;
}

static char *fun_query_args_describe(query_class_t *cl, query_args_t *args) {
#define DESCRIPTIONLEN 64
	char *description = calloc(DESCRIPTIONLEN, 1);
	if (!description)
		return NULL;
	char *descptr = description;
	void *fun = query_class_info(cl)->userdata;
	int n=0;
	if (fun == fun_getFirstParameter || fun == fun_getParameters) {
		if (DESCRIPTIONLEN > (descptr - description))
			descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%sname=\"%s\"", n++?", ":"", args->name?args->name:"");
	}
	if (fun == fun_isLinkedTo) {
		if (DESCRIPTIONLEN > (descptr - description))
			descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%starget=\"%s\"", n++?", ":"", args->target?args->target:"");
	}
	if (fun == fun_isUp || fun == fun_hasFlag || fun == fun_getIntfs || fun == fun_luckyIntf 
			|| fun == fun_getFirstParameter || fun == fun_getParameters) {
		if (DESCRIPTIONLEN > (descptr - description)) {
			char *flag = NULL;
			if (args->flag)
				flag = flagexp_print(args->flag);
			descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%sflag=\"%s\"", n++?", ":"", flag?flag:"");
			free(flag);
		}
	}
	if (fun == fun_hasFlag) {
		if (DESCRIPTIONLEN > (descptr - description)) {
			char *condition = NULL;
			if (args->condition)
				condition = flagexp_print(args->condition);
			descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%scondition=\"%s\"", n++?", ":"", condition?condition:"");
			free(condition);
		}
	}
	if (DESCRIPTIONLEN > (descptr - description))
		descptr += snprintf(descptr, DESCRIPTIONLEN - (descptr - description), "%straverse=\"%s\"", n++?", ":"", traverse_mode_string(args->traverse));
	(void)descptr; (void)n;
	return description;
}

static void fun_query_run(query_t *q, variant_t *result) {
	void *fun = query_class_info(query_class(q))->userdata;
	query_args_t *args = query_args(q);
	if (fun == fun_isUp || fun == fun_getIntfs || fun == fun_luckyIntf) {
		void (*run)(variant_t *result, intf_t *intf, flagexp_t *flag, traverse_mode_t traverse) = 
			(void (*)(variant_t *result, intf_t *intf, flagexp_t *flag, traverse_mode_t traverse))fun;
		run(result, query_intf(q), args->flag, args->traverse);
	} else if (fun == fun_isLinkedTo) {
		void (*run)(variant_t *result, intf_t *intf, intf_t *target, traverse_mode_t traverse) = 
			(void (*)(variant_t *result, intf_t *intf, intf_t *target, traverse_mode_t traverse))fun;
		run(result, query_intf(q), intf_find(args->target), args->traverse);
	} else if (fun == fun_getFirstParameter || fun == fun_getParameters) {
		void (*run)(variant_t *result, uint32_t uid, intf_t *intf, const char *name, flagexp_t *flag, traverse_mode_t traverse) =
			(void (*)(variant_t *result, uint32_t uid, intf_t *intf,const char *name, flagexp_t *flag, traverse_mode_t traverse))fun;
		run(result, query_uid(q), query_intf(q), args->name, args->flag, args->traverse);
	} else if (fun == fun_hasFlag) {
		void (*run)(variant_t *result, intf_t *intf, flagexp_t *flag, flagexp_t *condition, traverse_mode_t traverse) =
			(void (*)(variant_t *result, intf_t *intf, flagexp_t *flag, flagexp_t *condition, traverse_mode_t traverse))fun;
		run(result, query_intf(q), args->flag, args->condition, args->traverse);
	}
}

static unsigned long fun_isUpNode(traverse_node_t *node, flagexp_t *flagexp) {
	if (!traverse_node_marker(node)) {
		traverse_node_mark(node, 2);
		if (traverse_node_intf(node) && flagexp_evaluate(flagexp, intf_flagset(traverse_node_intf(node)))) {
			if (!flagset_check(intf_flagset(traverse_node_intf(node)), "up")) {
				traverse_node_mark(node, 1);
				return 1;
			} else {
				traverse_node_mark(node, 3);
			} 
		}
		traverse_edge_t *edge;
		for (edge = traverse_node_firstEdge(node); edge; edge = traverse_edge_next(edge)) {
			switch (fun_isUpNode(traverse_edge_node(edge), flagexp)) {
				case 3: 
					traverse_node_mark(node, 3);
					return 3;
				case 1: 
					traverse_node_mark(node, 1);
					break;
				default: 
					break;
			}
		}
	}
	return traverse_node_marker(node);
}

void fun_isUp(variant_t *result, intf_t *intf, flagexp_t *flag, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	variant_setBool(result, fun_isUpNode(traverse_tree_root(tree), flag)>2);
	traverse_tree_destroy(tree);
}

typedef struct _hasFlag_ctx {
	flagset_t *flagset;
	flagexp_t *condition;
	uint32_t uid;
} hasFlag_ctx_t;

static bool fun_hasFlagStep(intf_t *intf, void *userdata) {
	hasFlag_ctx_t *ctx = (hasFlag_ctx_t *)userdata;
	if (flagexp_evaluate(ctx->condition, intf_flagset(intf)))
		flagset_union(ctx->flagset, intf_flagset(intf));
	return false;
}

void fun_hasFlag(variant_t *result, intf_t *intf, flagexp_t *flag, flagexp_t *condition, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	hasFlag_ctx_t ctx = {NULL, condition, 0};
	ctx.flagset = flagset_create(false);
	traverse_tree_walk(tree, fun_hasFlagStep, &ctx);
	variant_setBool(result, flagexp_evaluate(flag, ctx.flagset));
	traverse_tree_destroy(tree);
	flagset_destroy(ctx.flagset);	
}

static bool fun_setFlagStep(intf_t *intf, void *userdata) {
	hasFlag_ctx_t *ctx = (hasFlag_ctx_t *)userdata;
	if (flagexp_evaluate(ctx->condition, intf_flagset(intf))) {
		if (!parameter_canWrite(object_getParameter(plugin_intf2object(intf), "Flags"), ctx->uid)) {
			SAH_TRACE_WARNING("nemo - Write access to NeMo.Intf.%s.Flags denied for user %u", intf_name(intf), ctx->uid);
		} else {
			flagset_union(intf_flagset(intf), ctx->flagset);
		}
	}
	return false;
}

void fun_setFlag(uint32_t uid, intf_t *intf, flagset_t *flag, flagexp_t *condition, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	hasFlag_ctx_t ctx = {flag, condition, uid};
	traverse_tree_walk(tree, fun_setFlagStep, &ctx);
	traverse_tree_destroy(tree);
}

static bool fun_clearFlagStep(intf_t *intf, void *userdata) {
	hasFlag_ctx_t *ctx = (hasFlag_ctx_t *)userdata;
	if (flagexp_evaluate(ctx->condition, intf_flagset(intf))) {
		if (!parameter_canWrite(object_getParameter(plugin_intf2object(intf), "Flags"), ctx->uid)) {
			SAH_TRACE_WARNING("nemo - Write access to NeMo.Intf.%s.Flags denied for user %u", intf_name(intf), ctx->uid);
		} else {
			flagset_subtract(intf_flagset(intf), ctx->flagset);
		}
	}
	return false;
}

void fun_clearFlag(uint32_t uid, intf_t *intf, flagset_t *flag, flagexp_t *condition, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	hasFlag_ctx_t ctx = {flag, condition, uid};
	traverse_tree_walk(tree, fun_clearFlagStep, &ctx);
	traverse_tree_destroy(tree);
}

typedef struct _isLinkedTo_ctx {
	intf_t *target;
	bool result;
} isLinkedTo_ctx_t;

static bool fun_isLinkedToStep(intf_t *intf, void *userdata) {
	isLinkedTo_ctx_t *ctx = (isLinkedTo_ctx_t *)userdata;
	if (intf == ctx->target) {
		ctx->result = true;
		return true;
	}
	return false;
}

void fun_isLinkedTo(variant_t *result, intf_t *intf, intf_t *target, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	isLinkedTo_ctx_t ctx = {target, false};
	traverse_tree_walk(tree, fun_isLinkedToStep, &ctx);
	variant_setBool(result, ctx.result);
	traverse_tree_destroy(tree);
}

typedef struct _getIntfs_ctx {
	variant_list_t list;
	flagexp_t *flagexp;
} getIntfs_ctx_t;

static bool fun_getIntfsStep(intf_t *intf, void *userdata) {
	getIntfs_ctx_t *ctx = (getIntfs_ctx_t *)userdata;
	if (flagexp_evaluate(ctx->flagexp, intf_flagset(intf)))
		variant_list_addChar(&ctx->list, intf_name(intf));
	return false;
}

void fun_getIntfs(variant_t *result, intf_t *intf, flagexp_t *flag, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	getIntfs_ctx_t ctx;
	ctx.flagexp = flag;
	variant_list_initialize(&ctx.list);
	traverse_tree_walk(tree, fun_getIntfsStep, &ctx);
	variant_setArrayMove(result, &ctx.list);
	variant_list_cleanup(&ctx.list);
	traverse_tree_destroy(tree);
}

typedef struct _luckyIntf_ctx {
	const char *name;
	flagexp_t *flagexp;
} luckyIntf_ctx_t;

static bool fun_luckyIntfStep(intf_t *intf, void *userdata) {
	luckyIntf_ctx_t *ctx = (luckyIntf_ctx_t *)userdata;
	if (flagexp_evaluate(ctx->flagexp, intf_flagset(intf))) {
		ctx->name = intf_name(intf);
		return true;
	}
	return false;
}

void fun_luckyIntf(variant_t *result, intf_t *intf, flagexp_t *flag, traverse_mode_t traverse) {
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	luckyIntf_ctx_t ctx = {NULL, flag};
	traverse_tree_walk(tree, fun_luckyIntfStep, &ctx);
	variant_setChar(result, ctx.name);
	traverse_tree_destroy(tree);
}

typedef struct _parameter_ctx {
	char *path;
	char *param;
	flagexp_t *flag;
	const variant_t *value;
	variant_map_t *map;
	bool breakearly;
	uint32_t uid;
} parameter_ctx_t;

static bool fun_getParameterStep(intf_t *intf, void *userdata) {
	parameter_ctx_t *ctx = (parameter_ctx_t *)userdata;
	if (flagexp_evaluate(ctx->flag, intf_flagset(intf))) {
		object_t *object = plugin_intf2object(intf);
		if (ctx->path)
			object = object_getObject(object, ctx->path, path_attr_key_notation, NULL);
		if (!object || object_state(object) == object_state_deleted)
			return false;
		parameter_t *parameter = object_getParameter(object, ctx->param);
		if (!parameter)
			return false;
		if (parameter_canReadValue(parameter, ctx->uid)) {
			parameter_update(parameter);
			if (ctx->breakearly)
				ctx->value = parameter_getValue(parameter);
			else
				variant_map_add(ctx->map, intf_name(intf), parameter_getValue(parameter));
		} else {
			SAH_TRACE_WARNING("nemo - Read access to NeMo.Intf.%s.%s%s%s denied for user %u",
			                   intf_name(intf), ctx->path?:"", ctx->path?".":"", ctx->param, ctx->uid);
		}
		if (ctx->breakearly)
			return true;
	}
	return false;
}

void fun_getFirstParameter(variant_t *result, uint32_t uid, intf_t *intf, const char *name, flagexp_t *flag, traverse_mode_t traverse) {
	char *spec = strdup(name);
	if (!spec) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", name);
		return;
	}
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	parameter_ctx_t ctx = {spec, NULL, flag, NULL, NULL, true, uid};
	ctx.param = strrchr(spec, '.');
	if (ctx.param) {
		*ctx.param++ = '\0';
	} else {
		ctx.path = NULL;
		ctx.param = spec;
	}
	traverse_tree_walk(tree, fun_getParameterStep, &ctx);
	variant_copy(result, ctx.value);
	traverse_tree_destroy(tree);
	free(spec);
}

void fun_getParameters(variant_t *result, uint32_t uid, intf_t *intf, const char *name, flagexp_t *flag, traverse_mode_t traverse) {
	char *spec = strdup(name);
	if (!spec) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", name);
		return;
	}
	parameter_ctx_t ctx = {spec, NULL, flag, NULL, NULL, false, uid};
	ctx.map = calloc(1, sizeof(variant_map_t));
	if (!ctx.map) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	variant_map_initialize(ctx.map);
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	ctx.param = strrchr(spec, '.');
	if (ctx.param) {
		*ctx.param++ = '\0';
	} else {
		ctx.path = NULL;
		ctx.param = spec;
	}
	traverse_tree_walk(tree, fun_getParameterStep, &ctx);
	variant_setMapMove(result, ctx.map);
	variant_map_cleanup(ctx.map);
	free(ctx.map);
	traverse_tree_destroy(tree);
	free(spec);
}

static bool fun_setParameterStep(intf_t *intf, void *userdata) {
	parameter_ctx_t *ctx = (parameter_ctx_t *)userdata;
	if (!flagexp_evaluate(ctx->flag, intf_flagset(intf)))
		return false;

	object_t *object = plugin_intf2object(intf);
	if (ctx->path)
		object = object_getObject(object, ctx->path, path_attr_key_notation, NULL);
	if (!object)
		return false;
	parameter_t *parameter = object_getParameter(object, ctx->param);
	if (!parameter)
		return false;
	int diff;
	if (parameter_isReadOnly(parameter)) {
		SAH_TRACE_WARNING("nemo - NeMo.Intf.%s.%s%s%s is read-only.",
		                   intf_name(intf), ctx->path?:"", ctx->path?".":"", ctx->param);
	} else if (!parameter_canWrite(parameter, ctx->uid)) {
		SAH_TRACE_WARNING("nemo - Write access to NeMo.Intf.%s.%s%s%s denied for user %u",
		                   intf_name(intf), ctx->path?:"", ctx->path?".":"", ctx->param, ctx->uid);
	} else if (variant_compare(parameter_getValue(parameter), ctx->value, &diff) && !diff) {
		// parameter not changed
	} else {
		char *charvalue = variant_char(ctx->value);
		SAH_TRACEZ_INFO("nemo", "parameter_setValue(path=NeMo.Intf.%s%s%s parameter=%s value=%s)",
				intf_name(intf), ctx->path?".":"", ctx->path?ctx->path:"", ctx->param, charvalue?charvalue:"(null)");
		free(charvalue);

		parameter_setValue(parameter, ctx->value);
		object_commit(object);
	}
	return ctx->breakearly;
}

void fun_setFirstParameter(uint32_t uid, intf_t *intf, const char *name, const variant_t *value, flagexp_t *flag, traverse_mode_t traverse) {
	char *spec = strdup(name);
	if (!spec) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", name);
		return;
	}
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	parameter_ctx_t ctx = {spec, NULL, flag, value, NULL, true, uid};
	ctx.param = strrchr(spec, '.');
	if (ctx.param) {
		*ctx.param++ = '\0';
	} else {
		ctx.path = NULL;
		ctx.param = spec;
	}
	traverse_tree_walk(tree, fun_setParameterStep, &ctx);
	traverse_tree_destroy(tree);
	free(spec);
}

void fun_setParameters(uint32_t uid, intf_t *intf, const char *name, const variant_t *value, flagexp_t *flag, traverse_mode_t traverse) {
	char *spec = strdup(name);
	if (!spec) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", name);
		return;
	}
	traverse_tree_t *tree = traverse_tree_create(traverse, intf);
	parameter_ctx_t ctx = {spec, NULL, flag, value, NULL, false, uid};
	ctx.param = strrchr(spec, '.');
	if (ctx.param) {
		*ctx.param++ = '\0';
	} else {
		ctx.path = NULL;
		ctx.param = spec;
	}
	traverse_tree_walk(tree, fun_setParameterStep, &ctx);
	traverse_tree_destroy(tree);
	free(spec);
}

static void fun_intfFlagHandler(intf_t *intf, const char *flag, bool value, void *userdata) {
	(void)intf; (void)value; (void)userdata;
	int i;
	for (i=0; i<fun_query_class_count; i++) {
		query_class_t *class = fun_query_classes[i];
		query_t *query;
		for (query = query_first(class); query; query = query_next(query)) {
			query_args_t *args = (query_args_t *)query_args(query);
			if (flagexp_contains(args->flag, flag) 
					|| flagexp_contains(args->condition, flag)
					|| (i == fun_query_class_isUp && !strcmp(flag, "up")))
				query_invalidate(query);
		}
	}
}

static void fun_intfMibHandler(intf_t *intf, intf_mibevent_t event, const char *path, void *userdata) {
	(void)intf; (void)event; (void)userdata;
	fun_query_class_index_t indexes[] = {fun_query_class_getFirstParameter, fun_query_class_getParameters};
	int i;
	for (i=0; i<2; i++) {
		query_class_t *class = fun_query_classes[indexes[i]];
		query_t *query;
		for (query = query_first(class); query; query = query_next(query)) {
			query_args_t *args = (query_args_t *)query_args(query);
			if (!strncmp(args->name, path, strlen(path)))
				query_invalidate(query);
		}
	}
}

static void fun_intfLinkHandler(intf_t *ulintf, intf_t *llintf, bool value, void *userdata) {
	(void)ulintf; (void)llintf; (void)value; (void)userdata;
	int i;
	for (i=0; i<fun_query_class_count; i++) {
		query_class_t *class = fun_query_classes[i];
		query_t *query;
		for (query = query_first(class); query; query = query_next(query)) {
			query_args_t *args = (query_args_t *)query_args(query);
			if (args->traverse != traverse_mode_this && args->traverse != traverse_mode_all)
				query_invalidate(query);
		}
	}
}

static void fun_intfMgmtHandler(intf_t *intf, void *userdata) {
	(void)intf; (void)userdata;
	int i;
	for (i=0; i<fun_query_class_count; i++) {
		query_class_t *class = fun_query_classes[i];
		query_t *query;
		for (query = query_first(class); query; query = query_next(query)) {
			query_args_t *args = (query_args_t *)query_args(query);
			if (args->traverse == traverse_mode_all)
				query_invalidate(query);
		}
	}
}

void fun_init() {
	int i;
	for (i=0; i<fun_query_class_count; i++)
		fun_query_classes[i] = query_class_register(&fun_query_classes_info[i]);
	intf_addFlagListener(NULL, NULL, fun_intfFlagHandler, NULL, false);
	intf_addMibListener(NULL, fun_intfMibHandler, NULL);
	intf_addLinkListener(NULL, NULL, fun_intfLinkHandler, NULL, false);
	intf_addMgmtListener(NULL, fun_intfMgmtHandler, NULL, fun_intfMgmtHandler, NULL, false);
}

void fun_cleanup() {
	int i;
	intf_delFlagListener(NULL, NULL, fun_intfFlagHandler, NULL, false);
	intf_delMibListener(NULL, fun_intfMibHandler, NULL);
	intf_delLinkListener(NULL, NULL, fun_intfLinkHandler, NULL, false);
	intf_delMgmtListener(NULL, fun_intfMgmtHandler, NULL, fun_intfMgmtHandler, NULL, false);
	for (i=0; i<fun_query_class_count; i++) 
		query_class_unregister(fun_query_classes[i]);
}


