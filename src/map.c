/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core/plugin.h"
#include "nemo/core/map.h"

#define MAP_ACTIVE 0x10000
#define MAP_BOOTING 0x20000

typedef struct _map {
	char *dstpath;
	object_t *object;
	int attributes;
	request_t *getrequest;
	request_t *setrequest;
	bool seterror;
	bool busy;
	peer_info_t *dst;
	map_requestHandler beforeSetRequestHandler;
	map_requestHandler afterSetRequestHandler;
	map_replyHandler beforeGetReplyHandler;
	map_replyHandler afterGetReplyHandler;
	map_notificationHandler notificationHandler;
	void *userdata;
	llist_t parameters;
	llist_t functions;
} _map_t;

struct _map_parameter {
	llist_iterator_t it;
	_map_t *map;
	parameter_t *parameter;
	char *dstname;
	int attributes;
	map_parameter_translator translate;
	void *userdata;
	variant_t value;
};

struct _map_function {
	llist_iterator_t it;
	_map_t *map;
	function_t *function;
	char *dstname;
	map_call_handler sendHandler;
	map_call_handler finishHandler;
	map_call_handler destroyHandler;
	void *userdata;
	llist_t calls;
};

struct _map_call {
	llist_iterator_t it;
	map_function_t *function;
	function_call_t *fcall;
	request_t *request;
	argument_value_list_t args;
	variant_t retval;
	void *userdata;
};

_map_t *map_create(object_t *object, int attributes, peer_info_t *dst, const char *dstpath, ...) {
	if (!dstpath) {
		SAH_TRACE_ERROR("nemo - dstpath is NULL");
		return NULL;
	}
	_map_t *map = calloc(1, sizeof(_map_t));
	if (!map) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	map->object = object;
	map->attributes = attributes;
	map->dst = dst?dst:plugin_getPeer();

	va_list args;
	va_start(args, dstpath);
	int n = vsnprintf(NULL, 0, dstpath, args) + 1;
	va_end(args);
	map->dstpath = malloc(n);
	if (!map->dstpath) {
		SAH_TRACE_ERROR("nemo - malloc(%d) failed", n);
		free(map);
		return NULL;
	}
	va_start(args, dstpath);
	vsnprintf(map->dstpath, n, dstpath, args);
	va_end(args);
	return map;
}

void map_destroy(_map_t *map) {
	if (!map)
		return;
	if (map->setrequest)
		request_destroy(map->setrequest);
	if (map->getrequest)
		request_destroy(map->getrequest);
	if (map->attributes & MAP_KILLER) {
		SAH_TRACEZ_INFO("nemo", "%s-", map->dstpath);
		request_t *request = request_create_deleteInstance(map->dstpath, request_common_path_key_notation);
		if (!request) {
			SAH_TRACE_ERROR("nemo - request_create_deleteInstance(path=%s) failed", map->dstpath);
		} else {
			if (!pcb_sendRequest(plugin_getPcb(), map->dst, request)) {
				SAH_TRACE_ERROR("nemo - Failed to send request: %d (%s)", pcb_error, error_string(pcb_error));
			}
			request_destroy(request);
		}
	}
	while (!llist_isEmpty(&map->parameters))
		map_parameter_destroy((map_parameter_t *)llist_first(&map->parameters));
	while (!llist_isEmpty(&map->functions))
		map_function_destroy((map_function_t *)llist_first(&map->functions));
	free(map->dstpath);
	free(map);
}

static int map_handleParameter(_map_t *map, const char *dstname, const variant_t *dstvalue, string_t *debugmsg, int n) {
	int diff;
	map_parameter_t *parameter = map_parameter_findByDstName(map, dstname);
	if (!parameter)
		return n;
	if (parameter->attributes & MAP_WRITEONLY)
		return n;
	variant_t value;
	variant_initialize(&value, variant_type_unknown);
	variant_copy(&value, dstvalue);
	if (parameter->translate)
		parameter->translate(parameter, map_from_destination, &value);
	if (variant_compare(&parameter->value, &value, &diff) && !diff) {
		variant_cleanup(&value);
		return n;
	}
	variant_copy(&parameter->value, &value);

	if (debugmsg) {
		char *charvalue = variant_char(&value);
		string_appendFormat(debugmsg, "%s%s=%s",  n?", ":"",
				parameter ? parameter_name(parameter->parameter) : parameter->dstname?:"", charvalue);
		free(charvalue);
	}

	if (parameter->parameter)
		parameter_setValue(parameter->parameter, &value);

	variant_cleanup(&value);

	return n + 1;
}

static bool map_handleGetReply(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	_map_t *map = (_map_t *)userdata;
	int n = 0;

	string_t debugmsgbuf;
	string_t *debugmsg = NULL;
	if (sahTraceZoneLevel(sahTraceGetZone("nemo")) >= TRACE_LEVEL_INFO) {
		debugmsg = &debugmsgbuf;
		string_initialize(debugmsg, 256);
		object_path(map->object, debugmsg, path_attr_key_notation);
		string_appendFormat(debugmsg, "{");
	} else {
		debugmsg = NULL;
	}

	map->busy = true;

	if (map->beforeGetReplyHandler)
		map->beforeGetReplyHandler(map, item);

	if (reply_item_type(item) == reply_type_object) {
		object_t *dstobject = reply_item_object(item);
		parameter_t *dstpar;
		for (dstpar = object_firstParameter(dstobject); dstpar; dstpar = object_nextParameter(dstpar))
			n = map_handleParameter(map, parameter_name(dstpar), parameter_getValue(dstpar), debugmsg, n);
	} else if (reply_item_type(item) == reply_type_notification) {
		notification_t *notification = reply_item_notification(item);
		if (notification_type(notification) == notify_value_changed) {
			const char *dstname = variant_da_char(notification_parameter_variant(
					notification_getParameter(notification, "parameter")));
			const variant_t *dstvalue = notification_parameter_variant(notification_getParameter(notification, "newvalue"));
			n = map_handleParameter(map, dstname, dstvalue, debugmsg, n);
		} else if (notification_type(notification) >= notify_custom) {
			string_t path;
			string_initialize(&path, 64);
			object_path(map->object, &path, path_attr_key_notation);
			notification_setObjectPath(notification, string_buffer(&path));
			string_cleanup(&path);
			if (!map->notificationHandler || map->notificationHandler(map, notification)) {
				SAH_TRACEZ_INFO("nemo", "Send notification %s[%d|%s]", notification_objectPath(notification)?:"(null)",
						notification_type(notification), notification_name(notification)?:"(null)");
				if (!pcb_sendNotification(plugin_getPcb(), NULL, NULL, notification)) {
					SAH_TRACE_ERROR("nemo - Failed to send notification %s[%d|%s]: %d (%s)", string_buffer(&path)?:"(null)",
							notification_type(notification), notification_name(notification)?:"(null)",
							pcb_error, error_string(pcb_error));
				}
			}
		}
	}

	if (n) {
		string_appendChar(debugmsg, "}");
		SAH_TRACEZ_INFO("nemo", "%s", string_buffer(debugmsg)?:"(null)");
		object_commit(map->object);
	}
	string_cleanup(debugmsg);

	if (map->attributes & MAP_BOOTING)
		map->attributes = ((map->attributes & ~MAP_BOOTING) | MAP_ACTIVE);

	map->busy = false;

	if (map->afterGetReplyHandler)
		map->afterGetReplyHandler(map, item);

	return true;
}

static void map_pull(_map_t *map) {
	if (map->getrequest) {
		request_destroy(map->getrequest);
		map->getrequest = NULL;
	}
	if (map->attributes & MAP_WRITEONLY)
		return;
	map->getrequest = request_create_getObject(map->dstpath, 0,
			request_no_object_caching | request_common_path_key_notation | request_getObject_parameters
			| request_notify_values_changed | request_notify_no_updates
			| ((map->attributes & MAP_NOTIFICATIONS) ? request_notify_custom : 0));
	if (!map->getrequest) {
		SAH_TRACE_ERROR("nemo - request_create_getObject(%s) failed", map->dstpath);
		return;
	}
	request_setData(map->getrequest, map);
	request_setReplyItemHandler(map->getrequest, map_handleGetReply);
	if (!pcb_sendRequest(plugin_getPcb(), map->dst, map->getrequest)) {
		SAH_TRACE_ERROR("nemo - Failed to send request");
		return;
	}
}

static bool map_handleSetDone(request_t *req, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	_map_t *map = (_map_t *)userdata;

	reply_t *reply = request_reply(req);
	if (reply_hasErrors(reply)) {
		SAH_TRACEZ_ERROR("nemo", "Received error for %s", request_path(req));
	}

	request_destroy(map->setrequest);
	map->setrequest = NULL;
	if (map->seterror)
		map_pull(map);
	return true;
}

static bool map_handleSetReply(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	map_t *map = userdata;
	if (reply_item_type(item) != reply_type_error)
		return true;
	SAH_TRACE_ERROR("nemo - 0x%8.8X %s: %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
	map->seterror = true;
	return true;
}

static inline bool map_dstToBeCreated(_map_t *map) {
	return !(map->attributes & (MAP_ACTIVE|MAP_BOOTING)) && (map->attributes & MAP_CREATOR);
}

static bool map_initSetRequest(string_t *debugmsg, _map_t *map) {
	if (map->setrequest) {
		request_destroy(map->setrequest);
		map->setrequest = NULL;
	}

	string_appendFormat(debugmsg, "%s{", map->dstpath);

	if (map_dstToBeCreated(map)) {
		char *key = strrchr(map->dstpath, '.');
		if (!key) {
			SAH_TRACE_WARNING("nemo - dstpath of map is not an instance path! (%s)", map->dstpath);
			return false;
		}
		*key = '\0';
		map->setrequest = request_create_addInstance(map->dstpath, 0, key + 1, request_no_object_caching | request_common_path_key_notation | request_createObject_not_persistent);
		*key = '.';
		if (!map->setrequest) {
			SAH_TRACE_ERROR("nemo - request_create_addInstance(%s) failed", map->dstpath);
			return false;
		}
	} else {
		map->setrequest = request_create_setObject(map->dstpath, request_no_object_caching | request_common_path_key_notation);
		if (!map->setrequest) {
			SAH_TRACE_ERROR("nemo - request_create_setObject(%s) failed", map->dstpath);
			return false;
		}
	}
	return true;
}

static void map_push(_map_t *map) {
	int n = 0, diff = 0, havesetrequest = false;

	string_t debugmsgbuf;
	string_t *debugmsg = NULL;
	if (sahTraceZoneLevel(sahTraceGetZone("nemo")) >= TRACE_LEVEL_INFO) {
		debugmsg = &debugmsgbuf;
		string_initialize(debugmsg, 256);
	} else {
		debugmsg = NULL;
	}

	map_parameter_t *parameter;

	if ((map->attributes & MAP_READONLY) || (!(map->attributes & MAP_ACTIVE) && (map->attributes & MAP_LEARN))) {
		goto leave;
	}

	for (parameter = map_parameter_first(map); parameter; parameter = map_parameter_next(parameter)) {
		if (parameter->attributes & MAP_READONLY)
			continue;
		if ((parameter->attributes & MAP_LEARN) && !(map->attributes & MAP_ACTIVE))
			continue;
		if ((parameter->attributes & MAP_KEY) && !map_dstToBeCreated(map))
			continue;
		variant_t value;
		variant_initialize(&value, variant_type_unknown);
		if (parameter->parameter)
			variant_copy(&value, parameter_getValue(parameter->parameter));
		if (parameter->translate)
			parameter->translate(parameter, map_to_destination, &value);
		if ((map->attributes & (MAP_ACTIVE|MAP_BOOTING)) && variant_compare(&parameter->value, &value, &diff) && !diff) {
			variant_cleanup(&value);
			continue;
		}
		variant_copy(&parameter->value, &value);

		if (!havesetrequest) {
			if (!map_initSetRequest(debugmsg, map))
				goto leave;
			havesetrequest = true;
		}

		if (debugmsg) {
			char *charvalue = variant_char(&value);
			string_appendFormat(debugmsg, "%s%s=%s", n?", ":"", map_parameter_dstname(parameter), charvalue);
			free(charvalue);
		}

		bool ret = request_addParameter(map->setrequest, map_parameter_dstname(parameter), &value);
		if (! ret) {
			SAH_TRACEZ_ERROR("nemo", "request_addParameter(%s, %s) failed: %d %s",
					map_parameter_dstname(parameter), variant_da_char(&value),
					pcb_error, error_string(pcb_error));
		}

		variant_cleanup(&value);
		n++;
	}
	if (!n && map_dstToBeCreated(map)) {
		if (!map_initSetRequest(debugmsg, map))
			goto leave;
		havesetrequest = true;
	}
	if (!havesetrequest)
		goto leave;
	string_appendChar(debugmsg, "}");
	SAH_TRACEZ_INFO("nemo", "%s", string_buffer(debugmsg)?:"(null)");

	if (map->beforeSetRequestHandler)
		map->beforeSetRequestHandler(map, map->setrequest);

	map->seterror = false;
	request_setData(map->setrequest, map);
	request_setReplyItemHandler(map->setrequest, map_handleSetReply);
	request_setDoneHandler(map->setrequest, map_handleSetDone);
	if (!pcb_sendRequest(plugin_getPcb(), map->dst, map->setrequest)) {
		SAH_TRACE_ERROR("nemo - Failed to send request");
	}

	if (map->afterSetRequestHandler)
		map->afterSetRequestHandler(map, map->setrequest);

leave:
	string_cleanup(debugmsg);
}

void map_commit(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}

	if (map->busy)
		return;

	map_push(map);

	if (!(map->attributes & (MAP_BOOTING|MAP_ACTIVE))) {
		map_pull(map);
		map->attributes |= MAP_BOOTING;
	}
}

void map_refresh(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}
	if (map->busy)
		return;
	map_pull(map);
}

void map_setBeforeSetRequestHandler(_map_t *map, map_requestHandler handler) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}
	map->beforeSetRequestHandler = handler;
}

void map_setAfterSetRequestHandler(_map_t *map, map_requestHandler handler) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}
	map->afterSetRequestHandler = handler;
}

void map_setBeforeGetReplyHandler(_map_t *map, map_replyHandler handler) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}
	map->beforeGetReplyHandler = handler;
}

void map_setAfterGetReplyHandler(_map_t *map, map_replyHandler handler) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}
	map->afterGetReplyHandler = handler;
}

void map_setUserdata(_map_t *map, void *userdata) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return;
	}
	map->userdata = userdata;
}

const char *map_dstpath(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	return map->dstpath;
}

object_t *map_object(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	return map->object;
}

bool map_busy(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return false;
	}
	return map->busy;
}

void *map_userdata(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	return map->userdata;
}

map_parameter_t *map_parameter_create(_map_t *map, parameter_t *p, const char *dstname, int attributes, map_parameter_translator translate, void *userdata) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	map_parameter_t *parameter = calloc(1, sizeof(map_parameter_t));
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	parameter->map = map;
	parameter->parameter = p;
	parameter->dstname = dstname?strdup(dstname):NULL;
	if (dstname && !parameter->dstname) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", dstname);
		free(parameter);
		return NULL;
	}
	parameter->attributes = attributes;
	parameter->translate = translate;
	parameter->userdata = userdata;
	variant_initialize(&parameter->value, variant_type_unknown);
	llist_append(&map->parameters, &parameter->it);
	return parameter;
}

void map_parameter_destroy(map_parameter_t *parameter) {
	if (!parameter)
		return;
	llist_iterator_take(&parameter->it);
	variant_cleanup(&parameter->value);
	free(parameter->dstname);
	free(parameter);
}

map_parameter_t *map_parameter_first(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	return (map_parameter_t *)llist_first(&map->parameters);
}

map_parameter_t *map_parameter_next(map_parameter_t *parameter) {
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - parameter is NULL");
		return NULL;
	}
	return (map_parameter_t *)llist_iterator_next(&parameter->it);
}

map_parameter_t *map_parameter_findByName(_map_t *map, const char *name) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	if (!name) {
		SAH_TRACE_ERROR("nemo - name is NULL");
		return NULL;
	}
	map_parameter_t *parameter;
	for (parameter = (map_parameter_t *)llist_first(&map->parameters); parameter; parameter = (map_parameter_t *)llist_iterator_next(&parameter->it)) {
		if (!strcmp(parameter->parameter ? parameter_name(parameter->parameter) : map_parameter_dstname(parameter), name))
			return parameter;
	}
	return NULL;
}

map_parameter_t *map_parameter_findByDstName(_map_t *map, const char *dstname) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	if (!dstname) {
		SAH_TRACE_ERROR("nemo - dstname is NULL");
		return NULL;
	}
	map_parameter_t *parameter;
	for (parameter = (map_parameter_t *)llist_first(&map->parameters); parameter; parameter = (map_parameter_t *)llist_iterator_next(&parameter->it)) {
		if (!strcmp(map_parameter_dstname(parameter), dstname))
			return parameter;
	}
	return NULL;
}

_map_t *map_parameter_map(map_parameter_t *parameter) {
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - parameter is NULL");
		return NULL;
	}
	return parameter->map;
}

parameter_t *map_parameter_parameter(map_parameter_t *parameter) {
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - parameter is NULL");
		return NULL;
	}
	return parameter->parameter;
}

const char *map_parameter_dstname(map_parameter_t *parameter) {
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - parameter is NULL");
		return NULL;
	}
	return parameter->dstname ? parameter->dstname : parameter->parameter ? parameter_name(parameter->parameter) : "";
}

void *map_parameter_userdata(map_parameter_t *parameter) {
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - parameter is NULL");
		return NULL;
	}
	return parameter->userdata;
}

int map_parameter_attributes(map_parameter_t *parameter) {
	if (!parameter) {
		SAH_TRACE_ERROR("nemo - parameter is NULL");
		return 0;
	}
	return parameter->attributes;
}

map_function_t *map_function_create(_map_t *map, function_t *f, const char *dstname,
                                    map_call_handler sendHandler, map_call_handler finishHandler, map_call_handler destroyHandler,
                                    void *userdata) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	if (!f) {
		SAH_TRACE_ERROR("nemo - f is NULL");
		return NULL;
	}
	map_function_t *function = calloc(1, sizeof(map_function_t));
	if (!function) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	function->map = map;
	function->function = f;
	function->dstname = dstname?strdup(dstname):NULL;
	if (dstname && !function->dstname) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", dstname);
		free(function);
		return NULL;
	}
	function->sendHandler = sendHandler;
	function->finishHandler = finishHandler;
	function->destroyHandler = destroyHandler;
	function->userdata = userdata;
	llist_append(&map->functions, &function->it);
	return function;
}

void map_function_destroy(map_function_t *function) {
	if (!function)
		return;
	while (!llist_isEmpty(&function->calls)) {
		map_call_t *call = (map_call_t *)llist_first(&function->calls);
		SAH_TRACE_WARNING("nemo - Aborted %s.%s()", function->map->dstpath, map_function_dstname(function));
		reply_addError(request_reply(fcall_request(call->fcall)), pcb_error_canceled, "Aborted", function->map->dstpath);
		map_call_finish(call);
	}
	llist_iterator_take(&function->it);
	free(function->dstname);
	free(function);
}

map_function_t *map_function_first(_map_t *map) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	return (map_function_t *)llist_first(&map->functions);
}

map_function_t *map_function_next(map_function_t *function) {
	if (!function) {
		SAH_TRACE_ERROR("nemo - function is NULL");
		return NULL;
	}
	return (map_function_t *)llist_iterator_next(&function->it);
}

map_function_t *map_function_findByName(_map_t *map, const char *name) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	if (!name) {
		SAH_TRACE_ERROR("nemo - name is NULL");
		return NULL;
	}
	map_function_t *function;
	for (function = (map_function_t *)llist_first(&map->functions); function; function = (map_function_t *)llist_iterator_next(&function->it)) {
		if (!strcmp(function_name(function->function), name))
			return function;
	}
	return NULL;
}

map_function_t *map_function_findByDstName(_map_t *map, const char *dstname) {
	if (!map) {
		SAH_TRACE_ERROR("nemo - map is NULL");
		return NULL;
	}
	if (!dstname) {
		SAH_TRACE_ERROR("nemo - dstname is NULL");
		return NULL;
	}
	map_function_t *function;
	for (function = (map_function_t *)llist_first(&map->functions); function; function = (map_function_t *)llist_iterator_next(&function->it)) {
		if (!strcmp(map_function_dstname(function), dstname))
			return function;
	}
	return NULL;
}

_map_t *map_function_map(map_function_t *function) {
	if (!function) {
		SAH_TRACE_ERROR("nemo - function is NULL");
		return NULL;
	}
	return function->map;
}

function_t *map_function_function(map_function_t *function) {
	if (!function) {
		SAH_TRACE_ERROR("nemo - function is NULL");
		return NULL;
	}
	return function->function;
}

const char *map_function_dstname(map_function_t *function) {
	if (!function) {
		SAH_TRACE_ERROR("nemo - function is NULL");
		return NULL;
	}
	return function->dstname ? : function_name(function->function);
}

void *map_function_userdata(map_function_t *function) {
	if (!function) {
		SAH_TRACE_ERROR("nemo - function is NULL");
		return NULL;
	}
	return function->userdata;
}

static bool map_call_requestReplyHandler(request_t *req, reply_item_t *item, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	map_call_t *call = (map_call_t *)userdata;
	switch (reply_item_type(item)) {
	case reply_type_error:
		SAH_TRACE_WARNING("nemo - Received error for %s.%s() : %08x - %s - %s",
				request_path(call->request), request_functionName(call->request),
				reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
		reply_addError(request_reply(fcall_request(call->fcall)),
				reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
		break;
	case reply_type_function_return: {
		variant_copy(&call->retval, reply_item_returnValue(item));
		argument_value_t *arg;
		for (arg = argument_valueFirstArgument(reply_item_returnArguments(item)); arg; arg = argument_valueNextArgument(arg)) {
			argument_valueAdd(&call->args, argument_valueName(arg), argument_value(arg));
		}
		break;
	}
	default:
		break;
	}
	return true;
}

static bool map_call_requestDoneHandler(request_t *req, pcb_t *pcb, peer_info_t *from, void *userdata) {
	(void)req; (void)pcb; (void)from;
	map_call_finish((map_call_t *)userdata);
	return true;
}

static void map_call_requestCancelHandler(request_t *req, void *userdata) {
	(void)req;
	map_call_t *call = (map_call_t *)userdata;
	SAH_TRACE_WARNING("nemo - %s.%s() aborted", call->function->map->dstpath, map_function_dstname(call->function));
	reply_addError(request_reply(fcall_request(call->fcall)), pcb_error_canceled, "Aborted", call->function->map->dstpath);
	map_call_finish(call);
}

static void map_call_fcallCancelHandler(function_call_t *fcall, void *userdata) {
	map_call_t *call = (map_call_t *)userdata;
	call->fcall = NULL;
	fcall_setCancelHandler(fcall, NULL);
	map_call_destroy(call);
}

map_call_t *map_call_create(map_function_t *function, function_call_t *fcall, argument_value_list_t *args, variant_t *retval, void *userdata) {
	(void)retval;
	if (!fcall) {
		SAH_TRACE_ERROR("nemo - fcall is NULL");
		return NULL;
	}
	map_call_t *call = NULL;
	if (!function) {
		string_t path;
		string_initialize(&path, 64);
		object_path(fcall_object(fcall), &path, path_attr_key_notation);
		SAH_TRACE_WARNING("nemo - %s.%s() not mapped", string_buffer(&path)?:"", function_name(fcall_function(fcall)));
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_wrong_state, "Not mapped", string_buffer(&path)?:"");
		string_cleanup(&path);
		goto error;
	}
	call = calloc(1, sizeof(map_call_t));
	if (!call) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		reply_addError(request_reply(fcall_request(fcall)), pcb_error_no_memory, "calloc failed", "");
		goto error;
	}
	llist_append(&function->calls, &call->it);
	call->function = function;
	variant_initialize(&call->retval, variant_type_unknown);
	argument_value_list_initialize(&call->args);
	call->request = request_create_executeFunction(function->map->dstpath, map_function_dstname(function),
			request_common_path_key_notation | (request_attributes(fcall_request(fcall)) & request_function_args_by_name));
	if (!call->request) {
		SAH_TRACE_ERROR("nemo - request_create_executeFunction(%s, %s) failed", function->map->dstpath, map_function_dstname(function));
		reply_addError(request_reply(fcall_request(call->fcall)), pcb_error_no_memory, "request_create_executeFunction", "");
		goto error;
	}
	argument_value_t *arg;
	while ((arg = argument_valueTakeArgumentFirst(args)))
		request_addArgument(call->request, arg);
	request_setData(call->request, call);
	request_setDoneHandler(call->request, map_call_requestDoneHandler);
	request_setReplyItemHandler(call->request, map_call_requestReplyHandler);
	request_setCancelHandler(call->request, map_call_requestCancelHandler);
	call->fcall = fcall;
	call->userdata = userdata;
	if (function->sendHandler)
		function->sendHandler(call);
	SAH_TRACEZ_INFO("nemo", "Send %s.%s()", function->map->dstpath, map_function_dstname(function));
	if (!pcb_sendRequest(plugin_getPcb(), function->map->dst, call->request)) {
		SAH_TRACE_ERROR("wlan - Failed to send %s.%s()", function->map->dstpath, map_function_dstname(function));
		reply_addError(request_reply(fcall_request(call->fcall)), pcb_error, error_string(pcb_error), function->map->dstpath);
		goto error;
	}
	fcall_setUserData(fcall, call);
	fcall_setCancelHandler(fcall, map_call_fcallCancelHandler);
	return call;
error:
	fcall_destroy(fcall);
	if (call) {
		call->function = NULL;
		call->userdata = NULL;
		call->fcall = NULL;
		map_call_destroy(call);
	}
	return NULL;

}

void map_call_destroy(map_call_t *call) {
	if (!call)
		return;
	if (call->function && call->function->destroyHandler)
		call->function->destroyHandler(call);
	if (call->request) {
		request_setCancelHandler(call->request, NULL);
		request_destroy(call->request);
	}
	if (call->fcall) {
		fcall_setCancelHandler(call->fcall, NULL);
		fcall_destroy(call->fcall);
	}
	variant_cleanup(&call->retval);
	argument_valueClear(&call->args);
	llist_iterator_take(&call->it);
	free(call);
}

void map_call_finish(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return;
	}
	SAH_TRACEZ_INFO("nemo", "Finished %s.%s()", call->function->map->dstpath, map_function_dstname(call->function));
	if (call->function->finishHandler)
		call->function->finishHandler(call);
	peer_info_t *peer = fcall_peer(call->fcall);
	request_t *request = fcall_request(call->fcall);
	pcb_replyBegin(peer, request, 0);
	pcb_writeFunctionReturnBegin(peer, request);
	if (variant_type(&call->retval) != variant_type_unknown)
		pcb_writeReturnValue(peer, request, &call->retval);
	if (!llist_isEmpty(&call->args)) {
		pcb_writeReturnArgumentsStart(peer, request);
		pcb_writeReturnArguments(peer, request, &call->args);
	}
	pcb_writeFunctionReturnEnd(peer, request);
	default_sendErrorList(peer, request);
	pcb_replyEnd(peer, request);
	request_setDone(request);
	map_call_destroy(call);
}

map_call_t *map_call_first(map_function_t *function) {
	if (!function) {
		SAH_TRACE_ERROR("nemo - function is NULL");
		return NULL;
	}
	return (map_call_t *)llist_first(&function->calls);
}

map_call_t *map_call_next(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return (map_call_t *)llist_iterator_next(&call->it);
}

map_function_t *map_call_function(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return call->function;
}

function_call_t *map_call_fcall(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return call->fcall;
}

request_t *map_call_request(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return call->request;
}

argument_value_list_t *map_call_arguments(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return &call->args;
}

variant_t *map_call_retval(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return &call->retval;
}

void *map_call_userdata(map_call_t *call) {
	if (!call) {
		SAH_TRACE_ERROR("nemo - call is NULL");
		return NULL;
	}
	return call->userdata;
}
