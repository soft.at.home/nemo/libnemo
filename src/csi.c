/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <debug/sahtrace.h>

#include "nemo/core/plugin.h"
#include "nemo/core/intf.h"
#include "nemo/core/arg.h"
#include "core/csi.h"

typedef struct _csi {
	llist_iterator_t it;
	intf_t *intf;
	char *func;
	peer_info_t *peer;
} csi_t;

typedef struct _csicall {
	llist_iterator_t it;
	uint32_t id;
	function_call_t *fcall;
	csi_t *csi;
} csicall_t;

static llist_t csis = {NULL, NULL};
static llist_t csicalls = {NULL, NULL};

static csicall_t *csicall_find(uint32_t id) {
	csicall_t *csicall;
	for (csicall = (csicall_t *)llist_first(&csicalls); csicall; csicall = (csicall_t *)llist_iterator_next(&csicall->it)) {
		if (csicall->id == id)
			return csicall;
	}
	return NULL;
}

static void csicall_destroy(csicall_t *csicall) {
	if (!csicall)
		return;
	llist_iterator_take(&csicall->it);
	if (csicall->fcall) {
		fcall_setCancelHandler(csicall->fcall, NULL);
		fcall_destroy(csicall->fcall);
	}
	free(csicall);
}

static csicall_t *csicall_create(csi_t *csi, function_call_t *fcall) {
	csicall_t *csicall = calloc(1, sizeof(csicall_t));
	if (!csicall) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		arg_setError(pcb_error_no_memory, "Out of memory", "");
		return NULL;
	}
	csicall->id = ({static uint32_t idcounter=0; idcounter++;});
	csicall->csi = csi;
	csicall->fcall = fcall;
	llist_append(&csicalls, &csicall->it);
	return csicall;	
}

static bool csicall_notify(csicall_t *csicall, uint32_t type, const char *name, argument_value_list_t *args) {
	bool ok = false;
	notification_t *notification = notification_create(type);
	notification_setName(notification, name);
	if (!notification) {
		SAH_TRACE_ERROR("nemo - notification_create failed");
		arg_setError(pcb_error_no_memory, "Out of memory", "");
		goto leave;
	}
	string_t path;
	string_initialize(&path, 64);
	object_path(plugin_intf2object(csicall->csi->intf), &path, path_attr_key_notation);
	notification_setObjectPath(notification, string_buffer(&path));
	SAH_TRACEZ_INFO("nemo", "Notify CSI call(type=%s path=%s intf=%s func=%s id=%u)"
		, type == NOTIFY_NEMO_CSI_CANCEL ? "cancel" : type == NOTIFY_NEMO_CSI_EXEC ? "exec" : "unknown"
		, string_buffer(&path), intf_name(csicall->csi->intf), csicall->csi->func, csicall->id);
	string_cleanup(&path);
	notification_addParameter(notification, notification_parameter_create("intf", intf_name(csicall->csi->intf)));
	notification_addParameter(notification, notification_parameter_create("func", csicall->csi->func));
	char decbuf[20];
	sprintf(decbuf, "%u", csicall->id);
	notification_addParameter(notification, notification_parameter_create("id", decbuf));
	if (args) {
		sprintf(decbuf, "%u", request_attributes(fcall_request(csicall->fcall)));
		notification_addParameter(notification, notification_parameter_create("attributes", decbuf));
		argument_value_t *arg;
		for (arg = argument_valueFirstArgument(args); arg; arg = argument_valueNextArgument(arg)) {
			notification_addParameter(notification, notification_parameter_createVariant(argument_valueName(arg), argument_value(arg)));
		}
	}
	if (!pcb_sendNotification(plugin_getPcb(), csicall->csi->peer, NULL, notification)) {
		SAH_TRACE_ERROR("nemo - Failed to send notification: %d (%s)", pcb_error, error_string(pcb_error));
		arg_setError(pcb_error, error_string(pcb_error), "");
		goto leave;
	}
	ok = true;
leave:
	if (notification)
		notification_destroy(notification);
	return ok;
}

static void csicall_reply(csicall_t *csicall, function_exec_state_t state, variant_t *returnValue, argument_value_list_t *args) {
	peer_info_t *peer = fcall_peer(csicall->fcall);
	request_t *request = fcall_request(csicall->fcall);
	pcb_replyBegin(peer, request, state == function_exec_error ? pcb_error_function_exec_failed : 0);
	if (state == function_exec_done) {
		pcb_writeFunctionReturnBegin(peer, request);
		if (returnValue)
			pcb_writeReturnValue(peer, request, returnValue);
		if (!llist_isEmpty(args)) {
		    pcb_writeReturnArgumentsStart(peer, request);
		    pcb_writeReturnArguments(peer, request, args);
		}
		pcb_writeFunctionReturnEnd(peer, request);
	}
	default_sendErrorList(peer, request);
	pcb_replyEnd(peer, request);
	request_setDone(request);
}

static function_t *csi_findFunction(intf_t *intf, const char *func) {
	function_t *function = NULL;
	char *spec = strdup(func);
	if (!spec) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", spec);
		arg_setError(pcb_error_no_memory, "Out of memory", "");
		goto leave;
	}
	char *path = spec;
	char *funcname = strrchr(spec, '.');
	if (funcname) {
		*funcname++ = '\0';
	} else {
		path = NULL;
		funcname = spec;
	}
	object_t *object = plugin_intf2object(intf);
	if (path) {
		object = object_getObject(object, path, path_attr_key_notation, NULL);
		if (!object) {
			SAH_TRACE_WARNING("nemo - Object NeMo.Intf.%s.%s not found", intf_name(intf), path);
			arg_setError(pcb_error_not_found, "Object not found", path);
			goto leave;
		}
	}
	function = object_getFunction(object, funcname);
	if (!function) {
		SAH_TRACE_WARNING("nemo - Function NeMo.Intf.%s.%s not found", intf_name(intf), func);
		arg_setError(pcb_error_not_found, "Function not found", func);
		goto leave;
	}
leave:
	free(spec);
	return function;
}

static csi_t *csi_find(intf_t *intf, const char *func) {
	csi_t *csi;
	for (csi = (csi_t *)llist_first(&csis); csi; csi = (csi_t *)llist_iterator_next(&csi->it)) {
		if (csi->intf == intf && !strcmp(csi->func, func))
			return csi;
	}
	return NULL;
}

static void csi_destroy(csi_t *csi) {
	if (!csi)
		return;
	csicall_t *csicall, *csicallnext;
	for (csicall = (csicall_t *)llist_first(&csicalls); csicall; csicall = csicallnext) {
		csicallnext = (csicall_t *)llist_iterator_next(&csicall->it);
		if (csicall->csi == csi) {
			SAH_TRACE_WARNING("nemo - Cancel CSI call (intf=%s func=%s id=%u) because of CSI unregistration", intf_name(csi->intf), csi->func, csicall->id);
			reply_addError(request_reply(fcall_request(csicall->fcall)), pcb_error_function_exec_failed, "Function execution failed", csi->func);
			csicall_reply(csicall, function_exec_error, NULL, NULL);
			csicall_destroy(csicall);
		}
	}
	function_t *function = csi_findFunction(csi->intf, csi->func);
	if (function)
		function_setHandler(function, NULL);
	llist_iterator_take(&csi->it);
	free(csi->func);
	free(csi);
}

static bool csi_peerCloseHandler(peer_info_t *peer) {
	csi_t *csi, *csinext;
	for (csi = (csi_t *)llist_first(&csis); csi; csi = csinext) {
		csinext = (csi_t *)llist_iterator_next(&csi->it);
		if (csi->peer == peer)
			csi_destroy(csi);
	}
	return true;
}

static csi_t *csi_create(intf_t *intf, const char *func, peer_info_t *peer) {
	csi_t *csi = calloc(1, sizeof(csi_t));
	if (!csi) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		goto error;
	}
	llist_append(&csis, &csi->it);
	csi->intf = intf;
	csi->func = strdup(func);
	if (!csi->func) {
		SAH_TRACE_ERROR("nemo - strdup(%s) failed", func);
		goto error;
	}
	csi->peer = peer;
	csi_t *c;
	for (c = (csi_t *)llist_first(&csis); c; c = (csi_t *)llist_iterator_next(&c->it)) {
		if (c != csi && c->peer == peer)
			break;
	}
	if (!c)
		peer_addCloseHandler(peer, csi_peerCloseHandler);
	return csi;
error:
	csi_destroy(csi);
	return NULL;
}

static void csi_fcallCancelHandler(function_call_t *fcall, void *userdata) {
	csicall_t *csicall = (csicall_t *)userdata;
	if (csicall) {
		csicall_notify(csicall, NOTIFY_NEMO_CSI_CANCEL, "csiCancel", NULL);
		fcall_setCancelHandler(fcall, NULL);
		csicall->fcall = NULL;
		csicall_destroy(csicall);
	}
}

static function_exec_state_t csi_exec(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	csicall_t *csicall = NULL;
	string_t path;
	string_initialize(&path, 64);
	
	// find intf and func from fcall (e.g. object=NeMo.Intf.eth0.Stats function=update() -> intf="eth0" and func="Stats.update" !)
	object_path(fcall_object(fcall), &path, path_attr_key_notation);
	string_appendChar(&path, ".");
	string_appendChar(&path, function_name(fcall_function(fcall)));
	if (string_length(&path) <= 10) {
		SAH_TRACE_ERROR("nemo - string_initialize or object_path or string_appendChar failed");
		arg_setError(pcb_error_no_memory, "Out of memory", "");
		goto error;
	}
	char *intf = path.buffer + 10;
	char *func = strchr(intf, '.');
	if (!func) {
		SAH_TRACE_ERROR("nemo - Unexpected error");
		arg_setError(pcb_error_no_memory, "Out of memory", "");
		goto error;
	}
	*func++ = '\0';
	csi_t *csi = csi_find(intf_find(intf), func);
	if (!csi) {
		SAH_TRACE_ERROR("nemo - Unexpected error");
		arg_setError(pcb_error_no_memory, "Out of memory", "");
		goto error;
	}

	csicall = csicall_create(csi, fcall);
	if (!csicall)
		goto error;
	fcall_setUserData(fcall, csicall);
	fcall_setCancelHandler(fcall, csi_fcallCancelHandler);
	if (!csicall_notify(csicall, NOTIFY_NEMO_CSI_EXEC, "csiExec", args))
		goto error;
	string_cleanup(&path);
	return function_exec_executing;
error:
	reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorInfo(), arg_errorDescription());
	if (csicall)
		csicall_destroy(csicall);
	else
		fcall_destroy(fcall);	
	string_cleanup(&path);
	return function_exec_error;
}

bool csi_register(intf_t *intf, const char *func, peer_info_t *peer) {
	function_t *function = csi_findFunction(intf, func);
	if (!function)
		return false;
	if (function_isImplemented(function)) {
		SAH_TRACE_WARNING("nemo - Function NeMo.Intf.%s.%s is already implemented", intf_name(intf), func);
		return arg_returnError(pcb_error_wrong_state, "Function is already implemented", func);
	}
	if (!csi_create(intf, func, peer)) {
		return arg_returnError(pcb_error_no_memory, "Out of memory", "");
	}
	SAH_TRACEZ_INFO("nemo", "CSI of function %s on intf %s registered", func, intf_name(intf));
	function_setHandler(function, csi_exec);
	return true;
}

bool csi_unregister(intf_t *intf, const char *func) {
	csi_t *csi = csi_find(intf, func);
	if (!csi) {
		SAH_TRACE_WARNING("nemo - Function NeMo.Intf.%s.%s is not a csi-function", intf_name(intf), func);
		return arg_returnError(pcb_error_wrong_state, "Function is not a csi-function", func);	
	}
	csi_destroy(csi);
	SAH_TRACEZ_INFO("nemo", "CSI of function %s on intf %s unregistered", func, intf_name(intf));
	return true;
}

bool csi_finish(uint32_t id, function_exec_state_t state, variant_t *returnValue, variant_list_t *errors, argument_value_list_t *args) {
	bool ok = false;
	char idbuf[20];
	sprintf(idbuf, "%u", id);
	csicall_t *csicall = csicall_find(id);
	if (!csicall) {
		SAH_TRACE_WARNING("nemo - No such pending csicall (id=%u)\n", id);
		arg_setError(pcb_error_invalid_parameter, "No such pending csicall", idbuf);
		goto leave;
	}
	
	char *returnValueString = variant_char(returnValue);
	SAH_TRACEZ_INFO("nemo", "Finish CSI call (intf=%s func=%s id=%u) with state=%s and returnValue=%s", intf_name(csicall->csi->intf), csicall->csi->func, 
			csicall->id, state == function_exec_error ? "error" : state == function_exec_done ? "done" : "unknown", returnValueString);
	free(returnValueString);
	
	variant_list_iterator_t *lit;
	for (lit = variant_list_first(errors); lit; lit = variant_list_iterator_next(lit)) {
		variant_map_t *map = variant_da_map(variant_list_iterator_data(lit));
		if (!map) {
			SAH_TRACE_WARNING("nemo - Error list should be a list of error maps");
			break;
		}
		uint32_t code = variant_uint32(variant_map_iterator_data(variant_map_find(map, "code")));
		char *description = variant_char(variant_map_iterator_data(variant_map_find(map, "description")));
		char *info = variant_char(variant_map_iterator_data(variant_map_find(map, "info")));
		reply_addError(request_reply(fcall_request(csicall->fcall)), code, description?description:error_string(code), info?info:"");
		free(description);
		free(info);
	}
	
	csicall_reply(csicall, state, returnValue, args);
	
	ok = true;
leave:
	if (csicall)
		csicall_destroy(csicall);
	return ok;
}

static void csi_intfDestroyHandler(intf_t *intf, void *userdata) {
	(void)userdata;
	csi_t *csi, *csinext;
	for (csi = (csi_t *)llist_first(&csis); csi; csi = csinext) {
		csinext = (csi_t *)llist_iterator_next(&csi->it);
		if (csi->intf == intf)
			csi_destroy(csi);
	}
}

void csi_init() {
	intf_addMgmtListener(NULL, NULL, NULL, csi_intfDestroyHandler, NULL, false);
}

void csi_cleanup() {
	while (!llist_isEmpty(&csis))
		csi_destroy((csi_t *)llist_first(&csis));
	intf_delMgmtListener(NULL, NULL, NULL, csi_intfDestroyHandler, NULL, false);
}

