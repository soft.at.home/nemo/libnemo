/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core.h"
#include "core/fun.h"
#include "core/mib.h"

const char *nemo_traverse_this = "this";
const char *nemo_traverse_down = "down";
const char *nemo_traverse_up = "up";
const char *nemo_traverse_down_exclusive = "down exclusive";
const char *nemo_traverse_up_exclusive = "up exclusive";
const char *nemo_traverse_one_level_down = "one level down";
const char *nemo_traverse_one_level_up = "one level up";
const char *nemo_traverse_all = "all";

bool nemo_isUp(const char *intf, const char *flag, const char *traverse) {
	bool ret = false;
	variant_t result;
	variant_initialize(&result, variant_type_bool);
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_isUp(&result, bintf, bflag, btraverse);
	ret = variant_bool(&result);
leave:
	flagexp_destroy(bflag);
	variant_cleanup(&result);
	return ret;
}

bool nemo_hasFlag(const char *intf, const char *flag, const char *condition, const char *traverse) {
	bool ret = false;
	variant_t result;
	variant_initialize(&result, variant_type_bool);
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	flagexp_t *bcondition = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseFlagexp(&bcondition, condition, 0, "condition"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_hasFlag(&result, bintf, bflag, bcondition, btraverse);
	ret = variant_bool(&result);
leave:
	flagexp_destroy(bflag);
	flagexp_destroy(bcondition);
	variant_cleanup(&result);
	return ret;
}

void nemo_setFlag(const char *intf, const char *flag, const char *condition, const char *traverse) {
	intf_t *bintf = intf_find("lo");
	flagset_t *bflag = NULL;
	flagexp_t *bcondition = NULL;
	traverse_mode_t btraverse = traverse_mode_this;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagset(&bflag, flag, ARG_MANDATORY, "flag"))
		goto leave;
	if (!arg_parseFlagexp(&bcondition, condition, 0, "condition"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_setFlag(arg_getUserID(), bintf, bflag, bcondition, btraverse);
leave:
	flagset_destroy(bflag);
	flagexp_destroy(bcondition);
}

void nemo_clearFlag(const char *intf, const char *flag, const char *condition, const char *traverse) {
	intf_t *bintf = intf_find("lo");
	flagset_t *bflag = NULL;
	flagexp_t *bcondition = NULL;
	traverse_mode_t btraverse = traverse_mode_this;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagset(&bflag, flag, ARG_MANDATORY, "flag"))
		goto leave;
	if (!arg_parseFlagexp(&bcondition, condition, 0, "condition"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_clearFlag(arg_getUserID(), bintf, bflag, bcondition, btraverse);
leave:
	flagset_destroy(bflag);
	flagexp_destroy(bcondition);
}

bool nemo_isLinkedTo(const char *intf, const char *target, const char *traverse) {
	bool ret = false;
	variant_t result;
	variant_initialize(&result, variant_type_bool);
	intf_t *bintf = intf_find("lo");
	intf_t *btarget = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseIntf(&btarget, target, ARG_MANDATORY, "target"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_isLinkedTo(&result, bintf, btarget, btraverse);
	ret = variant_bool(&result);
leave:
	variant_cleanup(&result);
	return ret;
}

variant_list_t *nemo_getIntfs(const char *intf, const char *flag, const char *traverse) {
	variant_list_t *ret = NULL;
	variant_t result;
	variant_initialize(&result, variant_type_array);
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_getIntfs(&result, bintf, bflag, btraverse);
	ret = variant_array(&result);
leave:
	flagexp_destroy(bflag);
	variant_cleanup(&result);
	return ret;
}

char *nemo_luckyIntf(const char *intf, const char *flag, const char *traverse) {
	char *ret = NULL;
	variant_t result;
	variant_initialize(&result, variant_type_string);
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_luckyIntf(&result, bintf, bflag, btraverse);
	ret = variant_char(&result);
leave:
	flagexp_destroy(bflag);
	variant_cleanup(&result);
	return ret;
}

variant_t *nemo_getFirstParameter(const char *intf, const char *name, const char *flag, const char *traverse) {
	variant_t *ret = NULL;
	variant_t result;
	variant_initialize(&result, variant_type_unknown);
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!name) {
		SAH_TRACE_ERROR("nemo - Mandatory argument %s missing", "name");
		goto leave;
	}
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_getFirstParameter(&result, arg_getUserID(), bintf, name, bflag, btraverse);
	ret = calloc(1, sizeof(variant_t));
	variant_copy(ret, &result);
leave:
	flagexp_destroy(bflag);
	variant_cleanup(&result);
	return ret;
}

variant_map_t *nemo_getParameters(const char *intf, const char *name, const char *flag, const char *traverse) {
	variant_map_t *ret = NULL;
	variant_t result;
	variant_initialize(&result, variant_type_map);
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!name) {
		SAH_TRACE_ERROR("nemo - Mandatory argument %s missing", "name");
		goto leave;
	}
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_getParameters(&result, arg_getUserID(), bintf, name, bflag, btraverse);
	ret = variant_map(&result);
leave:
	flagexp_destroy(bflag);
	variant_cleanup(&result);
	return ret;
}

void nemo_setFirstParameter(const char *intf, const char *name, const variant_t *value, const char *flag, const char *traverse) {
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!name) {
		SAH_TRACE_ERROR("nemo - Mandatory argument %s missing", "name");
		goto leave;
	}
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_setFirstParameter(arg_getUserID(), bintf, name, value, bflag, btraverse);
leave:
	flagexp_destroy(bflag);
}

void nemo_setParameters(const char *intf, const char *name, const variant_t *value, const char *flag, const char *traverse) {
	intf_t *bintf = intf_find("lo");
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!name) {
		SAH_TRACE_ERROR("nemo - Mandatory argument %s missing", "name");
		goto leave;
	}
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	fun_setParameters(arg_getUserID(), bintf, name, value, bflag, btraverse);
leave:
	flagexp_destroy(bflag);
}

variant_map_t *nemo_getMIBs(const char *intf, const char *mibs, const char *flag, const char *traverse) {
	variant_map_t *ret = NULL;
	variant_t result;
	variant_initialize(&result, variant_type_unknown);
	intf_t *bintf = intf_find("lo");
	flagset_t *bmibs = NULL;
	flagexp_t *bflag = NULL;
	traverse_mode_t btraverse = traverse_mode_down;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseFlagset(&bmibs, mibs, 0, "mibs"))
		goto leave;
	if (!arg_parseFlagexp(&bflag, flag, 0, "flag"))
		goto leave;
	if (!arg_parseTraverse(&btraverse, traverse, 0, "traverse"))
		goto leave;
	mib_getMibs(&result, arg_getUserID(), bintf, bmibs, bflag, btraverse);
	ret = calloc(1, sizeof(variant_map_t));
	variant_map_iterator_t *it = NULL;
	variant_map_for_each(it, variant_da_map(&result))
		variant_map_add(ret, variant_map_iterator_key(it), variant_map_iterator_data(it));
leave:
	flagset_destroy(bmibs);
	flagexp_destroy(bflag);
	variant_cleanup(&result);
	return ret;
}

void nemo_setMIBs(const char *intf, variant_map_t *mibs) {
	(void)intf;
	mib_setMibs(arg_getUserID(), mibs);
}

void nemo_linkIntfs(const char *ulintf, const char *llintf) {
	intf_t *bulintf = NULL;
	intf_t *bllintf = NULL;
	if (!arg_parseIntf(&bulintf, ulintf, ARG_MANDATORY, "ulintf"))
		return;
	if (!arg_parseIntf(&bllintf, llintf, ARG_MANDATORY, "llintf"))
		return;
	intf_link(bulintf, bllintf);
}

void nemo_unlinkIntfs(const char *ulintf, const char *llintf) {
	intf_t *bulintf = NULL;
	intf_t *bllintf = NULL;
	if (!arg_parseIntf(&bulintf, ulintf, ARG_MANDATORY, "ulintf"))
		return;
	if (!arg_parseIntf(&bllintf, llintf, ARG_MANDATORY, "llintf"))
		return;
	intf_unlink(bulintf, bllintf);
}

struct _nemo_query {
	llist_iterator_t it;
	char *intf;
	char *subscriber;
	char *class;
	argument_value_list_t args;
	uint32_t attributes;
	nemo_result_handler_t handler;
	void *userdata;
	query_t *query;
};

static llist_t queries = {NULL, NULL};

static void query_trigger(query_t *query, void *userdata) {
	nemo_query_t *q = (nemo_query_t *)userdata;
	if (q->handler)
		q->handler(query_result(query), q->userdata);
}

static void query_closed(query_t *query, void *userdata) {
	(void)query;
	((nemo_query_t *)userdata)->query = NULL;
}

static void nemo_query_destroy(nemo_query_t *query) {
	if (!query)
		return;
	free(query->intf);
	free(query->subscriber);
	free(query->class);
	argument_valueClear(&query->args);
	free(query);
}

static query_listener_t query_listener = {NULL, query_closed, NULL, query_trigger};

nemo_query_t *nemo_openQuery(const char *intf, const char *subscriber, const char *class, argument_value_list_t *args, uint32_t attributes, nemo_result_handler_t handler, void *userdata) {
	if (!intf)
		intf = "lo";
	bool ok = false;
	nemo_query_t *query = calloc(1, sizeof(nemo_query_t));
	if (!query) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	query->intf = strdup(intf);
	query->subscriber = strdup(subscriber);
	query->class = strdup(class);
	if (!argument_value_list_initialize(&query->args)) {
		SAH_TRACE_ERROR("nemo - argument_value_list_initialize failed");
		goto leave;
	}
	argument_value_t *arg;
	for (arg = argument_valueFirstArgument(args); arg; arg = argument_valueNextArgument(arg)) {
		if (!argument_valueAdd(&query->args, argument_valueName(arg), argument_value(arg))) {
			SAH_TRACE_ERROR("nemo - argument_valueAdd failed");
			goto leave;
		}	
	}
	query->attributes = attributes;
	query->handler = handler;
	query->userdata = userdata;

	intf_t *bintf = intf_find("lo");
	query_class_t *bclass = NULL;
	if (!arg_parseIntf(&bintf, intf, 0, "intf"))
		goto leave;
	if (!arg_parseQueryClass(&bclass, class, ARG_MANDATORY, "class"))
		goto leave;
	function_t *function = object_getFunction(plugin_intf2object(bintf), query_class_info(bclass)->name);
	if (function && !function_canExecute(function, arg_getUserID())) {
		SAH_TRACE_WARNING("nemo - Execute access to NeMo.Intf.%s.%s denied for user %u",
		                   intf, function_name(function), arg_getUserID());
		arg_setError(EACCES, "Permission denied", function_name(function));
		goto leave;
	}
	query->query = query_open(arg_getUserID(), subscriber, bintf, bclass, args, attributes);
	if (!query->query)
		goto leave;
	query_addListener(query->query, &query_listener, query);
	llist_append(&queries, &query->it);
	query_trigger(query->query, query);
	ok = true;
leave:
	if (!ok) {
		nemo_query_destroy(query);
		query = NULL;
	}
	return query;
}

void nemo_closeQuery(nemo_query_t *query) {
	if (!query)
		return;
	
	llist_iterator_take(&query->it);
	
	if (query->query)
		query_delListener(query->query, &query_listener, query);
	
	intf_t *bintf = intf_find("lo");
	query_class_t *bclass = NULL;
	if (!arg_parseIntf(&bintf, query->intf, ARG_MANDATORY, "intf"))
		goto leave;
	if (!arg_parseQueryClass(&bclass, query->class, ARG_MANDATORY, "class"))
		goto leave;
	query_close(0, query->subscriber, bintf, bclass, &query->args, query->attributes);
leave:
	nemo_query_destroy(query);
}

static void argument_valueAddChar(argument_value_list_t *args, const char *name, const char *value) {
	variant_t var;
	variant_initialize(&var, variant_type_string);
	variant_setChar(&var, value);
	argument_valueAdd(args, name, &var);
	variant_cleanup(&var);
}

nemo_query_t *nemo_openQuery_isUp(const char *intf, const char *subscriber, const char *flag, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (flag) argument_valueAddChar(&args, "flag", flag);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "isUp", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}
nemo_query_t *nemo_openQuery_hasFlag(const char *intf, const char *subscriber, const char *flag, const char *condition, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (flag) argument_valueAddChar(&args, "flag", flag);
	if (condition) argument_valueAddChar(&args, "condition", condition);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "hasFlag", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}
nemo_query_t *nemo_openQuery_isLinkedTo(const char *intf, const char *subscriber, const char *target, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (target) argument_valueAddChar(&args, "target", target);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "isLinkedTo", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}
nemo_query_t *nemo_openQuery_getIntfs(const char *intf, const char *subscriber, const char *flag, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (flag) argument_valueAddChar(&args, "flag", flag);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "getIntfs", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}
nemo_query_t *nemo_openQuery_luckyIntf(const char *intf, const char *subscriber, const char *flag, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (flag) argument_valueAddChar(&args, "flag", flag);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "luckyIntf", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}
nemo_query_t *nemo_openQuery_getFirstParameter(const char *intf, const char *subscriber, const char *name, const char *flag, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (name) argument_valueAddChar(&args, "name", name);
	if (flag) argument_valueAddChar(&args, "flag", flag);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "getFirstParameter", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}
nemo_query_t *nemo_openQuery_getParameters(const char *intf, const char *subscriber, const char *name, const char *flag, const char *traverse, nemo_result_handler_t handler, void *userdata) {
	argument_value_list_t args;
	argument_value_list_initialize(&args);
	if (name) argument_valueAddChar(&args, "name", name);
	if (flag) argument_valueAddChar(&args, "flag", flag);
	if (traverse) argument_valueAddChar(&args, "traverse", traverse);
	nemo_query_t *query = nemo_openQuery(intf, subscriber, "getParameters", &args, request_function_args_by_name, handler, userdata);
	argument_valueClear(&args);
	return query;
}

