/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core.h"
#include "core/fun.h"
#include "core/mib.h"
#include "core/csi.h"

function_exec_state_t __isUp(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_isUp(retval, plugin_object2intf(fcall_object(fcall)), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __hasFlag(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	flagexp_t *condition = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeFlagexp(&condition, args, attr, "condition"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_hasFlag(retval, plugin_object2intf(fcall_object(fcall)), flag, condition, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	flagexp_destroy(condition);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __setFlag(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	flagset_t *flag = NULL;
	flagexp_t *condition = NULL;
	traverse_mode_t traverse = traverse_mode_this;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagset(&flag, args, attr | ARG_MANDATORY, "flag"))
		goto leave;
	if (!arg_takeFlagexp(&condition, args, attr, "condition"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_setFlag(request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), flag, condition, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagset_destroy(flag);
	flagexp_destroy(condition);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __clearFlag(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	flagset_t *flag = NULL;
	flagexp_t *condition = NULL;
	traverse_mode_t traverse = traverse_mode_this;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagset(&flag, args, attr | ARG_MANDATORY, "flag"))
		goto leave;
	if (!arg_takeFlagexp(&condition, args, attr, "condition"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_clearFlag(request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), flag, condition, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagset_destroy(flag);
	flagexp_destroy(condition);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __isLinkedTo(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	intf_t *target = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeIntf(&target, args, attr | ARG_MANDATORY, "target"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_isLinkedTo(retval, plugin_object2intf(fcall_object(fcall)), target, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __getIntfs(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_getIntfs(retval, plugin_object2intf(fcall_object(fcall)), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __luckyIntf(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_luckyIntf(retval, plugin_object2intf(fcall_object(fcall)), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __getFirstParameter(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	char *name = NULL;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&name, args, attr | ARG_MANDATORY, "name"))
		goto leave;
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_getFirstParameter(retval, request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), name, flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	free(name);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __getParameters(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	char *name = NULL;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&name, args, attr | ARG_MANDATORY, "name"))
		goto leave;
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_getParameters(retval, request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), name, flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	free(name);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __setFirstParameter(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *name = NULL;
	argument_value_t *value = NULL;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&name, args, attr | ARG_MANDATORY, "name"))
		goto leave;
	if (!arg_take(&value, args, attr | ARG_MANDATORY, "value"))
		goto leave;
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_setFirstParameter(request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), name, argument_value(value), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(name);
	if (value) argument_valueDestroy(value);
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __setParameters(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *name = NULL;
	argument_value_t *value = NULL;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&name, args, attr | ARG_MANDATORY, "name"))
		goto leave;
	if (!arg_take(&value, args, attr | ARG_MANDATORY, "value"))
		goto leave;
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	fun_setParameters(request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), name, argument_value(value), flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(name);
	if (value) argument_valueDestroy(value);
	flagexp_destroy(flag);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __getMIBs(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	flagset_t *mibs = NULL;
	flagexp_t *flag = NULL;
	traverse_mode_t traverse = traverse_mode_down;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeFlagset(&mibs, args, attr, "mibs"))
		goto leave;
	if (!arg_takeFlagexp(&flag, args, attr, "flag"))
		goto leave;
	if (!arg_takeTraverse(&traverse, args, attr, "traverse"))
		goto leave;
	mib_getMibs(retval, request_userID(fcall_request(fcall)), plugin_object2intf(fcall_object(fcall)), mibs, flag, traverse);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	flagexp_destroy(flag);
	flagset_destroy(mibs);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __setMIBs(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	variant_map_t *mibs = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeMap(&mibs, args, attr | ARG_MANDATORY, "mibs"))
		goto leave;
	mib_setMibs(request_userID(fcall_request(fcall)), mibs);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	if (mibs) {
		variant_map_cleanup(mibs);
		free(mibs);
	}
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __openQuery(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	bool ok = false;
	char *subscriber = NULL;
	query_class_t *class = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&subscriber, args, attr | ARG_MANDATORY, "subscriber"))
		goto leave;
	if (!arg_takeQueryClass(&class, args, attr | ARG_MANDATORY, "class"))
		goto leave;
	function_t *function = object_getFunction(fcall_object(fcall), query_class_info(class)->name);
	if (function && !function_canExecute(function, request_userID(fcall_request(fcall)))) {
		SAH_TRACE_WARNING("nemo - Execute access to NeMo.Intf.%s.%s denied for user %u",
		                   object_name(fcall_object(fcall), path_attr_key_notation), function_name(function),
		                   request_userID(fcall_request(fcall)));
		arg_setError(EACCES, "Permission denied", function_name(function));
		goto leave;
	}
	query_t *q = query_open(request_userID(fcall_request(fcall)), subscriber, plugin_object2intf(fcall_object(fcall)), class, args, attr);
	variant_setUInt32(retval, query_id(q));
	ok = q?true:false;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(subscriber);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __closeQuery(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *subscriber = NULL;
	query_class_t *class = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&subscriber, args, attr | ARG_MANDATORY, "subscriber"))
		goto leave;
	if (!arg_takeQueryClass(&class, args, attr | ARG_MANDATORY, "class"))
		goto leave;
	query_close(request_userID(fcall_request(fcall)), subscriber, plugin_object2intf(fcall_object(fcall)), class, args, attr);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(subscriber);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __getResult(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)args;
	query_t *q = (query_t *)object_getUserData(fcall_object(fcall));
	variant_copy(retval, query_result(q));
	fcall_destroy(fcall);
	return function_exec_done;
}

function_exec_state_t __linkIntfs(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	intf_t *ulintf = NULL;
	intf_t *llintf = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeIntf(&ulintf, args, attr | ARG_MANDATORY, "ulintf"))
		goto leave;
	if (!arg_takeIntf(&llintf, args, attr | ARG_MANDATORY, "llintf"))
		goto leave;
	intf_link(ulintf, llintf);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __unlinkIntfs(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	intf_t *ulintf = NULL;
	intf_t *llintf = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeIntf(&ulintf, args, attr | ARG_MANDATORY, "ulintf"))
		goto leave;
	if (!arg_takeIntf(&llintf, args, attr | ARG_MANDATORY, "llintf"))
		goto leave;
	intf_unlink(ulintf, llintf);
	ok = true;
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __loadMIB(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *odlfile = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&odlfile, args, attr | ARG_MANDATORY, "odlfile"))
		goto leave;
	ok = mib_load(odlfile);
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(odlfile);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __unloadMIB(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *odlfile = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (!arg_takeChar(&odlfile, args, attr | ARG_MANDATORY, "odlfile"))
		goto leave;
	ok = mib_unload(odlfile);
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(odlfile);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __csiRegister(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *func = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (request_userID(fcall_request(fcall))) {
		SAH_TRACE_WARNING("nemo - Only root is allowed to invoke csiRegister()");
		arg_setError(EACCES, "Permission denied", "csiRegister");
		goto leave;
	}
	if (!arg_takeChar(&func, args, attr | ARG_MANDATORY, "func"))
		goto leave;
	ok = csi_register(plugin_object2intf(fcall_object(fcall)), func, fcall_peer(fcall));
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(func);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __csiUnregister(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	char *func = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (request_userID(fcall_request(fcall))) {
		SAH_TRACE_WARNING("nemo - Only root is allowed to invoke csiUnregister()");
		arg_setError(EACCES, "Permission denied", "csiRegister");
		goto leave;
	}
	if (!arg_takeChar(&func, args, attr | ARG_MANDATORY, "func"))
		goto leave;
	ok = csi_unregister(plugin_object2intf(fcall_object(fcall)), func);
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	free(func);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

function_exec_state_t __csiFinish(function_call_t *fcall, argument_value_list_t *args, variant_t *retval) {
	(void)retval;
	bool ok = false;
	uint32_t id = 0;
	argument_value_t *returnValue = NULL;
	char *state = NULL;
	function_exec_state_t bstate;
	variant_list_t *errors = NULL;
	uint32_t attr = request_attributes(fcall_request(fcall));
	if (request_userID(fcall_request(fcall))) {
		SAH_TRACE_WARNING("nemo - Only root is allowed to invoke csiFinish()");
		arg_setError(EACCES, "Permission denied", "csiRegister");
		goto leave;
	}
	if (!arg_takeUInt32(&id, args, attr | ARG_MANDATORY, "id"))
		goto leave;
	if (!arg_takeChar(&state, args, attr | ARG_MANDATORY, "state"))
		goto leave;
	if (!strcmp(state, "done")) {
		bstate = function_exec_done;
	} else if (!strcmp(state, "error")) {
		bstate = function_exec_error;
	} else {
		SAH_TRACE_WARNING("nemo - \"%s\" is not a valid function state", state);
		arg_setError(pcb_error_invalid_value, "Not a valid function state", state);
		goto leave;
	}
	if (!arg_take(&returnValue, args, attr, "returnValue"))
		goto leave;
	if (!arg_takeList(&errors, args, attr, "errors"))
		goto leave;
	ok = csi_finish(id, bstate, argument_value(returnValue), errors, args);
leave:
	if (!ok)
		reply_addError(request_reply(fcall_request(fcall)), arg_error(), arg_errorDescription(), arg_errorInfo());
	if (returnValue)
		argument_valueDestroy(returnValue);
	if (errors) {
		variant_list_cleanup(errors);
		free(errors);
	}
	free(state);
	fcall_destroy(fcall);
	return ok?function_exec_done:function_exec_error;
}

