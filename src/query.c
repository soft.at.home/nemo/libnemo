/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include <debug/sahtrace.h>

#include <pcb/utils/linked_list.h>
#include <pcb/utils/variant.h>
#include <pcb/core/function.h>

#include "nemo/core/flags.h"
#include "nemo/core/intf.h"
#include "nemo/core/query.h"

struct _query_class {
	llist_iterator_t it;
	query_class_info_t info;
	llist_t queries;
};

struct _query {
	llist_iterator_t it;
	unsigned int id;
	intf_t *intf;
	query_class_t *class;
	flagset_t *subscribers;
	variant_t result;
	query_args_t *args;
	llist_t listeners;
	uint32_t uid;
};

typedef struct _listener_list listener_list_t;

struct _listener_list {
	llist_iterator_t it;
	query_t *q;
	query_listener_t listener;
	void *userdata;
};

static llist_t classes = {NULL, NULL};
static llist_t listeners = {NULL, NULL};

static unsigned int query_id_counter = 0;

static bool intf_subscribed = false;

static void query_intfDestroyHandler(intf_t *intf, void *userdata) {
	(void)intf; (void)userdata;
	query_t *q, *qnext;
	query_class_t *cl;
	for (cl = query_class_first(); cl; cl = query_class_next(cl)) {
		for (q = (query_t *)llist_first(&cl->queries); q; q = qnext) {
			qnext = (query_t *)llist_iterator_next(&q->it);
			if (q->intf == intf)
				query_destroy(q);	
		}
	}
}

query_class_t *query_class_register(query_class_info_t *info) {
	if (!info) {
		SAH_TRACE_ERROR("nemo - info is NULL");
		return NULL;
	}
	query_class_t *cl = calloc(1, sizeof(query_class_t));
	if (!cl) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return NULL;
	}
	llist_append(&classes, &cl->it);
	cl->info = *info;
	if (!intf_subscribed) {
		intf_addMgmtListener(NULL, NULL, NULL, query_intfDestroyHandler, NULL, false);
		intf_subscribed = true;
	}
	return cl;
}

void query_class_unregister(query_class_t *cl) {
	if (!cl)
		return;
	while (!llist_isEmpty(&cl->queries))
		query_destroy((query_t *)llist_first(&cl->queries));
	llist_iterator_take(&cl->it);
	free(cl);
	if (llist_isEmpty(&classes) && intf_subscribed) {
		intf_delMgmtListener(NULL, NULL, NULL, query_intfDestroyHandler, NULL, false);
		intf_subscribed = false;
	}
}

query_class_info_t *query_class_info(query_class_t *cl) {
	if (!cl) {
		SAH_TRACE_ERROR("nemo - cl is NULL");
		return NULL;
	}
	return &cl->info;
}

query_class_t *query_class_find(const char *name) {
	if (!name) {
		SAH_TRACE_ERROR("nemo - name is NULL");
		return NULL;
	}
	query_class_t *cl;
	for(cl = query_class_first(); cl; cl = query_class_next(cl)) {
		if (!strcmp(name, cl->info.name))
			break;
	}
	return cl;
}

query_class_t *query_class_first() {
	return (query_class_t *)llist_first(&classes);
}

query_class_t *query_class_next(query_class_t *cl) {
	if (!cl) {
		SAH_TRACE_ERROR("nemo - cl is NULL");
		return NULL;
	}
	return (query_class_t *)llist_iterator_next((llist_iterator_t *)cl);
}

query_t *query_open(uint32_t uid, const char *subscriber, intf_t *intf, query_class_t *cl, argument_value_list_t *arglist, uint32_t attributes) {
	if (!subscriber) {
		SAH_TRACE_ERROR("nemo - subscriber is NULL");
		return NULL;
	}
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return NULL;
	}
	if (!cl) {
		SAH_TRACE_ERROR("nemo - class is NULL");
		return NULL;
	}
	query_t *q = NULL;
	query_args_t *args = NULL;
	bool argsok = false;
	llist_iterator_t *it;
	listener_list_t *l;
	if (cl->info.args_convert) {
		argsok = cl->info.args_convert(cl, &args, arglist, attributes);
		if (!argsok)
			goto error;
	}
	for (q = query_first(cl); q; q = query_next(q)) {
		if (q->uid != uid)
			continue;
		if (q->intf != intf)
			continue;
		if (!cl->info.args_compare || cl->info.args_compare(cl, q->args, args))
			break;
	}
	if (!q) {
		q = calloc(1, sizeof(query_t));
		if (!q) {
			SAH_TRACE_ERROR("nemo - calloc failed");
			goto error;
		}
		q->intf = intf;
		q->class = cl;
		q->args = args;
		q->subscribers = flagset_create(true);
		if (!q->subscribers)
			goto error;
		variant_initialize(&q->result, cl->info.type);
		q->id = ++query_id_counter;
		q->uid = uid;
		llist_append(&cl->queries, &q->it);
		if (cl->info.open)
			cl->info.open(q);
			
		if (cl->info.run)
			cl->info.run(q, &q->result);
		
		for (it = llist_first(&listeners); it; it = llist_iterator_next(it)) {
			l = (listener_list_t *)it;
			if (l->listener.opened)
				l->listener.opened(q, l->userdata);
		}
	} else {
		cl->info.args_destroy(cl, args);
	}
	flagset_set(q->subscribers, subscriber);
	for (it = llist_first(&listeners); it; it = llist_iterator_next(it)) {
		l = (listener_list_t *)it;
		if (l->listener.altered)
			l->listener.altered(q, l->userdata);
	}
	return q;
error:
	if (argsok && cl->info.args_destroy)
		cl->info.args_destroy(cl, args);
	free(q);
	return NULL;
}

void query_destroy(query_t *q) {
	if (!q)
		return;
	llist_iterator_t *it;
	listener_list_t *l;
	for (it = llist_first(&listeners); it; it = llist_iterator_next(it)) {
		l = (listener_list_t *)it;
		if (l->listener.closed)
			l->listener.closed(q, l->userdata);
	}
	for (it = llist_first(&q->listeners); it; it = llist_iterator_next(it)) {
		l = (listener_list_t *)it;
		if (l->listener.closed)
			l->listener.closed(q, l->userdata);
	}
	if (q->class->info.close)
		q->class->info.close(q);
	while ((it = llist_first(&q->listeners))) {
		llist_iterator_take(it);
		free(it);
	}
	llist_iterator_take(&q->it);
	if (q->class->info.args_destroy)
		q->class->info.args_destroy(q->class, q->args);
	variant_cleanup(&q->result);
	flagset_destroy(q->subscribers);
	free(q);
}

void query_close(uint32_t uid, const char *subscriber, intf_t *intf, query_class_t *cl, argument_value_list_t *arglist, uint32_t attributes) {
	if (!subscriber) {
		SAH_TRACE_ERROR("nemo - subscriber is NULL");
		return;
	}
	if (!intf) {
		SAH_TRACE_ERROR("nemo - intf is NULL");
		return;
	}
	if (!cl) {
		SAH_TRACE_ERROR("nemo - class is NULL");
		return;
	}
	query_t *q = NULL;
	query_args_t *args = NULL;
	bool argsok = false;
	if (cl->info.args_convert) {
		argsok = cl->info.args_convert(cl, &args, arglist, attributes);
		if (!argsok)
			goto leave;
	}
	for (q = query_first(cl); q; q = query_next(q)) {
		if (q->uid != uid)
			continue;
		if (q->intf != intf)
			continue;
		if (!cl->info.args_compare || cl->info.args_compare(cl, q->args, args))
			break;
	}
	if (!q) {
		SAH_TRACE_NOTICE("nemo - query not found");
		goto leave;
	}
	flagset_clear(q->subscribers, subscriber);
	llist_iterator_t *it;
	listener_list_t *l;
	for (it = llist_first(&listeners); it; it = llist_iterator_next(it)) {
		l = (listener_list_t *)it;
		if (l->listener.altered)
			l->listener.altered(q, l->userdata);
	}
	for (it = llist_first(&q->listeners); it; it = llist_iterator_next(it)) {
		l = (listener_list_t *)it;
		if (l->listener.altered)
			l->listener.altered(q, l->userdata);
	}
	if (flagset_count(q->subscribers, NULL))
		goto leave;
	query_destroy(q);
leave:
	if (argsok && cl->info.args_destroy)
		cl->info.args_destroy(cl, args);
}

query_t *query_first(query_class_t *cl) {
	if (!cl) {
		SAH_TRACE_ERROR("nemo - cl is NULL");
		return NULL;
	}
	return (query_t *)llist_first(&cl->queries);
}

query_t *query_next(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return (query_t *)llist_iterator_next(&q->it);
}

unsigned int query_id(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return 0;
	}
	return q->id;	
}

char *query_describe(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	char *args = q->class->info.args_describe ? q->class->info.args_describe(q->class, q->args) : NULL;
	char *description = malloc(14 + strlen(intf_name(q->intf)) + strlen(q->class->info.name) + (args?strlen(args):0));
	if (!description) {
		SAH_TRACE_ERROR("nemo - malloc(%d) failed", 14 + strlen(intf_name(q->intf)) + strlen(q->class->info.name));
		return NULL;
	}
	sprintf(description, "NeMo.Intf.%s.%s(%s)", intf_name(q->intf), q->class->info.name, args?args:"");
	free(args);
	return description;
}

flagset_t *query_subscribers(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return q->subscribers;
}

const variant_t *query_result(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return &q->result;
}

query_args_t *query_args(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return q->args;
}

intf_t *query_intf(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return q->intf;
}

query_class_t *query_class(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return q->class;
}

const char *query_classname(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return NULL;
	}
	return q->class->info.name;
}

variant_type_t query_type(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return variant_type_unknown;
	}
	return q->class->info.type;
}

uint32_t query_uid(query_t *q) {
	if (!q) {
		SAH_TRACE_ERROR("nemo - q is NULL");
		return 0;
	}
	return q->uid;
}

void query_class_invalidate(query_class_t *cl) {
	if (!cl) {
		SAH_TRACE_ERROR("nemo - cl is NULL");
		return;
	}
	query_t *q;
	for (q = query_first(cl); q; q = query_next(q)) {
		query_invalidate(q);
	}
}

void query_invalidate(query_t *q) {
	variant_t result;
	int diff = 1;
	variant_initialize(&result, q->class->info.type);

	if (q->class->info.run)
		q->class->info.run(q, &result);
	
	if (!variant_compare(&result, &q->result, &diff))
		diff = 1;
	
	if (diff) {
		variant_cleanup(&q->result);
		variant_copy(&q->result, &result);
		llist_iterator_t *it;
		listener_list_t *l;
		for (it = llist_first(&q->listeners); it; it = llist_iterator_next(it)) {
			l = (listener_list_t *)it;
			if (l->listener.trigger)
				l->listener.trigger(q, l->userdata);
		}
		for (it = llist_first(&listeners); it; it = llist_iterator_next(it)) {
			l = (listener_list_t *)it;
			if (l->listener.trigger)
				l->listener.trigger(q, l->userdata);
		}
	}
	variant_cleanup(&result);
}

void query_addListener(query_t *q, query_listener_t *l, void *userdata) {
	if (!l) {
		SAH_TRACE_ERROR("nemo - l is NULL");
		return;
	}
	llist_t *list;
	if (q)
		list = &q->listeners;
	else
		list = &listeners;
	listener_list_t *ll = calloc(1, sizeof(listener_list_t));
	if (!ll) {
		SAH_TRACE_ERROR("nemo - calloc failed");
		return;
	}
	ll->q = q;
	ll->listener = *l;
	ll->userdata = userdata;
	llist_append(list, &ll->it);
}

void query_delListener(query_t *q, query_listener_t *l, void *userdata) {
	if (!l) {
		SAH_TRACE_ERROR("nemo - l is NULL");
		return;
	}
	llist_t *list;
	if (q)
		list = &q->listeners;
	else
		list = &listeners;
	llist_iterator_t *it;
	for (it = llist_first(list); it; it = llist_iterator_next(it))
		if (!memcmp(l, &((listener_list_t *)it)->listener, sizeof(query_listener_t)) && userdata == ((listener_list_t *)it)->userdata)
			break;
	if (!it) {
		SAH_TRACE_ERROR("nemo - listener not found");
		return;
	}
	llist_iterator_take(it);
	free(it);
}

