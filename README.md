# libnemo

## Summary

Library for the NeMo plug-in, the network model of the system.
It provides an overview of all network interfaces, physical or logical.

## Description

This library contains the code for the NeMo (Network Model) plug-in, called `nemo-core`.

It manages the NeMo data model, controls the different NeMo interfaces,
loads and unloads MIBs depending on interface flags and keeps track of the 
NeMo queries created upon its interfaces and notifies subscribers of updates.

A MIB (Management Information Base) is an extension to the base data model.
It allows to export additional parameters and objects to the NeMo interfaces
where the MIB is load.

Next to the MIBs, NeMo also allows loading additional shared object modules
to extend functionality of the NeMo core.

It links directly with NetDev (Network Device) for management of the operating
system's network devices.

This library also launches a separate process, called `nemo-clients`.
It's a shell application that does not perform anything special on its own, but
provides a framework for modules to be loaded that perform small, well-defined
tasks within the NeMo ecosystem.
