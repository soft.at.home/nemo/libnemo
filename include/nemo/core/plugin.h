/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CORE_PLUGIN_H__
#define __NEMO_CORE_PLUGIN_H__

/**
@ingroup core
@defgroup plugin plugin.h - PCB plugin related utilities
@{

An important aspect of the NeMo core process is that it is just one of the many regular PCB plugins. This means that the PCB
library fully applies and starting at the reference to the PCB instance returned by plugin_getPcb(), modules have full
access to the data model. This makes modules very powerful, but unfortunately it also allows to break a lot of things.
Therefore the following warnings need to be taken into account when using the PCB core library from within a NeMo module.

@warning
Parameters of an Intf or any child object of an Intf belonging to a MIB are instances of the corresponding parameter in the MIB
definition at NeMo.MIB.{i}.DataModel. This means that they share a PCB parameter definition structure (memory optimization of PCB),
implying that they also share parameter read and write handlers. Set a parameter read or write handler on a specific parameter
on a specific Intf is possible, but you have to use plugin_setParameterReadHandler() and plugin_setParameterWriteHandler()
respectively for this purpose. The implementations of these functions rely on ownership of the userdata pointer of a parameter and
they call the provided handler from within the real parameter read or write handler. So preferably, you should not do
parameter_setReadHandler(), parameter_setWriteHandler() or parameter_setUserData() on any MIB parameter.

@warning
Some query functions rely on getting so called "MIB events". These events are fired by intf_mibnotify() which is called from within
- NeMo's default parameter write handler installed on each MIB parameter, see plugin_parameterWriteHandler()
- NeMo's default object instanceAdd handler installed on each MIB object, see plugin_objectInstanceAddHandler().
- NeMo's default object delete handler installed on each MIB object, see plugin_objectDeleteHandler().
.
Modules MAY overwrite these handlers, but assure in that case that the responsibility taken care of by these functions is still met.
The best way to do this is to call these handlers recursively. Notice by the way that they do more than calling intf_mibnotify()
only.

@warning
NeMo installs the following handlers that must not be overwritten in order to not break NeMo entirely:
- Object instanceAdd handler for NeMo.Intf.{i}.
- Object instanceDel handler for NeMo.Intf.{i}.
- Object write handler for NeMo.Intf.{i}.
- Parameter write handler for NeMo.Intf.{i}.Enable.
- Parameter write handler for NeMo.Intf.{i}.Flags.
- Parameter write handler for NeMo.Intf.{i}.Status.
- Object instanceAdd handler for NeMo.Intf.{i}.LLIntf.{i}.
- Object instanceDel handler for NeMo.Intf.{i}.LLIntf.{i}.
- Object instanceAdd handler for NeMo.Intf.{i}.ULIntf.{i}.
- Object instanceDel handler for NeMo.Intf.{i}.ULIntf.{i}.
- Execution handlers for all built-in data model functions.
.
If you feel the need to overwrite one of them anyway, please consider registering an Intf listener, see
@ref intf_flaglistener, @ref intf_linklistener and @ref intf_mgmtlistener.

@warning
NeMo relies on the ownership of the userdata pointer of NeMo.Intf.{i} instances. Please don't touch this. It will break the
coupling between data model objects and the internal @ref intf_t representation.

@warning
Intfs can be created and MIBs can be loaded, but Intfs can also be destroyed and MIBs can be unloaded or deactivated on some Intfs
because they no longer meet the MIB's conditions. So watch your pointers! Modules are allowed to keep pointers to Intfs, objects
and parameters (for performance and memory usage it's better to keep pointers than names and do the lookup every time), but they
should subscribe for the appropriate events and erase their pointers accordingly. There is one exception: unloading a MIB without
unloading the corresponding in-process implementation (i.e. module) first, is not considered to be use case. If this is done anyway,
it is permitted to cause a segmentation fault.
*/

#include <stdbool.h>
#include <stdint.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "nemo/core/intf.h"

/**
@brief
Get a reference to the PCB instance

@return A reference to the PCB instance
*/
pcb_t *plugin_getPcb();

/**
@brief
Get the default peer, which is the system bus.

@details
The exact location of the system bus is given the environmental variable PCB_SYS_BUS.

@return The default peer.
*/
peer_info_t *plugin_getPeer();

/**
@brief
Get the Intf represented by a given NeMo.Intf.{i} object.

@param object The NeMo.Intf.{i} object representing the Intf
@return The Intf represented by the object.
*/
intf_t *plugin_object2intf(object_t *object);

/**
@brief
Get the NeMo.Intf.{i} object representing an Intf.

@param intf The Intf to get the corresponding NeMo.Intf.{i} object from.
@return The data model object representing the Intf.
*/
object_t *plugin_intf2object(intf_t *intf);

/**
@brief
Set a write handler on a specific parameter instance.

@details
This is the preferred method to set parameter write handlers on MIB parameters, over calling parameter_setWriteHandler() directly.

@param parameter Data model parameter to set the read handler on.
@param handler Read handler.
*/
void plugin_setParameterWriteHandler(parameter_t *parameter, parameter_write_handler_t handler);

/**
@brief
Set a read handler on a specific parameter instance.

@details
This is the preferred method to set parameter read handlers on MIB parameters, over calling parameter_setReadHandler() directly.

@param parameter Data model parameter to set the read handler on.
@param handler Read handler.
*/
void plugin_setParameterReadHandler(parameter_t *parameter, parameter_read_handler_t handler);

/**
@brief
The default parameter write handler for all parameters under NeMo.Intf.{i} and everything below.

@details
This function does the following things:
- Call the write handler installed by plugin_setParameterWriteHandler().
- Call intf_mibnotify().
- Trigger autosave if the parameter is persistent.

@param parameter Written parameter.
@param oldvalue Previous value of the parameter.
*/
void plugin_parameterWriteHandler(parameter_t *parameter, const variant_t *oldvalue);

/**
@brief
The default parameter read handler for all parameters under NeMo.Intf.{i} and everything below.

@details
This function calls the parameter read handler installed by plugin_setParameterReadHandler().

@param parameter Read parameter.
@param value Variant to store the parameter's value in.
@return The return value of the parameter read handler installed by plugin_setParameterReadHandler() if there is any.
        True otherwise.
*/
bool plugin_parameterReadHandler(parameter_t *parameter, variant_t *value);

/**
@brief
The default object instanceAdd handler for all multi instance objects under NeMo.Intf.{i}.

@details
This function does the following things:
- Call intf_mibnotify().
- Trigger autosave if the created instance is persistent.

@param template Template object.
@param instance Newly created instance.
@return True.
*/
bool plugin_objectInstanceAddHandler(object_t *template, object_t *instance);

/**
@brief
The default object delete handler for all multi instance objects under NeMo.Intf.{i}.

@details
This function does the following things:
- Call intf_mibnotify().
- Trigger autosave if the deleted object is persistent.

@param object Deleted object.
@return True.
*/
bool plugin_objectDeleteHandler(object_t *object);

/**
@}
*/

#endif

