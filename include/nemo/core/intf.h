/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CORE_INTF_H__
#define __NEMO_CORE_INTF_H__

/**
@ingroup core
@defgroup intf intf.h - Intf management
@{
*/

#include "nemo/core/flags.h"

typedef struct _intf intf_t; /**< @brief Internal data type used to reference an Intf. */
typedef struct _intf_link intf_link_t; /**< @brief Handle for iterating over LLIntf/ULIntf dependencies. */
typedef struct _intf_blob intf_blob_t; /**< @brief Data type used to reference any kind of optional BLOB to attach to an interface.
                                                   The exact meaning is defined by the caller. */

/**
@brief
Enumeration of MIB event types.

@see intf_miblistener.
*/
typedef enum _intf_event {
	intf_mibevent_added,   /**< @brief An object has been added. */
	intf_mibevent_updated, /**< @brief A parameter is changed. */
	intf_mibevent_deleted, /**< @brief An object has been deleted. */
} intf_mibevent_t;

/**
@brief
Callback function type for flag event listeners.

@details
A flag event listener is called whenever a flag is set or cleared on an Intf.

@param intf The Intf on which a flag was set or cleared
@param flag The flag that is set/cleared.
@param value True if the flag was set, false if it was cleared.
@param userdata Void pointer passed to intf_addFlagListener().

@remark
The handler is called right AFTER the flag is set or cleared.

@remark
When an Intf is destroyed, its flags are cleared first, implying the invokation of the event listeners. This means that it is
safe to keep a reference to an Intf between getting notified of a specific flag being set and that same flag being cleared.

@see intf_addFlagListener()
*/
typedef void (*intf_flaglistener)(intf_t *intf, const char *flag, bool value, void *userdata);

/**
@brief
Callback function type for link or dependency event listeners.

@details
A link event listener is called whenever a ULIntf->LLIntf dependency is created or removed.

@param ulintf The ULIntf of the created or removed link.
@param llintf The LLIntf of the created or removed link.
@param value True if the link was created, false if it was removed.
@param userdata Void pointer passed to intf_addLinkListener().

@remark
The handler is called right AFTER the link is created or removed.

@remark
When an Intf is destroyed, its ULIntf and LLIntfs are removed first, implying the invokation of the event listeners. This means
that it is safe to keep a reference to an Intf between getting notified of a specific link being created and that same link being
removed again.

@see intf_addLinkListener()
*/
typedef void (*intf_linklistener)(intf_t *ulintf, intf_t *llintf, bool value, void *userdata);

/**
@brief
Callback function type for management event listeners.

@details
Management listeners are used to notify of Intf creation, destruction or modification events.
A modification event occurs when the PCB object that represents the Intf is written.
If multiple parameters are modified in a single write action, only one modification event is fired.

@param intf The Intf that was created/destroyed/modified.
@param userdata Void pointer passed to intf_addMgmtListener().

@note
Installing a creation, modification or destruction event listener is very similar to installing a PCB object
instanceAdd-, write- or instanceDel-handler respectively. However, the NeMo core library already installs these kinds of handlers
on the NeMo.Intf object and they must not be overwritten by modules. Nevertheless, modules may want to get triggered based on
these events anyway. The Intf management event listener registration is a solution for this problem, especially because there is no
limitation on the number event listeners and modules may register/unregister their own event listeners without affecting any other
module depending on the same events.

@see intf_addMgmtListener()
*/
typedef void (*intf_mgmtlistener)(intf_t *intf, void *userdata);

/**
@brief
Callback function type for MIB event listeners.

@details
A MIB event listener is called whenever a parameter is changed, an object instance is created or an object instance is deleted.
Object instances only include intances of child objects of an Intf, not the Intf instance itself.
If multiple parameters are modified in a single write action, multiple MIB events are fired: once for each parameter change.

@param intf The Intf on which the MIB event occurred.
@param event The type of event.
@param path Path of modified parameter, created instance of deleted instance relative to the Intf instance. For parameters,
            this path includes the parameter name itself, separated from the object path by an additional dot. This relative path
            is also known as the "parameter spec".
@param userdata Void pointer passed to intf_addMibListener().
@see intf_addMibListener()
*/
typedef void (*intf_miblistener)(intf_t *intf, intf_mibevent_t event, const char *path, void *userdata);

/**
@brief
Intf constructor.

@details
A corresponding Intf object in the data model will be created automatically before this function returns.
By default, an Intf contains no flags and no lower layer or upper layer dependencies.

@param name The name of the new Intf
@return The newly created Intf.
*/
intf_t* intf_create(const char *name);

/**
@brief
Intf destructor.

@details
This function clears the Intf's flagset and its lower layer and upper layer dependencies first, implying the invokation of
corresponding event listeners.
The corresponding Intf object in the data model will be deleted automatically before this function returns.

@param intf The Intf to destroy. When this function returns, this pointer is no longer valid.
*/
void intf_destroy(intf_t *intf);

/**
@brief
Fire a modify-event, i.e. invoke the appropriate management event listeners.

@warning
This function is called by the object write handler of the Intf object, residing inside the NeMo core library and it should not be
called anywhere else.

@param intf The modified Intf
*/
void intf_modify(intf_t *intf);

/**
@brief
Notify MIB event listeners of a specific MIB event.

@warning
This function is called from plugin_parameterWriteHandler() for parameter update events, from plugin_objectInstanceAddHandler() for
object-add events and from plugin_objectDeleteHandler() for object-delete events. Under normal conditions, this function should not
be called from anywhere else. However, some query functions rely on these events and not calling this function while it should be
called breaks the query mechanism. For more details on when and whether to call this function, have a look at the documentation of
plugin_parameterWriteHandler(), plugin_objectInstanceAddHandler() and plugin_objectDeleteHandler().

@param intf The Intf to fire the MIB event on.
@param event The MIB event type.
@param path Path of modified parameter, created instance of deleted instance relative to the Intf instance. For parameters,
            this path includes the parameter name itself, separated from the object path by an additional dot. This relative path
            is also known as the "parameter spec".
*/
void intf_mibnotify(intf_t *intf, intf_mibevent_t event, const char *path);

/**
@brief
Find an Intf given its name.

@param name The name of the Intf to find.
@return The Intf if it was found, NULL otherwise.
*/
intf_t *intf_find(const char *name);

/**
@brief
Start iterating over the global list of Intfs.

@return The first Intf in the list.
*/
intf_t *intf_first();

/**
@brief
Iterate over the global list of Intfs.

@param intf The current Intf.
@return The next Intf in the list.
*/
intf_t *intf_next(intf_t *intf);

/**
@brief
Get an Intf's flag set.

@param intf The Intf to get the flag set of.
@return Not a copy, but the Intf's internal flag set, (must not be destroyed by the caller!) so it can be used to both query and
        manipulate the Intf's flag set. When manipulating the flag set, the Intf will notice through its alert handler (please do
        not overwrite this!) and it will invoke all Intf flag listeners on its turn.
*/
flagset_t *intf_flagset(intf_t *intf);

/**
@brief
Get an Intf's name.

@param intf The Intf to get the name of.
@return The Intf's name.
*/
const char *intf_name(intf_t *intf);

/**
@brief
Start iterating over the Intf's LLIntfs.

@param intf The Intf to get the LLIntfs of.
@return An iterator pointing to the first LLIntf of the Intf. It can be retrieved with intf_link_current(). If the Intf has no
        LLIntfs, this function returns a NULL pointer.
*/
intf_link_t *intf_llintfs(intf_t *intf);

/**
@brief
Start iterating over the Intf's ULIntfs.

@param intf The Intf to get the ULIntfs of.
@return An iterator pointing to the first ULIntf of the Intf. It can be retrieved with intf_link_current(). If the Intf has no
        ULIntfs, this function returns a NULL pointer.
*/
intf_link_t *intf_ulintfs(intf_t *intf);

/**
@brief
Retrieve the Intf that is represented by the current iterator.

@param link The current LLIntf/ULIntf iterator.
@return The Intf is represented by the current iterator.
*/
intf_t *intf_link_current(intf_link_t *link);

/**
@brief
Iterate over the LLIntfs/ULIntfs.

@param link The current Intf iterator.
@return An iterator pointing to the next LLIntf/ULIntf. If the Intf contains no more LLIntfs/ULIntfs, this function returns a
        NULL pointer.
*/
intf_link_t *intf_link_next(intf_link_t *link);

/**
@brief
Create an ULIntf->LLIntf dependency if it does not exist already.

@details
This function invokes the link event listeners if the link needed to be created.

@param ulintf The ULIntf of the dependency or the Intf to add the LLIntf dependency to.
@param llintf The LLIntf of the dependency or the Intf to add the ULIntf dependency to.
*/
void intf_link(intf_t *ulintf, intf_t *llintf);

/**
@brief
Remove an ULIntf->LLIntf dependency if it exists.

@details
This function invokes the link event listeners if the link existed.

@param ulintf The ULIntf of the dependency or the Intf to remove the LLIntf dependency from.
@param llintf The LLIntf of the dependency or the Intf to remove the ULIntf dependency from.
*/
void intf_unlink(intf_t *ulintf, intf_t *llintf);

/**
@brief
Register an Intf flag event listener.

@param intf If NULL, subscribe for flag events on all Intfs. If non-NULL, subscribe only for flag events on the given Intf.
@param flag If NULL, subscribe for flag events for all flags. If non-NULL, subscribe only for flag events for the given flag.
@param l The callback function to be called whenever a flag is set or cleared.
@param userdata A void pointer to pass to the callback function.
@param boost If set to true, call the callback function with value=true before this function returns for each flag for which
             it would have been called if the listener was already registered since eternity (i.e. before the flags arose).
*/
void intf_addFlagListener(intf_t *intf, const char *flag, intf_flaglistener l, void *userdata, bool boost);

/**
@brief
Unregister an Intf flag event listener.

@param intf Should equal the intf argument of the corresponding call to intf_addFlagListener().
@param flag Should equal the flag argument of the corresponding call to intf_addFlagListener().
@param l Should equal the l argument of the corresponding call to intf_addFlagListener().
@param userdata Should equal the userdata argument of the corresponding call to intf_addFlagListener().
@param teardown If set to true, call the callback function with value=false before this function returns for each flag for which
                it would have been called if the listener was registered until eternity (i.e. when all flags are destroyed).
*/
void intf_delFlagListener(intf_t *intf, const char *flag, intf_flaglistener l, void *userdata, bool teardown);

/**
@brief
Register an Intf link event listener.

@param ulintf If NULL, subscribe for link events for any ULIntf. If non-NULL, subscribe only for link events for which the given
              Intf is the ULIntf.
@param llintf If NULL, subscribe for link events for any LLIntf. If non-NULL, subscribe only for link events for which the given
              Intf is the LLIntf.
@param l The callback function to be called whenever a link is created or removed.
@param userdata A void pointer to pass to the callback function.
@param boost If set to true, call the callback function with value=true before this function returns for each link for which
             it would have been called if the listener was already registered since eternity (i.e. before the links arose).
*/
void intf_addLinkListener(intf_t *ulintf, intf_t *llintf, intf_linklistener l, void *userdata, bool boost);

/**
@brief
Unregister an Intf link event listener.

@param ulintf Should equal the ulintf argument of the corresponding call to intf_addLinkListener().
@param llintf Should equal the llintf argument of the corresponding call to intf_addLinkListener().
@param l Should equal the l argument of the corresponding call to intf_addLinkListener().
@param userdata Should equal the userdata argument of the corresponding call to intf_addLinkListener().
@param teardown If set to true, call the callback function with value=false before this function returns for each link for which
                it would have been called if the listener was registered until eternity (i.e. when all links are destroyed).
*/
void intf_delLinkListener(intf_t *ulintf, intf_t *llintf, intf_linklistener l, void *userdata, bool teardown);

/**
@brief
Register an Intf management event listener.

@param intf If NULL, subscribe for management events on all Intfs. If non-NULL, subscribe only for management events on the given
            Intf.
@param create If non-NULL, the callback function to be called whenever an Intf is created. Should it be mentioned that it is rather
              pointless to provide a create handler if the intf argument is non-NULL?
@param modify If non-NULL, the callback function to be called whenever an Intf is modified. Refer to @ref intf_mgmtlistener to
              get details on what "modified" means in this context.
@param destroy If non-NULL, the callback function to be called whenever an Intf is destroyed. By the time this function is called,
               the Intf's flag set and ULIntf and LLIntf dependencies have been cleaned up already.
@param userdata A void pointer to pass to the callback functions.
@param boost If set to true, call the create handler before this function returns for each Intf for which
             it would have been called if the listener was already registered since eternity (i.e. before the Intfs arose).
*/
void intf_addMgmtListener(intf_t *intf, intf_mgmtlistener create, intf_mgmtlistener modify, intf_mgmtlistener destroy,
                          void *userdata, bool boost);

/**
@brief
Unregister an Intf management event listener.

@param intf Should equal the intf argument of the corresponding call to intf_addMgmtListener().
@param create Should equal the create argument of the corresponding call to intf_addMgmtListener().
@param modify Should equal the modify argument of the corresponding call to intf_addMgmtListener().
@param destroy Should equal the destroy argument of the corresponding call to intf_addMgmtListener().
@param userdata Should equal the userdata argument of the corresponding call to intf_addMgmtListener().
@param teardown If set to true, call the destroy handler before this function returns for each Intf for which
                it would have been called if the listener was registered until eternity (i.e. when all Intfs are destroyed).
*/
void intf_delMgmtListener(intf_t *intf, intf_mgmtlistener create, intf_mgmtlistener modify, intf_mgmtlistener destroy,
                          void *userdata, bool teardown);

/**
@brief
Register an Intf MIB event listener.

@param intf If NULL, subscribe for MIB events on all Intfs. If non-NULL, subscribe only for MIB events on the given Intf.
@param l The callback function to be called for every MIB event.
@param userdata A void pointer to pass to the callback function.

@remark
You can not "boost" MIB event listeners.

*/
void intf_addMibListener(intf_t *intf, intf_miblistener l, void *userdata);

/**
@brief
Unregister an Intf MIB event listener.

@param intf Should equal the intf argument of the corresponding call to intf_addMibListener().
@param l Should equal the l argument of the corresponding call to intf_addMibListener().
@param userdata Should equal the userdata argument of the corresponding call to intf_addMibListener().

@remark
You can not "teardown" MIB event listeners.

*/
void intf_delMibListener(intf_t *intf, intf_miblistener l, void *userdata);

/**
@brief
Attach an optional BLOB to an interface.

@details
It can be retrieved later on with intf_getBlob() and detached with intf_delBlob().

@param intf The Intf to add the BLOB to.
@param key The unique key of the BLOB, to be used later on to retrieve or detach the BLOB. This key is opaque to the core library
           so it can be anything, but it must be unique across all BLOBs of the Intf. A good example of a unique key is
           a pointer to a static variable.
@param blob The BLOB to attach to the Intf. The BLOB is opaque to the library, so its exact data type can be anything defined by
            the caller.
*/
void intf_addBlob(intf_t *intf, void *key, intf_blob_t *blob);

/**
@brief
Detach an optional BLOB from an interface.

@param intf The Intf to detach a BLOB from.
@param key The unique key of the BLOB. It should equal the key used to attach the BLOB.
*/
void intf_delBlob(intf_t *intf, void *key);

/**
@brief
Fetch an optional BLOB from an interface, given its key.

@param intf The Intf to retrieve a BLOB from.
@param key The unique key of the BLOB. It should equal the key used to attach the BLOB.
@return The BLOB that was attached using the given key or NULL if there is no such BLOB attached to the Intf.
*/
intf_blob_t *intf_getBlob(intf_t *intf, void *key);

/**
@}
*/

#endif
