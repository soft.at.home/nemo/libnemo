/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CORE_FLAGS_H__
#define __NEMO_CORE_FLAGS_H__

/**
@ingroup core
@defgroup flags flags.h - Flag set and flag expression management
@{

*/

#include <stdbool.h>

typedef struct _flagset flagset_t; /**< @brief Opaque object holding a flag set. */
typedef struct _flagset_iterator flagset_iterator_t; /**< @brief Handle for iterating over all flags of a flag set. */
typedef struct _flagexp flagexp_t; /**< @brief Opaque object holding a flag expression. */

/**
@brief
Flag set alert handler type.

@details
If a flag set alert handler is installed on a flag set, this handler is called every time a flag is set or cleared from the
flag set.

@param flagset The flag set that is modified.
@param flag The flag that is set/cleared.
@param value True if the flag was set, false if it was cleared.
@param userdata Userdata void pointer provided to flagset_alert().

@remarks
The handler is called right AFTER the flag is set or cleared.

@see flagset_alert()
*/
typedef void (*flagset_alert_handler)(flagset_t *flagset, const char *flag, bool value, void *userdata);

/**
@brief
Flag set constructor.

@details
Allocates memory to hold a flag set. The allocated memory should be released again with flagset_destroy().

@param recursive If set to true, the created flag set will keep a counter for each flag. Each time a specific flag is
                 set, the counter is incremented and when the flag is cleared, the counter is decremented. Only when the
                 counter reaches zero, the flag is really removed from the flag set. If set to
                 false, the flag set will have default behaviour, i.e. when clearing a flag, it is removed immediately
                 no matter how many times it has been set.
@return The newly created (empty) flag set.
*/
flagset_t *flagset_create(bool recursive);

/**
@brief
Reset or empty a flag set, i.e. clear all flags.

@param flagset The flag set to reset.
*/
void flagset_reset(flagset_t *flagset);

/**
@brief
Flag set destructor.

@details
Releases any allocated memory for the flag set.

@param flagset The flag set to destroy. When this function returns, this pointer is no longer valid.
*/
void flagset_destroy(flagset_t *flagset);

/**
@brief
Parse a space-separated list of flags and put them in a flag set.

@param flagset The flag set to store the parsed flags into.
@param str The string that holds the space separated list of flags.
@return True if the string is parsed successfully.
*/
bool flagset_parse(flagset_t *flagset, const char *str);

/**
@brief
Convert a flag set to a space-separated list of flags.

@param flagset The flag set to convert.
@return A string holding the space-separated list of flags. It's allocated on the heap and needs to be freed by the caller with
        free(). A NULL pointer is returned if flagset is NULL or if memory allocation failed.
*/
char *flagset_print(flagset_t *flagset);

/**
@brief
Get a handle for iterating over all flags held by a flag set.

@param flagset The flag set to iterate over.
@return An iterator pointing to the first flag of the flag set. It can be retrieved with flagset_iterator_current(). If the flag
        set is empty, this function returns a NULL pointer.
*/
flagset_iterator_t *flagset_iterator(flagset_t *flagset);

/**
@brief
Iterate over the flag set.

@param it The current flag set iterator.
@return An iterator pointing to the next flag of the flag set. If the flag set contains no more flags, this function returns a
        NULL pointer.
*/
flagset_iterator_t *flagset_iterator_next(flagset_iterator_t *it);

/**
@brief
Retrieve the flag that is represented by the current iterator.

@param it The current flag set iterator.
@return The flag that is represented by the current iterator.
*/
const char *flagset_iterator_current(flagset_iterator_t *it);

/**
@brief
Set a flag in a flag set.

@details
Add a flag to a flag set if it does not contain it already. If the flag set already contains the flag, increase its
counter if the flag set is a resursive flag set or do nothing it all for normal flag sets.

@param flagset The flag set on which a flag needs to be set.
@param flag The flag to set.
*/
void flagset_set(flagset_t *flagset, const char *flag);

/**
@brief
Clear a flag from a flag set.

@details
If the flag set contains the flag, decrease its counter if it is a recursive flag set. If it is a normal flag set, or the
flag's counter reaches zero, remove the flag from the flag set. If the flag set does not contain the flag, do not do
anything at all.

@param flagset The flag set on which a flag needs to be cleared.
@param flag The flag to clear.
*/
void flagset_clear(flagset_t *flagset, const char *flag);

/**
@brief
Check if a flag is set on a flag set.

@param flagset The flag set on which the flag needs to be checked. NULL is considered to be the empty flag set.
@param flag The flag to check.
@return True if and only if the flag is set.
*/
bool flagset_check(flagset_t *flagset, const char *flag);

/**
@brief
Get a flag counter.

@details
Get the counter of a specific flag if flag is non-NULL or the global flag set counter if flag is NULL. The global counter is the
sum of all flag counters. For recursive flag sets, the counter of a flag is the number of times the flag was set. For normal flag
sets, the counter equals 1 if the flag is set.

@param flagset The flag set for which a counter needs to be returned. NULL is considered to be the empty flag set.
@param flag The flag to count. If it equals NULL, the global flag set counter is returned.
@return A flag counter or the flag set's global counter.
*/
int flagset_count(flagset_t *flagset, const char *flag);

/**
@brief
Merge a flag set with another flag set.

@details
All flags that are set in the flag set referenced by operand, but not in the flag set referenced by flagset, are added to flagset.
Additionally, for recursive flag sets, the counter of each flag of flagset will be incremented by the counter of the corresponding
flag in operand.

@param flagset Left hand operand of the union operator, as well as the target to store the result into.
@param operand Right hand operand of the union operator. NULL is considered to be the empty flag set.
*/
void flagset_union(flagset_t *flagset, flagset_t *operand);

/**
@brief
Intersect a flag set with another flag set.

@details
All flags that are set in the flag set referenced by flagset, but not in the flag set referenced by operand, are removed from
flagset. Additionally, for recursive flag sets, the counter of each flag of flagset will be set to the minimum of itself and
the counter of the corresponding flag in operand.

@param flagset Left hand operand of the intersect operator, as well as the target to store the result into.
@param operand Right hand operand of the intersect operator. NULL is considered to be the empty flag set.
*/
void flagset_intersect(flagset_t *flagset, flagset_t *operand);

/**
@brief
Subtract a flag set from another flag set.

@details
For normal flag sets, all flags that are set in both the flag set referenced by flagset and the flag set referenced by operand, are
removed from flagset.

For recursive flag sets, the counter of each flag of flagset will be decremented by the counter of the corresponding flag in
operand. If this results in a counter equal to or less than zero, the flag is removed from flagset.

@param flagset Left hand operand of the subtract operator, as well as the target to store the result into.
@param operand Right hand operand of the subtract operator. NULL is considered to be the empty flag set.
*/
void flagset_subtract(flagset_t *flagset, flagset_t *operand);

/**
@brief
Compare two flag sets.

@details
Normal flag sets are considered to be equal if they contain the exact same set of flags.
For recursive flag sets, the flag counters have to match as well in order to be considered as equal.

@param flagset1 Left hand operand of the equals operator. NULL is considered to be the empty flag set.
@param flagset2 Right hand operand of the equals operator. NULL is considered to be the empty flag set.
@return True if and only if flagset1 and flagset2 are equal.
*/
bool flagset_equals(flagset_t *flagset1, flagset_t *flagset2);

/**
@brief
Install a flag set alert handler.

@details
For normal flag sets, this handler is called every time a flag is set or cleared from the flag set.

For recursive flag sets, this handler is called only whenever a flag is really added to or removed from the flag set, i.e. not
when setting or clearing a flag only caused a flag counter to be incremented or decremented.

@param flagset The flag set on which an alert handler needs to be installed.
@param handler The callback function to be called whenever a flag is set or cleared.
@param userdata A void pointer to be passed to the callback function.
*/
void flagset_alert(flagset_t *flagset, flagset_alert_handler handler, void *userdata);

/**
@brief
Flag expression constructor.

@details
Allocates memory to hold a flag expression. The allocated memory should be released again with flagexp_destroy().

@return The newly created (empty) flag expression, which evaluates to true by definition.
*/
flagexp_t *flagexp_create();

/**
@brief
Reset or empty a flag expression.

@param flagexp The flag expression to reset.
*/
void flagexp_reset(flagexp_t *flagexp);

/**
@brief
Flag expression destructor.

@details
Releases any allocated memory for the flag expression.

@param flagexp The flag expression to destroy. When this function returns, this pointer is no longer valid.
*/
void flagexp_destroy(flagexp_t *flagexp);

/**
@brief
Parse a written flag expression.

@details
A written flag expression is a string in which flag names are combined with the logical operator &&, || and !.
Subexpressions may be grouped with parentheses. This function parses such string, compiles it into binary form and stores it in
the provided flag expression.

@param flagexp The flag expression to store the compiled flag expression into.
@param str String holding the written flag expression to be parsed and compiled.
@return True if parsing the written flag expression succeeded.
*/
bool flagexp_parse(flagexp_t *flagexp, const char *str);

/**
@brief
Convert a flag expression into written form.

@param flagexp The flag expression to convert.
@return A string holding the written flag expression. It's allocated on the heap and needs to be freed by the caller with
        free(). A NULL pointer is returned if flagexp is NULL or if memory allocation failed.
*/
char *flagexp_print(flagexp_t *flagexp);

/**
@brief
Evaluate a flag expression on a flag set.

@param flagexp The flag expression to evaluate. NULL is considered to be the empty flag expression which evaluates to true by
               definition.
@param flagset The flag set to evaluate the flag expression on. NULL is considered to be the empty flag set.
@return The evaluation result.
*/
bool flagexp_evaluate(flagexp_t *flagexp, flagset_t *flagset);

/**
@brief
Find any leaf operand of a flag expression that equals a specific flag.

@param flagexp The flag expression to search through.
@param flag The flag to search for.
@return True if flag occurs in flagexp.
*/
bool flagexp_contains(flagexp_t *flagexp, const char *flag);

/**
@brief
Compare two flag expressions.

@param flagexp1 Left hand operand of the equals operator.
@param flagexp2 Right hand operand of the equals operator..
@return True if and only if flagexp1 and flagexp2 are equal.
*/
bool flagexp_equals(flagexp_t *flagexp1, flagexp_t *flagexp2);

/**
@}
*/

#endif
