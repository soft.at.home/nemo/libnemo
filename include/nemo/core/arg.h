/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CORE_ARG_H__
#define __NEMO_CORE_ARG_H__

/**
@ingroup core
@defgroup arg arg.h - Argument parsing utilities
@{

*/

#include <pcb/core/function.h>
#include <pcb/core/request.h>

#include <nemo/core/flags.h>
#include <nemo/core/intf.h>
#include <nemo/core/traverse.h>
#include <nemo/core/query.h>

#define ARG_BYNAME request_function_args_by_name /**< @brief Process the argument list by name lookup rather than by order. */
#define ARG_MANDATORY 0x80000000 /**< @brief Require the presence of an argument. */

/**
@brief
Translate a string argument holding an Intf name into an Intf.

@param target The target to store the Intf into. It is left untouched if the argument is absent or if translation failed.
@param string The string argument holding the Intf name to translate or NULL to indicate the absence of the argument.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
@param name Name of the argument. Used in warning messages.
@return True if the argument could be translated successfully or if it was absent and optional. False if translating the argument
        failed (i.e. Intf does not exist) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_parseIntf(intf_t **target, const char *string, uint32_t attributes, const char *name);

/**
@brief
Translate a string argument holding a written flag set into a flag set.

@param target The target to store the flag set into. It is left untouched if the argument is absent or if translation failed.
@param string The string argument holding the written flag set to translate or NULL to indicate the absence of the argument.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
@param name Name of the argument. Used in warning messages.
@return True if the argument could be translated successfully or if it was absent and optional. False if translating the argument
        failed (i.e. syntax error) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_parseFlagset(flagset_t **target, const char *string, uint32_t attributes, const char *name);

/**
@brief
Translate a string argument holding a written flag expression into a flag expression.

@param target The target to store the flag expression into. It is left untouched if the argument is absent or if translation failed.
@param string The string argument holding the written flag expression to translate or NULL to indicate the absence of the argument.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
@param name Name of the argument. Used in warning messages.
@return True if the argument could be translated successfully or if it was absent and optional. False if translating the argument
        failed (i.e. syntax error) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_parseFlagexp(flagexp_t **target, const char *string, uint32_t attributes, const char *name);

/**
@brief
Translate a string argument holding a traverse mode into a traverse mode enumeration value.

@param target The target to store the traverse mode enumeration value into.
              It is left untouched if the argument is absent or if translation failed.
@param string The string argument holding the traverse mode to translate or NULL to indicate the absence of the argument.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
@param name Name of the argument. Used in warning messages.
@return True if the argument could be translated successfully or if it was absent and optional. False if translating the argument
        failed (i.e. invalid traverse mode) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_parseTraverse(traverse_mode_t *target, const char *string, uint32_t attributes, const char *name);

/**
@brief
Translate a string argument holding a query class name into a query class.

@param target The target to store the query class into. It is left untouched if the argument is absent or if translation failed.
@param string The string argument holding the query class name to translate or NULL to indicate the absence of the argument.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
@param name Name of the argument. Used in warning messages.
@return True if the argument could be translated successfully or if it was absent and optional. False if translating the argument
        failed (i.e. invalid query class name) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_parseQueryClass(query_class_t **target, const char *string, uint32_t attributes, const char *name);

/**
@brief
Take a specific argument from the argument list.

@param target The target to store the argument into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_take(argument_value_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a string argument from the argument list.

@param target The target to store the string into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeChar(char **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a map argument from the argument list.

@param target The target to store the map into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeMap(variant_map_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take an Intf argument from the argument list.

@param target The target to store the Intf into. It is left untouched if the argument is absent or if parsing it failed.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found and parsed successfully or if it was absent and optional.
        False if parsing the argument failed (i.e. Intf does not exist) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeIntf(intf_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a flag set argument from the argument list.

@param target The target to store the flag set into. It is left untouched if the argument is absent or if parsing it failed.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found and parsed successfully or if it was absent and optional.
        False if parsing the argument failed (i.e. syntax error) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeFlagset(flagset_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a flag expression argument from the argument list.

@param target The target to store the flag expression into. It is left untouched if the argument is absent or if parsing it failed.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found and parsed successfully or if it was absent and optional.
        False if parsing the argument failed (i.e. syntax error) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeFlagexp(flagexp_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a traverse mode argument from the argument list.

@param target The target to store the traverse mode into. It is left untouched if the argument is absent or if parsing it failed.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found and parsed successfully or if it was absent and optional.
        False if parsing the argument failed (i.e. invalid traverse mode) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeTraverse(traverse_mode_t *target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a query class argument from the argument list.

@param target The target to store the query class into. It is left untouched if the argument is absent or if parsing it failed.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found and parsed successfully or if it was absent and optional.
        False if parsing the argument failed (i.e. invalid query class name) or if it was absent and mandatory.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeQueryClass(query_class_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take an uint32 argument from the argument list.

@param target The target to store the uint32 into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeUInt32(uint32_t *target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take an uint16 argument from the argument list.

@param target The target to store the uint16 into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeUInt16(uint16_t *target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take an uint8 argument from the argument list.

@param target The target to store the uint8 into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeUInt8(uint8_t *target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Take a list argument from the argument list.

@param target The target to store the list into. It is left untouched if the argument is absent.
@param args The argument list to search through. A NULL pointer is considered to be the empty argument list.
@param attributes Bitwise OR of zero or more of the following attributes:
                  - @ref ARG_MANDATORY Require the presence of the argument.
                  - @ref ARG_BYNAME Lookup the argument by name rather than just taking the first one (i.e. process by order).
@param name Name of the argument. Used in warning messages and to lookup the argument if @ref ARG_BYNAME is set.
@return True if the argument was found or it was optional. False if it was required but missing.

@remark
If this function fails, it also sets a global non-zero error (code,description,info)-tuple available through arg_error(),
arg_errorDescription() and arg_errorInfo(). If it succeeds, it sets the global error code to zero.
*/
bool arg_takeList(variant_list_t **target, argument_value_list_t *args, uint32_t attributes, const char *name);

/**
@brief
Get the global error code.
@return
The global error code.
*/
unsigned long arg_error();

/**
@brief
Get the global error description.
@return
The global error description.
*/
const char *arg_errorDescription();

/**
@brief
Get the global error info.
@return
The global error info.
*/
const char *arg_errorInfo();

/**
@brief
Set the global error (code,description,info)-tuple.

@param e The global error code to set.
@param eDescription The global error description to set. This should be a constant because this pointer is recorded and
                    kept until the next call to arg_setError(), arg_setSuccess(), arg_returnError() or arg_returnSuccess().
@param eInfo The global error info to set. This may be any string because the pointer is not recorded by this function. Instead,
             its content is copied to a fixed buffer.
@remark
The global error (code,description,info)-tuple is available through arg_error(), arg_errorDescription(), arg_errorInfo().
*/
void arg_setError(unsigned long e, const char *eDescription, const char *eInfo);

/**
@brief
Clear the global error (code,description,info)-tuple.

@details
This function assigns the empty string to the global error description and the global error info and sets the error code to 0.

@remark
The global error (code,description,info)-tuple is available through arg_error(), arg_errorDescription(), arg_errorInfo().
*/
void arg_setSuccess();

/**
@brief
Set the global error (code,description,info)-tuple and return false.

@param e The global error code to set.
@param eDescription The global error description to set. This should be a constant because this pointer is recorded and
                    kept until the next call to arg_setError(), arg_setSuccess(), arg_returnError() or arg_returnSuccess().
@param eInfo The global error info to set. This may be any string because the pointer is not recorded by this function. Instead,
             its content is copied to a fixed buffer.
@return False.
@remark
The global error (code,description,info)-tuple is available through arg_error(), arg_errorDescription(), arg_errorInfo().
*/
bool arg_returnError(unsigned long e, const char *eDescription, const char *eInfo);

/**
@brief
Clear the global error (code,description,info)-tuple and return true.

@details
This function assigns the empty string to the global error description and the global error info and sets the error code to 0.

@return True.

@remark
The global error (code,description,info)-tuple is available through arg_error(), arg_errorDescription(), arg_errorInfo().
*/
bool arg_returnSuccess();

/**
@brief
Get the global user ID.

@return The global user ID.
@see arg_setUserID()
*/
uint32_t arg_getUserID();

/**
@brief
Set the global user ID.

@details
Some functions of the NeMo Common API may behave differently depending on the user ID of the corresponding data model function
execution request. More precisely nemo_setFlag(), nemo_clearFlag(), nemo_getFirstParameter(), nemo_setFirstParameter(),
nemo_getParameters(), nemo_setParameters(), nemo_getMIBs(), nemo_setMIBs() and nemo_openQuery(). Example: nemo_setFirstParameter()
checks if the calling user has write access to a parameter before effectively writing to that parameter. This works of course only
if the function was called from within a PCB client context. However, NeMo modules are capable of calling such a common API function
as if it was called from within a PCB client context with a given user ID by setting the global user ID.

@warning
The global user ID must be cleared with arg_clearUserID() after using it.

@param uid The new global user ID.
*/
void arg_setUserID(uint32_t uid);

/**
@brief
Clear the global user ID.

@see arg_setUserID()
*/
void arg_clearUserID();

/**
@}
*/

#endif
