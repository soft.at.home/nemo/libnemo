/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CORE_MAP_h__
#define __NEMO_CORE_MAP_h__

/**
@ingroup core
@defgroup map map.h - NeMo object mapping utility
@{

*/

#include <pcb/utils/variant.h>
#include <pcb/core/object.h>
#include <pcb/core/request.h>
#include <pcb/core/reply.h>

#define MAP_DEFAULT 0 /**< @brief Default mapping attributes
@details
This is
- Map in both directions, i.e. from NeMo to the destination plugin and from the destination plugin to NeMo.
- The initial values in NeMo are pushed to the destination plugin rather than learning the initial values from the destination
  plugin.
- Assume the existence of the destination object.
*/
#define MAP_READONLY 1 /**< @brief Map only from destination plugin to NeMo, never push values to the destination plugin.*/
#define MAP_WRITEONLY 2 /**< @brief Map only from NeMo to destination plugin, never pull values from the destination plugin.*/
#define MAP_LEARN 4 /**< @brief Learn initial values from the destination plugin rather than pushing the initial values of NeMo to
                                the destination plugin.*/
#define MAP_CREATOR 8 /**< @brief The destination object needs to be created when the mapping is established.*/
#define MAP_KILLER 16 /**< @brief The destination object needs to be deleted when the mapping is released.*/
#define MAP_OWNER (MAP_CREATOR|MAP_KILLER) /**< @brief The destination object is "owned" by the mapping, i.e. it must be created
                                                       when the mapping is established and deleted when the mapping is released.*/
#define MAP_KEY 32 /**< @brief Indicate that a parameter is a "key"-parameter of the destination object, i.e. it can only be set
                               once, namely while creating the destination object.*/
#define MAP_NOTIFICATIONS 64 /**< @brief Forward custom notifications from the destination object to subscribers.*/

/**
@brief
Enumeration of mapping directions.

@details
Used to pass to parameter translator handlers. See @ref map_parameter_translator.
*/
typedef enum _map_direction {
	map_from_destination, /**< @brief Map from destination plugin to NeMo (used when processing replies on get requests). */
	map_to_destination, /**< @brief Map from NeMo to destination plugin (used when composing set requests). */
} map_direction_t;

typedef struct _map map_t; /**< @brief Opaque object holding an object map.*/
typedef struct _map_parameter map_parameter_t; /**< @brief Opaque object holding one parameter of an object map.*/
typedef struct _map_function map_function_t; /**< @brief Opaque object holding one function of an object map.*/
typedef struct _map_call map_call_t; /**< @brief Opaque object holding one mapped function call related to an object map.*/

/**
@brief
Parameter value translation callback function type.

@param parameter Parameter for which a value needs to be translated.
@param direction Indicates in which direction the parameter needs to be translated.
@param value The value to be translated. The translated value should also be stored in this variant.
*/
typedef void (*map_parameter_translator)(map_parameter_t *parameter, map_direction_t direction, variant_t *value);

/**
@brief
Callback function type for before/after set-request/get-reply triggers.

@param map Map for which trigger is called.
@param request The request for which this trigger is called.

@remark
If the callback function needs the hand of an extra void pointer, use map_setUserData() / map_userdata().
*/
typedef void (*map_requestHandler)(map_t *map, request_t *request);

/**
@brief
Callback function type for before/after get-reply triggers.

@param map Map for which trigger is called.
@param item The reply item for which this trigger is called.

@remark
If the callback function needs the hand of an extra void pointer, use map_setUserData() / map_userdata().
*/
typedef void (*map_replyHandler)(map_t *map, reply_item_t *item);

/**
@brief
Callback function type for notification triggers.

@param map Map for which this trigger is called.
@param notification The notification for which this trigger is called.
@return True to forward the notification, false to drop the notification.
*/
typedef bool (*map_notificationHandler)(map_t *map, notification_t *notification);

/**
@brief
Callback function type for mapped function call triggers.

@param call The mapped function call for which this trigger is called.
*/
typedef void (*map_call_handler)(map_call_t *call);

/**
@brief
Map constructor.

@details
Allocates memory to hold an object map. This function doesn't actually establish the map. This is done with map_commit(), preferably
after parameters are added to the map.

@param object The local object to be mapped. This pointer is kept by the map until it is destroyed again, so please, make sure
              that the object is not destroyed before the map.
@param attributes Bitwise OR of zero or more the following constants:
                  - @ref MAP_DEFAULT
                  - @ref MAP_WRITEONLY
                  - @ref MAP_READONLY
                  - @ref MAP_LEARN
                  - @ref MAP_CREATOR
                  - @ref MAP_KILLER
                  - @ref MAP_OWNER
                  - @ref MAP_NOTIFICATIONS
@param dst Peer through which the destination plugin is reachable. If this is a NULL pointer, the default peer returned by
           plugin_getPeer() is used.
@param dstpath printf()-compatible format string identifying the path of the destination object in dot-separated key path notation.
@param ... Arguments relative to the dstpath format string.
@return The created map. To be released with map_destroy().
*/
map_t *map_create(object_t *object, int attributes, peer_info_t *dst, const char *dstpath, ...);

/**
@brief
Map destructor.

@details
Releases the mapping and internally associated data structures, including all parameters.

@param map The map to destroy.
*/
void map_destroy(map_t *map);

/**
@brief
Establish the mapping and/or push local changes to the destination plugin.

@details
This function should be called for the first time shortly after map_create() to establish the mapping (after all parameters have
been added to the map) and after that it should be called again whenever changes have been made to the local object that should be
pushed to the destination plugin potentially (only changed parameters are pushed and if no parameters changed, no request is sent).
In other words, you should call it from the object write handler of the local object (or from a @ref intf_mgmtlistener if the local
object is a NeMo.Intf.{i} instance).

@param map The map to commit.
*/
void map_commit(map_t *map);

/**
@brief
Refresh the local object by explicitly sending a get-request again to the destination plugin.

@details
When a mapping is established (after the first call to map_commit()), a get-request has been sent to the destination plugin with
event notifications enabled to keep the local object synchronized with the destination object. This causes problem for volatile
parameters (i.e. parameters that may change without event notification). To keep this kind of parameters up to date, the map needs
to refreshed explicitly. Please be moderate calling this function, in fact it should be avoided.

@remark
In NeMo all I/O that may block is processed asynchronously, this function is no exception. It will return before the local object
is actually refreshed.

@param map The map to refresh.
*/
void map_refresh(map_t *map);

/**
@brief
Set a trigger to be called right before sending a set-request.

@param map Map to set a trigger on.
@param handler The callback function to be called right before sending a set-request.
*/
void map_setBeforeSetRequestHandler(map_t *map, map_requestHandler handler);

/**
@brief
Set a trigger to be called right after sending a set-request.

@param map Map to set a trigger on.
@param handler The callback function to be called right after sending a set-request.
*/
void map_setAfterSetRequestHandler(map_t *map, map_requestHandler handler);

/**
@brief
Set a trigger to be called right before processing a reply to a get-request.

@param map Map to set a trigger on.
@param handler The callback function to be called right before processing a reply to a get-request.
*/
void map_setBeforeGetReplyHandler(map_t *map, map_replyHandler handler);

/**
@brief
Set a trigger to be called right after processing a reply to a get-request.

@param map Map to set a trigger on.
@param handler The callback function to be called right after processing a reply to a get-request.
*/
void map_setAfterGetReplyHandler(map_t *map, map_replyHandler handler);

/**
@brief
Set a trigger to be called when a custom notification is received from the destination object.

@details
Notification handlers can be used to
- edit custom notifications before forwarding them to subscribers.
- filter custom notifications.

@param map Map to set a trigger on.
@param handler The callback function to be called when a custom notification is received.
*/
void map_setNotificationHandler(map_t *map, map_notificationHandler handler);

/**
@brief
Associate a map with any kind of void pointer.

@param map The map to associate with the void pointer.
@param userdata The void pointer to associate the map with.
*/
void map_setUserdata(map_t *map, void *userdata);

/**
@brief
Get the path of the destination object of a map.

@param map The map to get the path of destination object from.
@return The path of the destination object.
*/
const char *map_dstpath(map_t *map);

/**
@brief
Get the local object of a map.

@param map The map to get the local object from.
@return The local object.
*/
object_t *map_object(map_t *map);

/**
@brief
Check if a map is busy processing a reply to a get request.

@remark
When a reply to a get-request (i.e. initial get reply or event notification) is received, the map updates the local object
autonomously from within the reply handler. This causes the object write handler and perhaps several parameter write handlers to
be called. These handlers might want to behave differently depending on whether they were called from within the reply handler
of the get request of the map. However, do not bother checking if the map is not busy before calling map_commit(), map_commit()
already checks it itself.

@param map The map to check.
@return True if the map is busy processing a reply to a get request.
*/
bool map_busy(map_t *map);

/**
@brief
Get the void pointer that is associated with a map.

@param map The map to get the associated void pointer from.
@return The void pointer.
*/
void *map_userdata(map_t *map);

/**
@brief
Add a parameter to a map.

@param map Map to add the parameter to.
@param parameter The local parameter to map. This pointer is kept by the map until this parameter is removed from the map again
                 (or the map is destroyed), so please, make sure the parameter is not destroyed before it is removed from the map.
@param dstname The name of the corresponding parameter in the destination object. If a NULL pointer is provided, the name of the
               local parameter is used.
@param attributes Bitwise OR of zero or more of the following constants:
                  - @ref MAP_DEFAULT
                  - @ref MAP_WRITEONLY
                  - @ref MAP_READONLY
                  - @ref MAP_LEARN
                  - @ref MAP_KEY
@param translate An optional callback function to translate the parameter value between the local object and the destination object.
@param userdata A void pointer to pass to the callback function.
@return The newly created map parameter. To be released again with map_parameter_destroy() or map_destroy().
*/
map_parameter_t *map_parameter_create(map_t *map, parameter_t *parameter, const char *dstname, int attributes,
                                      map_parameter_translator translate, void *userdata);

/**
@brief
Remove a parameter from a map.

@param parameter Map parameter to remove. This pointer will be invalid after returning from this function.
*/
void map_parameter_destroy(map_parameter_t *parameter);

/**
@brief
Get the first parameter of a map.

@param map The map to get the first parameter from.
@return The first parameter of the map.
*/
map_parameter_t *map_parameter_first(map_t *map);

/**
@brief
Get the next parameter of a map.

@param parameter The current map parameter.
@return The next parameter of the map.
*/
map_parameter_t *map_parameter_next(map_parameter_t *parameter);

/**
@brief
Get a map parameter given its local name.

@param map The map to get the parameter from.
@param name The name of the parameter to get. This should be the name of the local parameter, not of the destination parameter.
@return The parameter if it is found. NULL otherwise.
*/
map_parameter_t *map_parameter_findByName(map_t *map, const char *name);

/**
@brief
Get a map parameter given its destination name.

@param map The map to get the parameter from.
@param dstname The name of the parameter to get. This should be the name of the destination parameter, not of the local parameter.
@return The parameter if it is found. NULL otherwise.
*/
map_parameter_t *map_parameter_findByDstName(map_t *map, const char *dstname);

/**
@brief
Get the map that owns a given map parameter.

@param parameter The parameter to get the owning map from.
@return The map that owns the given parameter.
*/
map_t *map_parameter_map(map_parameter_t *parameter);

/**
@brief
Get the local parameter of a map parameter.

@param parameter The map parameter to get the local parameter from.
@return The local parameter of the map parameter.
*/
parameter_t *map_parameter_parameter(map_parameter_t *parameter);

/**
@brief
Get the name of the destination parameter name of a map parameter.

@param parameter The map parameter to get the name of the destination parameter name from.
@return The name of the destination parameter of the map parameter.
*/
const char *map_parameter_dstname(map_parameter_t *parameter);

/**
@brief
Get the userdata pointer of a map parameter.

@param parameter The map parameter to get the userdata pointer from.
@return The userdata pointer of the map parameter.
*/
void *map_parameter_userdata(map_parameter_t *parameter);

/**
@brief
Get the attributes of a map parameter.

@param parameter The map parameter to get the attributes from.
@return The attributes of a map parameter.
*/
int map_parameter_attributes(map_parameter_t *parameter);

/**
@brief
Add a function to a map.

@param map Map to add the function to.
@param function The local function to map. This pointer is kept by the map until this function is removed from the map again
                 (or the map is destroyed), so please, make sure the function is not destroyed before it is removed from the map.
@param dstname The name of the corresponding function in the destination object. If a NULL pointer is provided, the name of the
               local function is used.
@param sendHandler An optional callback function that may mangle the outgoing request before sending it.
@param finishHandler An optional callback function that may mangle the response to the caller before sending it.
@param destroyHandler An optional callback function to cleanup call specific userdata.
@param userdata A void pointer to be used by the send and/or reply handler. CAUTION: This is <b>function</b> specific userdata
                and should not be confused with <b>call</b> specific user data which should be cleaned up by the destroyHandler.
@return The newly created map function. To be released again with map_function_destroy() or map_destroy().
*/
map_function_t *map_function_create(map_t *map, function_t *function, const char *dstname, map_call_handler sendHandler,
                                    map_call_handler finishHandler, map_call_handler destroyHandler, void *userdata);

/**
@brief
Remove a function from a map.

@param function Map function to remove. This pointer will be invalid after returning from this function.
*/
void map_function_destroy(map_function_t *function);

/**
@brief
Get the first function of a map.

@param map The map to get the first function from.
@return The first function of the map.
*/
map_function_t *map_function_first(map_t *map);

/**
@brief
Get the next function of a map.

@param function The current map function.
@return The next function of the map.
*/
map_function_t *map_function_next(map_function_t *function);

/**
Get a map function given its local name.

@param map The map to get the function from.
@param name The name of the function to get. This should be the name of the local function, not of the destination function.
@return The function if it is found. NULL otherwise.
*/
map_function_t *map_function_findByName(map_t *map, const char *name);

/**
Get a map function given its destination name.

@param map The map to get the function from.
@param dstname The name of the function to get. This should be the name of the destination function, not of the local function.
@return The function if it is found. NULL otherwise.
*/
map_function_t *map_function_findByDstName(map_t *map, const char *dstname);

/**
@brief
Get the map that owns a given map function.

@param function The function to get the owning map from.
@return The map that owns the given function.
*/
map_t *map_function_map(map_function_t *function);

/**
@brief
Get the local function of a map function.

@param function The map function to get the local function from.
@return The local function of the map function.
*/
function_t *map_function_function(map_function_t *function);

/**
@brief
Get the name of the destination function name of a map function.

@param function The map function to get the name of the function parameter name from.
@return The name of the destination function of the map function.
*/
const char *map_function_dstname(map_function_t *function);

/**
@brief
Get the userdata pointer of a map function.

@param function The map function to get the userdata pointer from.
@return The userdata pointer of the map function.
*/
void *map_function_userdata(map_function_t *function);

/**
@brief
Invoke a map function.

@details
This function creates a mapped function call context, takes ownership of the provided local function call context,
creates and sends the corresponding outgoing function execution request and follows up that request. When the outgoing request
is completed, a reply is generated and sent for the local function call and the mapped function call context is destroyed by
the mapper itself.

@param function The invoked map function. This may be a NULL pointer, in that case the local function call will be replied with an
                appropriate error descriptor.
@param fcall The local function call context. This function takes ownership of this pointer, regardless any error conditions.
             This means
             - After calling this function, the caller MUST NOT call fcall_destroy() for the provided local function call context.
             - The mapper installs userdata and a cancel handler at the local function call context, so the caller should keep its
               hands off once this function is called.
@param args The input arguments of the function call, as provided to the local function handler.
@param retval Should be the return value as provided to the local function handler. However, this function doesn't do anything with
              it.
@param userdata Additional call specific userdata to be used by the send/reply handlers. If this is a malloc'ed block or any other
                kind of resource consuming data that needs to be released, it should be cleaned up by the destroyHandler passed
                to map_function_create(). However, there are some important corner cases. If this function returns NULL (because an
                error occurred, e.g. calloc() failed or the outgoing request could not be sent), the destroyHandler will NOT
                be called and then it's up to the caller of this function to release any resources associated with this userdata.
@return The mapped function call context or NULL if an error occurred. This pointer may be released explicitly with
        map_call_destroy() or map_call_finish() or it may also be destroyed implicitly when the map function or the map is
        destroyed, but all of those are corner cases and should not happen operationally. The main case is that the pointer is
        destroyed implicitly because the function call finished. So be careful when recording this pointer. Normally the caller
        should not record it, but if it's needed for any particular reason anyway, it can be done safely on the condition that it
        is erased again by the map function's destroy handler.
*/
map_call_t *map_call_create(map_function_t *function, function_call_t *fcall, argument_value_list_t *args, variant_t *retval,
                            void *userdata);

/**
@brief
Destroy a mapped function call.

@details
This brutely releases the resources associated with the mapped function call, without notifying the caller. So if the caller
has its function execution request still open, it will remain open and for the caller it will seem as if function call never
terminates. Don't call this unless if you reply to the caller yourself or if you have your reasons to assume that the caller
is no longer interested in the reply. Notice however that if the caller cancels the function execution request, or if it is
canceled by any other mean, it will be catched by the mapper's local function call cancel handler and the mapped function call
context will be destroyed automatically.

@param call The mapped function call to destroy. This pointer will be invalid after returning from this function.
*/
void map_call_destroy(map_call_t *call);

/**
@brief
Finish and destroy mapped function call.

@details
This function writes the reply to the caller and then call map_call_destroy() to release the resources associated with the mapped
function call. The reply written to the caller includes
- The function return value accessible through map_call_retval().
- The output arguments accessible through map_call_arguments().
- The error list accessible through the reply of the request associated with the local function call context returned by
  map_call_fcall().
.
Normally this function doesn't have to be called because it is called automatically by the mapper when
- The outgoing request was completed.
- The outgoing request was canceled because of some error condition (e.g. the destination plugin crashed).
- The map function or map was destroyed while the call was still open.

@param call The mapped function call to finish. This pointer will be invalid after returning from this function.
*/
void map_call_finish(map_call_t *call);

/**
@brief
Get the first open call of a map function.

@param function The map function to get the first open call of.
@return The first open call of a map function.
*/
map_call_t *map_call_first(map_function_t *function);

/**
@brief
Get the next open call of a map function.

@param call The current call.
@return The next open call of a map function.
*/
map_call_t *map_call_next(map_call_t *call);

/**
@brief
Get the map function corresponding to a mapped function call.

@param call The mapped function call to get the corresponding map function of.
@return The map function corresponding to the mapped function call.
*/
map_function_t *map_call_function(map_call_t *call);

/**
@brief
Get the local function call context corresponding to a mapped function call.

@param call The mapped function call to get the corresponding local function call context of.
@return The local function call context corresponding to the mapped function call.
*/
function_call_t *map_call_fcall(map_call_t *call);

/**
@brief
Get the outgoing request corresponding to a mapped function call.

@warning
Not to be confused with the incoming function execution request which is accessible through the local function call context!

@details
By default, the outgoing request equals the incoming request except that the object path is translated into the destination object
path of the map and the function name is translated into the destination function name. If needed, other modifications may be done
by the sendHandler of the map function.

@param call The mapped function call.
@return The outgoing request corresponding to the mapped function call.
*/
request_t *map_call_request(map_call_t *call);

/**
@brief
Get the output arguments corresponding to a mapped function call.

@warning
Not to be confused with the input arguments of the outgoing request or the incoming request!

@details
By default, the output arguments are copied from the reply of the outgoing request to the reply of the incoming request. If needed,
the output argument list may be modified by the finishHandler of the map function.

@param call The mapped function call.
@return The output argument list of the mapped function call.
*/
argument_value_list_t *map_call_arguments(map_call_t *call);

/**
@brief
Get the return value corresponding to a mapped function call.

@details
By default, the return value is copied from the reply of the outgoing request to the reply of the incoming request. If needed,
the return value may be modified by the finishHandler of the map function.

@param call The mapped function call.
@return The return value of the mapped function call.
*/
variant_t *map_call_retval(map_call_t *call);

/**
@brief
Get the call specific userdata associated with a mapped function call.

@param call The mapped function call.
@return The call specific userdata associated with the mapped function call.
*/
void *map_call_userdata(map_call_t *call);

/**
@}
*/

#endif
