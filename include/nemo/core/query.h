/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __NEMO_CORE_QUERY_H__
#define __NEMO_CORE_QUERY_H__

/**
@ingroup core
@defgroup query query.h - Query management
@{

@brief
This is the API of the generic part of query management. It can be used by modules to register query support for their own
functions.
*/

#include <stdbool.h>

#include <pcb/utils/variant.h>
#include <pcb/core/function.h>
#include <pcb/core/request.h>

#include "nemo/core/flags.h"
#include "nemo/core/intf.h"

typedef struct _query_class query_class_t; /**< @brief Opaque object holding a query class. A query class is a function with
                                                       registered query support. */

typedef struct _query query_t; /**< @brief Opaque object holding a single opened query*/

typedef struct _query_args query_args_t; /**< @brief Data type used to reference query arguments in binary form.
                                                     The exact structure to be defined by the query class implementation. */

/**
@brief
Set of information and callback functions to be provided by a query class implementation.
*/
typedef struct _query_class_info {
	const char *name; /**< @brief Name of the query class. It should equal the name of the data model function, e.g. "isUp" */
	variant_type_t type; /**< @brief Type of the query result. Not strictly necessary, it only causes variants holding the result
	                                 to be intialized correctly at forehand. */
	/**
	@brief
	Convert an argument list into a binary arguments object of which the layout is to be defined by the implementation.

	@details
	This function is called whenever a query of this class is opened. It should preprocess the argument list as much as
	possible and translate it into a ready-to-use implementation specific arguments object. It is the resulting
	arguments object that is kept by the query instance, rather than the originating argument list, to avoid the overhead of this
	step during each query recalculation.

	@param cl The query class for which arguments need to be converted.
	@param args The target to store the binary arguments object into. The pointer should be set to something allocated on the heap,
	            if parsing the argument list succeeded. If parsing the argument list failed, this function should not leave anything
	            allocated on the heap and this pointer should be left untouched.
	@param arglist The argument list.
	@param attributes Bitwise OR of attributes regarding the arglist argument. Currently the only supported attribute is
	                  @ref ARG_BYNAME, indicating that the arguments should be processed by name lookup rather than by order.
	@return True if parsing the argument list succeeded and the resulting binary arguments object has been stored into the args
	        argument, false otherwise. The relation between producing an args object and returning true is very important because
	        args_destroy() is guaranteed to be called if and only if this function returns true. If this function returns
	        false, it should have set the global error code (see arg_setError()) and it should have logged a warning level message
	        describing the error.
	*/
	bool (*args_convert)(query_class_t *cl, query_args_t **args, argument_value_list_t *arglist, uint32_t attributes);

	/**
	@brief
	Binary arguments object destructor.

	@details
	This function is guaranteed to be called exactly once for each args object produced by a successful call to the args_convert()
	callback.

	@param cl The query class for which arguments need to be destroyed.
	@param args The binary arguments object to destroy.
	*/
	void (*args_destroy)(query_class_t *cl, query_args_t *args);

	/**
	@brief
	Compare two binary arguments objects and return true if they are equal.

	@details
	This function is used to
	- Check if a query to be opened equals one of the previously opened queries with the intention to multiplex equal queries.
	- Find an opened query that matches the arguments of a call to closeQuery().

	@param cl The query class for which arguments need to be compared.
	@param args1 The first binary arguments object.
	@param args2 The second binary arguments object.
	@return True if they are equal.
	*/
	bool (*args_compare)(query_class_t *cl, query_args_t *args1, query_args_t *args2);

	/**
	@brief
	Convert a binary arguments object into a human readable string.

	@details
	This function is used to present a description of a query in the data model of NeMo, mainly intended for debugging.
	It should produce a comma-separated list of arguments in pcb_cli syntax, e.g. "arg1=value1, arg2=value2". This string will be
	included by the query description, surrounded by parentheses and prepended by the path and function of the query, e.g.
	"NeMo.Intf.i.foo(arg1=value1, arg2=value2)".

	@param cl The query class for which arguments need to be described.
	@param args The binary arguments object to be described.
	@return A human readable string describing the arguments, allocated on the heap. It will be released using free() shortly
	        after calling this function.
	*/
	char *(*args_describe)(query_class_t *cl, query_args_t *args);

	/**
	@brief
	Optional callback function to be called whenever a query of this class is effectively opened.

	@details
	Effectively in this context means that this callback function is called only once for several openQuery() calls that are
	multiplexed to the same Query object.

	@param q The opened query.
	*/
	void (*open)(query_t *q);

	/**
	@brief
	Optional callback function to be called whenever a query of this class is effectively closed.

	@details
	Effectively in this context means that this callback function is called only once for a multiplexed query, namely when
	the last subscriber calls closeQuery().

	@param q The closed query.
	*/
	void (*close)(query_t *q);

	/**
	@brief
	The query function.

	@param q The query to be (re)calculated.
	@param result The variant to store the result into.
	*/
	void (*run)(query_t *q, variant_t *result);

	void *userdata; /**< @brief Void pointer to be used freely by callback functions. */
} query_class_info_t;

/**
@brief
A set of general event listener callback functions for queries.
*/
typedef struct _query_listener {
	/**
	@brief
	A query has been opened.

	@details
	This function is not called when a call to openQuery() resulted in an existing, multiplexed Query object.

	@param query The opened query.
	@param userdata Void pointer passed to query_addListener()
	*/
	void (*opened)(query_t *query, void *userdata);

	/**
	@brief
	A query has been closed.

	@details
	This function is not called when the caller of closeQuery() was not the last subscriber of the query.

	@param query The closed query.
	@param userdata Void pointer passed to query_addListener()
	*/
	void (*closed)(query_t *query, void *userdata);

	/**
	@brief
	The set of subscribers of a query has been changed.

	@param query The altered query.
	@param userdata Void pointer passed to query_addListener()
	*/
	void (*altered)(query_t *query, void *userdata); // set of subscribers changes

	/**
	@brief
	The query result has changed.

	@details
	This is function not always called when the query was recalculated, but only if the result really changed.

	@param query The triggered query.
	@param userdata Void pointer passed to query_addListener()
	*/
	void (*trigger)(query_t *query, void *userdata); // query result has changed
} query_listener_t;

/**
@brief
Register a query class.

@param info A set of query class specific information and callback functions.
@return An opaque object holding the query class. It should be released again using query_class_unregister().
*/
query_class_t *query_class_register(query_class_info_t *info);

/**
@brief
Unregister a query class.

@param cl The query class to unregister.
*/
void query_class_unregister(query_class_t *cl);

/**
@brief
Get the info object of a query class holding the query class specific information and callback functions.

@param cl The query class to get the info object from.
@return The info object passed to query_class_register().
*/
query_class_info_t *query_class_info(query_class_t *cl);

/**
@brief
Find a query class by name.

@param name Name of the query class. This should equal the name of the corresponding function with query support, e.g. "isUp".
@return The query class if it was found. NULL otherwise.
*/
query_class_t *query_class_find(const char *name);

/**
@brief
Start iterating over all registered query classes.

@return The first registered query class.
*/
query_class_t *query_class_first();

/**
@brief
Iterate over all registered query classes.

@param cl The current registered query class.
@return The next registered query class.
*/
query_class_t *query_class_next(query_class_t *cl);

/**
@brief
Open a query of the specified query class.

@param uid The user id of the caller. Must be set to 0 for internal usage and to the user id of the corresponding PCB request for
           external usage.
@param subscriber An arbitrary subscriber name.
@param intf The Intf to open the query on.
@param cl The query class to open a query of.
@param arglist The arguments to be passed to the query function.
@param attributes Bitwise OR of attributes regarding the arglist argument. Currently the only supported attribute is
                  @ref ARG_BYNAME, indicating that the arguments should be processed by name lookup rather than by order.
@return A query object if opening the query succeeded. This object is to be released with query_destroy() or with a subsequent
        call to query_close() with a matching set of arguments. Notice that the returned query pointer might point to a newly
        created query object or to an existing query object if the arguments match any previously opened query. If opening the
        query failed, a NULL pointer is returned, the global error code will have been set and a warning level message is
        logged.
*/
query_t *query_open(uint32_t uid, const char *subscriber, intf_t *intf, query_class_t *cl, argument_value_list_t *arglist, uint32_t attributes);

/**
@brief
Close a query given its arguments.

@param uid The user id of the caller. Must be set to 0 for internal usage and to the user id of the corresponding PCB request for
           external usage.
@param subscriber An arbitrary subscriber name.
@param intf The Intf to close a query on.
@param cl The query class to close a query of.
@param arglist The arguments to be passed to the query function.
@param attributes Bitwise OR of attributes regarding the arglist argument. Currently the only supported attribute is
                  @ref ARG_BYNAME, indicating that the arguments should be processed by name lookup rather than by order.
@remark
This function might actually destroy a query object, but only if there was a matching query object found for which the provided
subscriber was the last one.
*/
void query_close(uint32_t uid, const char *subscriber, intf_t *intf, query_class_t *cl, argument_value_list_t *arglist, uint32_t attributes);

/**
@brief
Brutely destroy a query object.

@param q The query to destroy.
*/
void query_destroy(query_t *q);

/**
@brief
Start iterating over the opened queries of a specific query class.

@param cl The query class to start iterating over.
@return The first query of the query class.
*/
query_t *query_first(query_class_t *cl);

/**
@brief
Iterate over the opened queries of a specific query class.

@param q The current query.
@return The next query of the query class.
*/
query_t *query_next(query_t *q);

/**
@brief
Get the ID of a query.

@details
This equals the key of the corresponding NeMo.Intf.{i}.Query.{j} instance.

@param q The query to get the ID of.
@return The non-zero query ID.
*/
unsigned int query_id(query_t *q);

/**
@brief
Get a human readable description of the query.

@param q The query to get a description of.
@return A human readable description of the query, allocated on the heap. It should be released by the caller with free().
        The returned string looks like a pcb_cli command in dot-separated key path notation, e.g.
        NeMo.Intf.x.isUp(flag=y, traverse=down).
*/
char *query_describe(query_t *q);

/**
@brief
Get the set of subscribers of a query.

@param q The query to get the subscribers of.
@return The set of subscribers modeled as a recursive flag set. This is a pointer to an internal structure, please consider it to
        be read-only.
*/
flagset_t *query_subscribers(query_t *q);

/**
@brief
Get the current cached query result

@param q Query to get the result from.
@return The current cached query result.
*/
const variant_t *query_result(query_t *q);

/**
@brief
Get the binary arguments object of a query.

@param q Query to get the binary arguments object from.
@return The binary arguments object of the query.
*/
query_args_t *query_args(query_t *q);

/**
@brief
Get the Intf to which a query applies.

@param q The query to get the Intf from.
@return The Intf to which the query applies.
*/
intf_t *query_intf(query_t *q);

/**
@brief
Get the query class of which a query is an instance.

@param q The query instance to get the class of.
@return The query class of which the query is an instance.
*/
query_class_t *query_class(query_t *q);

/**
@brief
Get the query class name of a query.

@param q The query to get the class name of.
@return The query class name of the query.
*/
const char *query_classname(query_t *q);

/**
@brief
Get the return type of a query.

@param q The query to get the return type of.
@return The return type of the query.
*/
variant_type_t query_type(query_t *q);

/**
@brief
Get the user id of a query.

@param q The query to get the user id of.
@return The user id of the query.
*/
uint32_t query_uid(query_t *q);

/**
@brief
Trigger the recalculation of all query instances of a given query class at once.

@details
This function calls query_invalidate() for each instance of the provided query class.

@param cl The query class to invalidate.
*/
void query_class_invalidate(query_class_t *cl);

/**
@brief
Trigger the recalculation of a query.

@details
Queries are never recalculated automatically by the generic query management component. The triggering of queries completely relies
on this function getting called from within the query class implementation every time a query result potentially changed. Notice
that queries should be invalidated as little as possible to minimize the CPU time consumption (query recalculations contribute
significantly to the total amount of NeMo's CPU time consumption), ideally only when the query result has changed for sure, but
certainly not less than that. False positives (recalculating the query result while it hasn't changed) cause less optimal
performance, but false negatives (not recalculating the query result while it changed) break the query mechanism severely.

@param q The query to be invalidated.
*/
void query_invalidate(query_t *q);

/**
@brief
Register a general query listener.

@param q The query to register a listener for. NULL means register for events for all queries.
@param l The set of callback functions to be called upon different events.
@param userdata A void pointer to pass to the callback functions.
*/
void query_addListener(query_t *q, query_listener_t *l, void *userdata);

/**
@brief
Unregister a general query listener.

@param q Should match the q argument of the corresponding call to query_addListener() exactly.
@param l Should match the l argument of the corresponding call to query_addListener() exactly.
@param userdata Should match the userdata argument of the corresponding call to query_addListener() exactly.
*/
void query_delListener(query_t *q, query_listener_t *l, void *userdata);

/**
@}
*/

#endif

