/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

/**
@defgroup core core.h - Core Library
@{

@brief
The NeMo Core Library contains the core logic of NeMo and it's intended to have only one instance, namely inside the NeMo core
process.

@details
Although it is not intended to be used by other applications, the library has a publicly available set of header files anyway
because NeMo supports dynamic module loading. These modules are supposed to include one or more of these header files and to link
against the core library and not against the client library.
Compare it with dynamic loadable kernel modules that make use of the public available kernel API for example.

By linking with the core library and including its API, modules can directly query and manipulate the internal data structures of
NeMo. On top of that, the libary contains an implementation of the NeMo Common API like the client library does, see also @ref
common_api. This way, the common API is available to both modules running inside NeMo and applications running in other processes.

Before starting to write a NeMo module, please read the following important preconditions:
- NeMo is a critical component of the SoftAtHome Operating Platform and its availability/stability must never be compromised.
- The NeMo core process is single threaded. It is forbidden to create threads in modules.
- The NeMo core process is and should remain completely asynchronous, so modules MUST NOT contain blocking I/O. Modules are allowed
  to get input from external sources, but they should add their file descriptors to the PCB event loop and process the available
  input in a corresponding callback function which must also not block in that case. Notice that if this important precondition is
  not met, the responsiveness of NeMo is put at stake affecting all dependants.
- Given the heavy load that is put on NeMo, both in terms of memory (huge data model) and performance (data model is highly dynamic
  putting heavy load on the PCB serializer and the query recalucation mechanism), it is important to have the possibility to run it
  on a PC on which memory and performance analysis tools are available. Not only to run it, but also to get as close as possible to
  the real life situation while running it on the PC, implying that it should model the network from the HGW, not from the PC
  and that all dependent services that run on the HGW can connect seamlessly to the NeMo instance running on the PC. This is
  quite a challenge, but it has been done before and it should remain possible. This means that modules should be portable and that
  there should be no relation between the system that is modeled by the data model and the system on which NeMo runs. Examples:
  - Modules must never invoke system commands like ip, iptables etc.
  - Modules must never talk directly to the system, e.g. ioctls are forbidden.
  - Modules may connect to IPC server sockets of other plugins without going through the PCB system bus (performance optimization),
    as long as fallback over the network, e.g. via the system bus, is possible.
.
Notice that although your own code might meet these preconditions, the libraries against which your code links might not. So
extra prudence is needed when including libraries. An example pitfall is to use a third-party library that calls exit() if it does
not like the input it gets. Another mistake would be to load a hardco library inside the NeMo core process because it clearly
violates the last precondition.

Shortly, you are discouraged to write a NeMo module. However, some functionality simply can't be implemented elsewhere:
- Volatile parameters like uptime counters need a parameter read handler which can only be implemented inside NeMo.
- Functions with query support. Functions can be implemented out-of-process, but query support can't (e.g. getDHCPOption).
- To map a MIB to a dedicated plugin, some code needs to be written that runs inside NeMo to establish the mapping.
.
Another valid reason to implement functionality in a NeMo module is if the performance penalty of going out of process is not worth
the relaxation of preconditions.

To use the NeMo Core Library, modules shall:
- Include <nemo/core.h>.
- Add the package nemo_core to the pkg-config command line, e.g. link with `pkg-config --libs nemo_core`.
- Depend on sah_lib_nemo in their Component.dep and on SAH_LIB_NEMO in their Component.in.
- Compile into a shared object.
- Be MTK modules preferably.

@author Dries De Winter <dries.dewinter@softathome.com>
*/

#include <nemo/core/flags.h>
#include <nemo/core/intf.h>
#include <nemo/core/traverse.h>
#include <nemo/core/query.h>
#include <nemo/core/plugin.h>
#include <nemo/core/arg.h>
#include <nemo/core/map.h>

#include <nemo/common_api.h>

/**
@}
*/
