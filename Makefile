-include $(CONFIGDIR)/components.config

INSTALL ?= install
PKG_CONFIG_LIBDIR ?= $(D)/usr/lib/pkgconfig/


compile: 
	$(MAKE) -C src all
	$(MAKE) -C odl all
	$(MAKE) -C scripts all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C scripts clean
	$(MAKE) -C odl clean 

install:
	$(INSTALL) -D -m 0755 scripts/nemo-core $(D)/etc/init.d/nemo-core
	$(INSTALL) -d $(D)/usr/lib/debuginfo
	ln -sfr $(D)/etc/init.d/nemo-core $(D)/usr/lib/debuginfo/D20nemo-core
	$(INSTALL) -D -m 0755 scripts/nemo-clients $(D)/etc/init.d/nemo-clients
	$(INSTALL) -d $(D)/usr/lib/debuginfo
	ln -sfr $(D)/etc/init.d/nemo-clients $(D)/usr/lib/debuginfo/D23nemo-clients
	$(INSTALL) -D -m 755 src/libnemo_core.so $(D)/lib/libnemo_core.so.$(PV)
	ln -sfr $(D)/lib/libnemo_core.so.$(PV) $(D)/lib/libnemo_core.so
	$(INSTALL) -d $(D)/usr/lib/nemo/
	$(INSTALL) -D -m 644  odl/nemo.odl $(D)/usr/lib/nemo/
	$(INSTALL) -p -D -m 0640 pkgconfig/nemo_core.pc $(PKG_CONFIG_LIBDIR)/nemo_core.pc
	$(INSTALL) -d $(D)/usr/include/nemo/core
	$(INSTALL) -d $(D)/usr/include/netdev
	$(INSTALL) -p -D -m 0640 include/nemo/core.h  $(D)/usr/include/nemo/
	$(INSTALL) -p -D -m 0640 include/nemo/core/flags.h  $(D)/usr/include/nemo/core/
	$(INSTALL) -p -D -m 0640 include/nemo/core/intf.h  $(D)/usr/include//nemo/core/
	$(INSTALL) -p -D -m 0640 include/nemo/core/traverse.h  $(D)/usr/include//nemo/core/
	$(INSTALL) -p -D -m 0640 include/nemo/core/query.h  $(D)/usr/include//nemo/core/
	$(INSTALL) -p -D -m 0640 include/nemo/core/plugin.h  $(D)/usr/include//nemo/core/
	$(INSTALL) -p -D -m 0640 include/nemo/core/arg.h  $(D)/usr/include/nemo/core/
	$(INSTALL) -p -D -m 0640 include/nemo/core/map.h  $(D)/usr/include/nemo/core/

.PHONY: compile clean install

